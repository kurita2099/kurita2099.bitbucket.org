﻿// 住居シーンクラス
const kagu_price = [
    	[100,150,200],
    	[200,300,400],
    	[400,600,800],
    	[800,1200,1600],
    	[1600,2400,3200],
    	[3200,4800,6400],
    	[6400,9600,12800],
    	[20000,20000,20000],
    	[20000,20000,20000]
    ];
    
const kagu_MaxHeart = [//それぞれの家具を購入した際に増えるハートの最大値：家具[グレード]
		[1,2,3],
		[2,3,4],
		[3,4,5],
		[4,5,6],
		[5,6,7],
		[6,7,8],
		[7,8,9],
		[50,50,50],
	];
		
const kaguPosition = [//Player.kaguと連動「タンス、かまど、ソファ、バスタブ、テーブル、子供ベッド、夫婦ベッド、ペット」、チップ座標XY
		[	[1,1],	[2,1], 						[0],[0],[0],[0],[0],[0]	],	//shop1
		[	[1,3],	[3,1],	[4,3],	[1,1],				[0],[0],[0],[0]	],	//shop2
		[	[1,3],	[4,1],	[6,3],	[1,1],	[2,5],	[7,5],		[0],[0]	],	//shop3
		[	[1,3],	[5,1], 	[8,3],	[1,1],	[3,4],	[9,7],	[1,7],	[7,6]],	//shop4
		];
		
const BOUNS_MINE = [//各鉱石のID：茶、青、灰、赤、黄、虹)
				[ [5,4,3],[4,4,4] ],	//Aグループ
				[ [4,3,2],[3,3,3] ],	//Bグループ
				[ [2,1,0],[1,1,1] ],	//Cグループ
				];
const BOUNS_MINE_RAND = [	//中に入っている数値はそれぞれの確率分布(A,B,Cグループ)
				[0,0,1],	//何もなし
				[0,1,0],	//魔法使いの宅急便
				[2,0,2],	//魔王とボク
				[1,2,1],	//勇者の大冒険
				];
		
const kidPosition = [ [2,3],[4,5],[5,7],[7,9] ];	//子供の位置、チップ座標XY
const kaguSize = [ [64,64],[64,64],[128,64],[128,64],[128,128],[64,128],[128,128],[64,64] ];	//各チップの大きさ


const HOUSE_WINDOW_SEQUENCE_NONE = 1;	//何もしない
const HOUSE_WINDOW_SEQUENCE_POP = 2;	//出現
const HOUSE_WINDOW_SEQUENCE_WAIT = 3;	//待機
const HOUSE_WINDOW_SEQUENCE_END = 4;	//終了
//window呼び出しの記憶
const HOUSE_WINDOW_CALLNAME_WIFE = 0;	//妻
const HOUSE_WINDOW_CALLNAME_KID = 1;	//子供

//アニメーションを容易にするためのSprite構築
HouseSprite = Class.create(Sprite,{
	initialize:function(imageBox,w,h){
		Sprite.call(this,w,h);
		this.images = [];
		this.image = imageBox;
		if(imageBox)
		{
			this.images = imageBox;	//画像の配列(getImageでオブジェクトを中に入れておく事)
			this.image = imageBox[0];
		}
		
		this.animeSpeed = 6;	//アニメーションする速度
	},
	
	AnimeUpdate:function()
	{
		if(!this.images)return;
		//EnterFrame中で呼ぶ、game.frameを機軸とした時間系
		var frame = Math.floor(game.frame/this.animeSpeed);
		if(this.images.length > 1)
		{
			var img = (frame % 2 == 0) ?this.images[0]:this.images[1];	//とりあえず二種類の切り替えしか想定しない
			if(this.image != img)this.image = img;
		}
	},

});


HouseScene = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		playBgm(se.bgm_main);	//bgm
		
		this.HIT = game.UI.HitPos;
		
		//ずらし
		this.OFFSET_X = 0;
		this.OFFSET_Y = 48+80;
		
		//現在の会話進行度(0か1、ショップレベル毎に判定4段階)、-1なら喋る事がない
		this.tookIndex = -1;
		this.menuIndex = 0;	//上から順に０１２
		this.ivent_took = false;	//会話windowが開かれているか
		this.ivent_ObjectMove = false;	//スロットがクリックされたらオブジェクト移動開始
		this.handSlot = null;	//枠を記憶するための物
		this.handObject = null;	//家具オブジェクトを記憶するための物
		
		//階段座標
		this.kdown = [];
		//レベルに応じて階段座標を変更
		var pos;//計算した座標受け皿
		pos = ChipToPosition(this,3 + (Player.shopLevel*2),1)
		this.kdown[0] = pos[0];
		this.kdown[1] = pos[1];
		this.kdown[2] = pos[0]+CHIPSIZE;
		this.kdown[3] = pos[1]+CHIPSIZE;
		//ハウスマップ
		this.house = createMap(mapdata[HOUSE_OFFSET + Player.shopLevel][0],this);
		this.house.visible = true;
		this.house.x += this.OFFSET_X;
		this.house.y += this.OFFSET_Y;
		this.addChild(this.house);
	    game.UI.ViewState(this,SCENE_STATE_HOUSE,0,0);
	   	
	   	//武器屋の嫁(いつも中央に立っている
	   	var s;
	   	var imageBox;
	   	var str_round = Player.round == 0 ? "starting" : "endless";
		imageBox = [getImage("house/"+str_round+"/c_wife_1.png"),getImage("house/"+str_round+"/c_wife_2.png")];
		s = this.sprite_wife = new HouseSprite(imageBox,64,64);
		s.visible = true;
		pos = ChipToPosition(this,2 + (Player.shopLevel*1),2 + (Player.shopLevel*1) );
		s.x = pos[0];
		s.y = pos[1];
		this.addChild(s);
		
		
		//子供(ショップのレベルによって読み込む画像が違う
		var str = "";
		var LV = Player.shopLevel;
		if(LV == 0)str = ["c_baby"];
		if(LV == 1)str = ["c_kid_1","c_kid_2"];
		if(LV == 2)str = ["c_boy_1","c_boy_2"];
		if(LV == 3)str = ["c_man_1","c_man_2"];
		imageBox = [];
		for(var i = 0; i < str.length; i++)imageBox.push( getImage("house/"+str_round+"/"+str[i]+".png") );	//必要な画像を持ってきてpush
		s = this.sprite_kid = new HouseSprite(imageBox,64,64);
		s.visible = true;
		pos = ChipToPosition(this,kidPosition[LV][0],kidPosition[LV][1]);
		s.x = pos[0];
		s.y = pos[1];
		var ontime = Date.parse( new Date() );	//現在時刻
		if(Player.kid_callCoolTimer < ontime )
		{
			Player.kid_call = false;	//ついでに外す
			this.addChild(s);
		}
		
		//全ての家具
		this.houseObject = [];
		var check = true;	//該当する物があれば即座に条件から抜ける
		for(var i = 0; i < Player.kagu.length; i++)
		{
			this.houseObject.push(new HouseSprite(null,64,64) );
			s = this.houseObject[i];
			s.visible = false;
			var LV = Player.shopLevel;
			if(kaguPosition[LV][i] != 0)
			{
				//現在のレベルでアクセスできる家具なら
				
				if(Player.kaguPos[i][0] == 0)
				{
					//もし座標が設定されていなければ
					pos = ChipToPosition(this,kaguPosition[LV][i][0],kaguPosition[LV][i][1]);
					s.x = pos[0];
					s.y = pos[1];
					this.addChild(s);	//出現できるショップレベルならアタッチ
				}else
				{
					//設定されているならば、その座標通りに設置
					pos = ChipToPosition(this,Player.kaguPos[i][0],Player.kaguPos[i][1]);
					s.x = pos[0];
					s.y = pos[1];
					this.addChild(s);	//出現できるショップレベルならアタッチ
				}
			}
			
		}
		this.setImage_kagu();
		
		
		//ハート(アニメーション)
		s = this.sprite_hart = new Sprite(40,40);
		s.image = getImage("house/hart2.png");
		s.visible = false;
		this.addChild(s);
		
		
		//ウィンドウ
		this.window_state = HOUSE_WINDOW_SEQUENCE_NONE;	//シーケンス
		this.window_callName;	//誰がwindowを呼び出したのか記憶(これにより画像更新を判断)
		this.window_time = 0;
		this.window_zeroPosition = [ POSITION_MENU[0], POSITION_MENU[1] ];	//windowの出現位置(InterFaceの表示位置流用
		var zp = this.window_zeroPosition;
		//s = this.sprite_window = createDomSprite("",512,258);
		s = this.sprite_window = new Sprite(512,258);
		s.visible = false;
		s.x = zp[0];
		s.x += -(s.width/2)
		s.y = zp[1];
		this.addChild(s);
		//ウィンドウに付属する家具image
		s = this.sprite_window_kagu = new Sprite(64,64);
		s.visible = false;
		this.addChild(s);
		//HITRECT
		s = this.hitRect_wife = [];
		for(var i = 0; i < 3; i++)
		{
			//当たりエリア付けてHIT判定してしまう
			s.push(createDomSprite("debug/ybox2.png") );
			s[i].x = this.sprite_window.x + this.sprite_window.width;
			s[i].y = this.sprite_window.y;
			s[i].width = 314;
			s[i].height = 45;
			s[i].x += -(s[i].width);
			s[i].x += -10
			s[i].y += 80;
			s[i].y += (i * (s[i].height+12) );
			//ヒットエリアを可視化するなら入れる
			//s[i].visible = true;
			//this.addChild(s[i]);
			
		}
		//カーソル
		//s = this.sprite_cursor = createDomSprite("cursor_s.png",8,14);
		s = this.sprite_cursor = new Sprite(8,14);
		s.image = getImage("cursor_s.png");
		this.addChild(s);
		s.visible = false;
		//買うボタン
		//s = this.sprite_buy = createDomSprite("w_buy.png",192,90);
		s = this.sprite_buy = new Sprite(192,90);
		s.image = getImage("w_buy.png");
		this.addChild(s);
		s.x = this.sprite_window.x + this.sprite_window.width/2;
		s.y = this.sprite_window.y + this.sprite_window.height;
		s.x += -(s.width/2);
		s.y += WINDOW_SUB_OFFSET;
		s.visible = false;
		//閉じるボタン
		//s = this.sprite_close = createDomSprite("close.png" ,90,90);	//closeボタン
		s = this.sprite_close = new Sprite(90,90);
		s.image = getImage("close.png");
		s.x = CLOSE_OFFSET_X;
		s.y = CLOSE_OFFSET_Y;
		s.visible = false;
		this.addChild(s);
		
		
		this.Check_MenuIndex();	//現在の会話進行度の設定
		game.UI.ViewStateUpdate();	//インターフェイス更新
		
		this.addEventListener(Event.TOUCH_MOVE, function(e)
		{
		
		});
		
		this.addEventListener(Event.TOUCH_START, function(e)
		{
			var ClickPosition = [e.localX,e.localY];
			var s;
			var shop_Ivents = this.ivent_took || this.ivent_ObjectMove ;	//何かしらイベントが発生している状態ならtrue
			
			if(!game.UI.getInterfaceIvent() && !shop_Ivents)
			{
				if( (e.localX>this.kdown[0] && e.localX<this.kdown[2]) && (e.localY>this.kdown[1] && e.localY < this.kdown[3]) ){
	                 this.removeChild(game.UI.stateSprite);
					 game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));	//下り階段
					 playSe("sound/move.wav");
				}
			}
			
			if(Player.shopLevel == MAX_SHOPLEVEL)
			{
				//店舗が最大状態でのみ配置を変更できるように
				this.Touch_ObjectFieldTouch(ClickPosition);	//家具が選択されている状態でフィールドをタッチした際の処理
				this.Touch_HouseSlot(ClickPosition);		//家具スロットのタッチ処理
			}
			
			s = this.sprite_window;	//mainwindow参照
			
			//妻と息子の選択肢の会話
			if( this.HIT(s,ClickPosition) && s.visible)
			{
				if(this.ivent_took)
				{
					//会話中なら項目の当たり判定
					for(var i = 0; i < this.hitRect_wife.length; i++)
					{
						s = this.hitRect_wife[i];
						if( this.HIT(s,ClickPosition) )
						{
							this.menuIndex = i;	//項目更新
							this.setImage_window();	//window更新
							playSe(se.tap_1);
						}
					}
					
				}
			}
			//（妻）会話購入（現在共用)
			s = this.sprite_buy;
			if( this.HIT(s,ClickPosition) && s.visible)
			{
				switch(this.window_callName)
				{
					case HOUSE_WINDOW_CALLNAME_WIFE:	//妻の処理
					{
						var id = this.tookIndex-1;	//現在の会話の家具ID
						if(Player.coins >= kagu_price[id][this.menuIndex])
						{
							//購入処理
							playSe(se.made);
							Player.kagu[id] = this.menuIndex+1;	//現在の会話の家具LV
							Player.coins +=  -kagu_price[id][this.menuIndex];	//金増減
							this.Check_MenuIndex();	//会話進行度更新
							this.setImage_kagu();
							this.IventOff_Window();
							this.CallWindow("お礼_妻");
							//ハートアニメ
							s = this.sprite_hart;
							s.visible = true;
							pos = [this.sprite_wife.x,this.sprite_wife.y];
							s.x = pos[0];
							s.y = pos[1];
							s.x += 12;
							s.y += -30;
							s.tl.fadeIn(3).moveBy(0,-60,10).moveBy(0,30,20).fadeOut(15);
							this.setMaxHeart();	//ハートの最大値を更新する
							game.UI.ViewStateUpdate();	//インターフェイス更新
							this.Check_MenuIndex();	//メニューIndexを更新する
							
						}else
						{
							//買えない
							playSe(se.tap_2);
						}
						break;
					}
					case HOUSE_WINDOW_CALLNAME_KID:	//子供の購入
					{
						//購入処理
						if(Player.coins >= 5)
						{
							Player.coins += -5;
							Player.kid_books = this.menuIndex+1;
							playSe(se.made);
							this.CallWindow("本要求後");
						}else
						{
							playSe(se.tap_2);
						}
						
						break;
					}

				}
				
			}
			//妻
			s = this.sprite_wife;
			if(s.visible && this.HIT(s,ClickPosition) && !shop_Ivents)
			{
			if(DEBUG)console.log("tookIndex:"+this.tookIndex);
				if(this.tookIndex != -1)
				{
					this.CallWindow("家具要求");
				}else
				{
					this.CallWindow("お礼_妻");
				}
				playSe(se.tap_1);
			}
			var ontime = Date.parse( new Date() );	//現在時刻
			//子供
			s = this.sprite_kid;
			if(s.visible && this.HIT(s,ClickPosition) && !shop_Ivents)
			{
				var str = "";
				var box = 0;	//鉱石関連で使う
				if(Player.shopLevel == 0)str = "泣き声";
				if(Player.shopLevel == 1)
				{
					//本が購入されているか判断
					if(Player.kid_books == 0)
					{
						str = "本要求";
					}else
					{
						str = "本要求後"
					}
				}
				if(Player.shopLevel == 2)
				{
					var ontime = Date.parse( new Date() );	//現在時刻
					if(!Player.kid_call && Player.kid_callCoolTimer < ontime )
					{
						//まだ呼び込みをしていない && 再使用時間が終わっている
						str = "店番";
						s.tl.delay(20).then(function(){playSe(se.run);}).fadeOut(6)
						s
						//呼び込み開始
						Player.kid_call = true;
						Player.kid_callTimer = ontime+(HOUSE_KID_SHOPCALL_TIME*1000);	//各時間を記憶
						Player.kid_callCoolTimer = ontime+(HOUSE_KID_SHOPCALL_COOLTIME*1000);
					}
				}
				if(Player.shopLevel == 3)
				{
					var ontime = Date.parse( new Date() );	//現在時刻
					if(ontime > Player.kid_mineTimer)
					{
						//前回の採掘時間を超えていたら、鉱石獲得
						str = "鉱石採掘";
						//確率分布の演算を開始
						var max = 0;
						var bounsBox = [];	//ここにグループIDが入る(2なら2個入る)
						var bIndex = Player.kid_books;
						for(var i = 0; i < BOUNS_MINE_RAND[bIndex].length; i++)
						{
							var data = BOUNS_MINE_RAND[bIndex][i];
							max += data;
							if(data > 0)for(var k = 0; k < data; k++)bounsBox.push(i);	//データの数分グループID追加(これ自体が確率に)
							
						}
						if(DEBUG)console.log(bounsBox);
						var ans = Math.floor(Math.random() * max);
						var groupID = bounsBox[ans];	//グループ確定
						ans = Math.floor(Math.random()*2);//さらに2つに判定
						var mstr = "";
						if(groupID == 0)mstr = "a";
						if(groupID == 1)mstr = "b";
						if(groupID == 2)mstr = "c";
						box = "house/w_material_"+mstr+"_"+(ans+1)+".png";	//callwindowに渡す物
						//報酬を加算
						for(var i = 0; i < BOUNS_MINE[groupID][ans].length; i++)
						{
							var ID = BOUNS_MINE[groupID][ans][i];
							Player.stone[ID]++;
						}
						playSe(se.made);
						//次の採掘時間を記憶
						Player.kid_mineTimer = ontime + (HOUSE_KID_GETMINE_TIME*1000);
						
					}else
					{
						//超えていないなら後の会話
						str = "鉱石採掘後";
					}
				}
				
				
				if(DEBUG)console.log(str);
				if(str != "")
				{
					this.CallWindow(str,box);
					playSe(se.tap_1);
				}
				
			}
			
			//(妻)クローズ(現在共用）
			if( this.HIT(this.sprite_close,ClickPosition) && this.sprite_close.visible)
			{
				this.IventOff_Window();
				playSe(se.tap_2);
			}
			
			
			if(!shop_Ivents)game.UI.TouchButton(ClickPosition);
		});
		
		
		
		this.addEventListener(Event.ENTER_FRAME, function(e)
		{
			var s;
			var ClickPosition = [e.localX,e.localY];
			//嫁アニメション
			this.sprite_wife.AnimeUpdate();
			//子供アニメション
			this.sprite_kid.AnimeUpdate();
			//家具ペットアニメション
			s = this.houseObject[7];
			if(s.visible)s.AnimeUpdate();
			
			
			switch(this.window_state)
			{
				case HOUSE_WINDOW_SEQUENCE_POP:
				{
					//時間を更新してWAITへ
					this.window_time = new Date();
					this.window_state = HOUSE_WINDOW_SEQUENCE_WAIT;
					break;
				}
				case HOUSE_WINDOW_SEQUENCE_WAIT:
				{
					var time = (new Date() - this.window_time ) /1000;
					if(time > 2)
					{
						//とりあえず二秒経過で消す
						this.window_state = HOUSE_WINDOW_SEQUENCE_END;
					}
					break;
				}
				case HOUSE_WINDOW_SEQUENCE_END:
				{
					this.sprite_window.visible = false;	//非表示
					this.window_state = HOUSE_WINDOW_SEQUENCE_NONE;
					break;
				}
				
			}
		});
	},
	
	IventOn_Window:function()
	{
		this.ivent_took = true;
		this.menuIndex = 0;
		this.sprite_window.visible = true;
		this.sprite_buy.visible = true;
		this.sprite_cursor.visible = true;
		this.sprite_close.visible = true;
		this.sprite_window_kagu.visible = true;
	},
	
	IventOff_Window:function()
	{
		this.ivent_took = false;
		this.sprite_window.visible = false;
		this.sprite_buy.visible = false;
		this.sprite_cursor.visible = false;
		this.sprite_close.visible = false;
		this.sprite_window_kagu.visible = false;
	},
	
	CallWindow:function(name,box)
	{
		//鉱石要求以外はboxは使用しない
		switch(name)
		{
			case "お礼_妻":
			{
				this.ivent_took = false;
				this.sprite_window_kagu.visible = false;
				this.sprite_buy.visible = false;
				this.sprite_cursor.visible = false;
				this.sprite_close.visible = false;
				s = this.sprite_window;
				s.visible = true;
				s.width = 512;
				s.height = 90;
				s.image = getImage("house/w_wife_thanks.png");
				this.window_state = HOUSE_WINDOW_SEQUENCE_POP;	//もしwindowが表示状態なら自動で消すシークへ
				break;
			}
			case "家具要求":
			{
				//要求は少々特殊
				this.window_callName = HOUSE_WINDOW_CALLNAME_WIFE;
				this.window_state = HOUSE_WINDOW_SEQUENCE_NONE;
				this.IventOn_Window();
				this.setImage_window();
				break;
			}
			case "本要求":
			{
				this.window_callName = HOUSE_WINDOW_CALLNAME_KID;
				this.window_state = HOUSE_WINDOW_SEQUENCE_NONE;
				this.IventOn_Window();
				this.setImage_window();
				break;
			}
			case "本要求後":
			{
				this.ivent_took = false;
				this.sprite_window_kagu.visible = false;
				this.sprite_buy.visible = false;
				this.sprite_cursor.visible = false;
				this.sprite_close.visible = false;
				s = this.sprite_window;
				s.visible = true;
				s.width = 512;
				s.height = 90;
				s.image = getImage("house/w_kid_thanks.png");
				this.window_state = HOUSE_WINDOW_SEQUENCE_POP;	//もしwindowが表示状態なら自動で消すシークへ
				break;
			}
			case "泣き声":
			{
				this.ivent_took = false;
				this.sprite_window_kagu.visible = false;
				this.sprite_buy.visible = false;
				this.sprite_cursor.visible = false;
				this.sprite_close.visible = false;
				s = this.sprite_window;
				s.visible = true;
				s.width = 512;
				s.height = 90;
				s.image = getImage("house/w_baby.png");
				this.window_state = HOUSE_WINDOW_SEQUENCE_POP;	//もしwindowが表示状態なら自動で消すシークへ
				break;
			}
			case "店番":
			{
				this.ivent_took = false;
				this.sprite_window_kagu.visible = false;
				this.sprite_buy.visible = false;
				this.sprite_cursor.visible = false;
				this.sprite_close.visible = false;
				s = this.sprite_window;
				s.visible = true;
				s.width = 512;
				s.height = 90;
				s.image = getImage("house/w_kid_help.png");
				this.window_state = HOUSE_WINDOW_SEQUENCE_POP;	//もしwindowが表示状態なら自動で消すシークへ
				break;
			}
			case "鉱石採掘":
			{
				this.ivent_took = false;
				this.sprite_window_kagu.visible = false;
				this.sprite_buy.visible = false;
				this.sprite_cursor.visible = false;
				this.sprite_close.visible = false;
				s = this.sprite_window;
				s.visible = true;
				s.width = 512;
				s.height = 90;
				s.image = getImage(box);
				this.window_state = HOUSE_WINDOW_SEQUENCE_POP;	//もしwindowが表示状態なら自動で消すシークへ
				break;
			}
			case "鉱石採掘後":
			{
				this.ivent_took = false;
				this.sprite_window_kagu.visible = false;
				this.sprite_buy.visible = false;
				this.sprite_cursor.visible = false;
				this.sprite_close.visible = false;
				s = this.sprite_window;
				s.visible = true;
				s.width = 512;
				s.height = 90;
				s.image = getImage("house/w_son_help.png");
				this.window_state = HOUSE_WINDOW_SEQUENCE_POP;	//もしwindowが表示状態なら自動で消すシークへ
				break;
			}
			
		}
	},
	
	Touch_HouseSlot:function(TouchPosition)
	{
		//家具が置かれているスロットのタッチされた時に起きる処理
		for(var i = 0; i < game.InventoryManager.s_back.length; i++)
		{
			if( this.HIT(game.InventoryManager.s_back[i],TouchPosition) )
			{
				if(this.ivent_ObjectMove)
				{
					if(this.handSlot == game.InventoryManager.s_back[i])
					{
						//もし一緒のスロットなら終了
						playSe(se.tap_2);
						this.ivent_ObjectMove = false;
						this.handSlot.image = getImage("slot_1.png");
						this.handSlot = null
						this.handObject = null;
						game.UI.ViewStateUpdate();
					}else
					{
						//違うスロットならチェンジ
						playSe(se.tap_1);
						this.handSlot.image = getImage("slot_1.png");
						this.handSlot = game.InventoryManager.s_back[i];
						this.handSlot.image = getImage("slot_2.png");
						this.handObject = this.houseObject[i];
						game.UI.ViewStateUpdate();
					}
				}else
				{
					//もし初めてのタッチなら
					playSe(se.tap_1);
					this.ivent_ObjectMove = true;
					this.handSlot = game.InventoryManager.s_back[i];
					this.handSlot.image = getImage("slot_2.png");
					this.handSlot.visible = false;
					this.handObject = this.houseObject[i];
					game.UI.ViewStateUpdate();
					
				}
			}
		}
	},
	
	Touch_ObjectFieldTouch:function(TouchPosition)
	{
		//スロットのオブジェクトが選択されている状態でのマップクリック処理
		if(this.ivent_ObjectMove)
		{
			if( this.HIT(this.house,TouchPosition) )
			{
				//タッチした座標がマップの範囲内なら
				//現在の座標系をチップ座標系へ変換
				var pos = 0;	//受け皿
				var oldPos = 0;	//記憶用
				var cpos = PositionToChip(this.house,TouchPosition[0],TouchPosition[1]);
				var MAX = 5 + (Player.shopLevel*2);	//店舗LVに応じた横縦最大チップ数
				var keyX = this.handObject.width/CHIPSIZE;	//縦横の検索回数(家具のサイズに応じてブロック判定に使用）
				var keyY = this.handObject.height/CHIPSIZE;
				if(DEBUG)console.log("チップ座標系:"+cpos[0] + " "+ cpos[1]);
				if(DEBUG)console.log("最大チップ幅:"+MAX);
				
				var hantei = (cpos[0] > 0 && cpos[1] > 0 && cpos[0] < MAX-1 && cpos[1] < MAX-1);	//条件：外側の壁ブロックよりも内側ならば
				if(hantei)
				{
					//次に、現在掴んでいるオブジェクトの大きさによってその先に他のオブジェクトまたは妻、子供、階段が無いか検索
					var canOn = true;	//家具を置くことができるかどうか
					pos = ChipToPosition(this,cpos[0],cpos[1]);	//チップ座標系を絶対座標系へ
					oldPos = [ this.handObject.x, this.handObject.y ];
					
					//この時点で一回移動
					this.handObject.x = pos[0];
					this.handObject.y = pos[1];
					
					for(var i = 0; i < this.houseObject.length; i++)
					{
						//オブジェクト内接触判定
						if(this.handObject != this.houseObject[i])
						{
							//console.log("index:"+i+" チップ位置:"+cPos[0]+"  "+cPos[1]);
							if( this.Check_RectHit(this.handObject,this.houseObject[i]) )
							{
								if(DEBUG)console.log("家具接触");
								canOn = false;
							}
						}
					}
					
					//妻
					if( this.Check_RectHit(this.handObject,this.sprite_wife) )
					{
						if(DEBUG)console.log("妻接触");
						canOn = false;
					}
					
					//子供
					if( this.Check_RectHit(this.handObject,this.sprite_kid) )
					{
						if(DEBUG)console.log("子供接触");
						canOn = false;
					}
					
					//階段
					var KD = {x:this.kdown[0] , y:this.kdown[1], width:64, height:64};	//判定用にオブジェクト作ってしまう
					if( this.Check_RectHit(this.handObject,KD) )
					{
						if(DEBUG)console.log("階段接触");
						canOn = false;
					}
					
					//家具の大きさによって壁の接触判定
					for(var i = 0; i < keyY; i++)
					{
						for(var j = 0; j < keyX; j++)
						{
							var hPos = PositionToChip(this.house,this.handObject.x,this.handObject.y);	//チップ座標変換
							hPos[0] += j;
							hPos[1] += i;
							if( !(hPos[0] > 0 && hPos[1] > 0 && hPos[0] < MAX-1 && hPos[1] < MAX-1 ) )
							{
								//もし置いた後壁に接触するなら
								if(DEBUG)console.log("設置後壁接触");
								canOn = false;
							}
						}
					}
					
					if(!canOn)
					{
						//もし置けないなら過去の座標へ戻す
						this.handObject.x = oldPos[0];
						this.handObject.y = oldPos[1];
						playSe(se.tap_2);
					}else
					{
						//置けるなら、記録
						for(var i = 0; i < this.houseObject.length; i++)if(this.handObject == this.houseObject[i])Player.kaguPos[i] = cpos;	//indexは記憶してないので検索
						playSe(se.tap_1);
					}
					
					
				}else
				{
					//もしタッチされた場所がブロックならキャンセル音
					playSe(se.tap_2);
				}
				
			}
			
			
		}
	},
	
	setImage_window:function()
	{
		//もしDom化するならカーソルの処理もここに埋め込んでるので別にする
		
		var index;		//会話番号
		var s;
		var image_window;
		var image_window_kagu;
		//呼び出し元によってオブジェクト挿入
		switch(this.window_callName)
		{
			case HOUSE_WINDOW_CALLNAME_WIFE:
			{
				index = this.tookIndex;
				if(this.tookIndex == -1)return;
				image_window = getImage("house/w_kagu_0"+index+".png");
				image_window_kagu = getImage("house/object/kagu_0"+index+"_0"+(this.menuIndex+1)+".png");
				
				break;
			}
			case HOUSE_WINDOW_CALLNAME_KID:
			{
				index = 1;	//とりあえず子供の選択要求は今一つしかないので1固定
				image_window = getImage("house/w_kid_0"+index+".png");
				image_window_kagu = getImage("house/book_"+(this.menuIndex+1)+".png");
				break;
			}
			
		}
		
		
		//setDomImage(this.sprite_window,"house/w_kagu_0"+index+".png",512,258);
		s = this.sprite_window;
		s.image = image_window;
		s.width = 512;
		s.height = 258;
		
		//項目によってカーソル移動
		s = this.sprite_cursor;
		s.x = this.hitRect_wife[this.menuIndex].x - 18;
		s.y = this.hitRect_wife[this.menuIndex].y + 14;
		//項目によって家具表示
		s = this.sprite_window_kagu;
		s.image = image_window_kagu;
		//座標
		s.x = this.sprite_window.x;
		s.y = this.sprite_window.y;
		s.x += 86;
		s.y += 164;
		//家具の種類応じて中央寄せ
		s.width = kaguSize[index-1][0];
		s.height = kaguSize[index-1][1];
		s.x += -(s.width/2);
		s.y += -(s.height/2);
		
		
	},
	
	setImage_kagu:function()
	{
		//現在の情報に基づいて家具の画像及び描画許可を設定
		for(var i = 0; i < Player.kagu.length; i++)
		{
			var s = this.houseObject[i];
			if(Player.kagu[i] != 0)
			{
				if(i < Player.kagu.length-1)
				{
					s.image = getImage("house/object/kagu_0"+(i+1)+"_0"+Player.kagu[i]+".png");
					s.visible = true;
					s.width = kaguSize[i][0];
					s.height = kaguSize[i][1];
				}else
				{
					//ペットだけは、アニメーションするのでHouseSpriteで作ってしまう
					var imageBox = [ getImage("house/object/kagu_08_0"+Player.kagu[i]+".png"),getImage("house/object/kagu_08_0"+Player.kagu[i]+"b.png") ];
					s.images = imageBox;
					s.image = s.images[0];
					s.visible = true;
					s.width = kaguSize[i][0];
					s.height = kaguSize[i][1];
				}
			}else
			{
				//0なら非表示
				s.visible = false;
			}
		}
	},
	
	setMaxHeart:function()
	{
		//現在の持ってる家具を計算して、Playerのハートの最大値を更新する
		var kagus = Player.kagu;
		var heart = 0+3;	//初期ライフ加算
		for(var i = 0; i < kagus.length; i++)
		{
			if(kagus[i] > 0)
			{
				var heartID = kagus[i]-1;
				//購入されている家具からハートを加算
				heart += kagu_MaxHeart[i][heartID];
				if(i == kagus.length-1)heart = 50;	//最大値一律50
			}
		}
		
		Player.lifemax = heart;
		//Player.life = heart;
		
	},
	
	Check_MenuIndex:function()
	{
		//自分が持っている家具とショップレベルに合わせてmenuIndex、会話進行度を更新する
		for(var i = 0; i < Player.kagu.length; i++)
		{
			s = this.houseObject[i];
			//もし家具を所持していないなら
			var max = (Player.shopLevel+1)*2;
			//if(i >= (max-2) && i < max)
			if(i < max)
			{
				//レベル毎に2ずつ会話シーケンスを区切る
				if(Player.kagu[i] == 0)
				{
					this.tookIndex = i + 1;
					break;
				}else
				{
					this.tookIndex = -1;	//該当しないなら-1
				}
			}
			
		}
	},
	
	Check_RectHit:function(obj1,obj2)
	{
		//普通の矩形判定
		return (obj1.x < obj2.x + obj2.width
			 && obj1.x + obj1.width > obj2.x
			 && obj1.y < obj2.y + obj2.height
			 && obj1.y + obj1.height > obj2.y);
	},
	
});
