﻿SceneEvent = Class.create(Scene,{
	initialize:function(eventname){
	Scene.call(this);
	var sprite_arr = [];

		var bg = createDomSprite(null,640,960);
		bg._element.style.backgroundColor = "#000000";
	
		sprite_arr.push(bg);//黒い背景

		this.name = eventname;
		switch(eventname){
			case "opening":
				sprite_arr.push(createDomSprite("effect/t_opening.png", 640 , 128 ));
				this.setallSprite(0,sprite_arr);
				sprite_arr[0].tl.fadeIn(10).delay(30).fadeOut(10);//BGアニメーション
				sprite_arr[1].tl.fadeIn(10).delay(30).fadeOut(10);//logoアニメーション
				sprite_arr[1].y = (960-128)/2
			break;
			case "prologue":
				var messdata = [400,58,58*2,58*3-8,860-400-58*6+8];
				sprite_arr.push(createDomSprite("effect/title.png", 640 , messdata[0] ));//タイトル
				var _h = messdata[0];
				for(var ii=0;ii<4;ii++){
					sprite_arr.push(createDomSprite("effect/title.png", 640 , messdata[ii+1] ));//メッセージ
					sprite_arr[ii + 2].y = _h;
					sprite_arr[ii + 2]._element.style.backgroundPosition = '0 -' + _h +'px'; 
					_h += messdata[ii+1];
				}
				//アニメーション
				this.tl.cue({
					0: function(){
						sprite_arr[0].tl.fadeIn(10).delay(130).fadeOut(10);//BGアニメーション
						sprite_arr[1].tl.fadeIn(10).delay(130).fadeOut(10);
						},//アニメーション
					20: function(){sprite_arr[2].tl.fadeIn(10).delay(130-20).fadeOut(10);}, //文字1
					50: function(){sprite_arr[3].tl.fadeIn(10).delay(130-50).fadeOut(10);},
					80:function(){sprite_arr[4].tl.fadeIn(10).delay(130-80).fadeOut(10);},
					110:function(){sprite_arr[5].tl.fadeIn(10).delay(130-110).fadeOut(10);},
				});
				this.setallSprite(0,sprite_arr);
			break;
			case "epilogue":
				sprite_arr.push(createDomSprite("effect/scene.png", 640 , 380 ));//タイトル
				//sprite_arr.push(createDomSprite("effect/epilogue.png", 640 , 480 ));//メッセージ1
			    var messdata2 = [140,170,170];
				var _h = 0;
				for(var ii=0;ii<3;ii++){
				sprite_arr.push(createDomSprite("effect/epilogue.png", 640 , messdata2[ii] ));//メッセージ
				sprite_arr[ii + 2].y = 380+_h;
				sprite_arr[ii + 2]._element.style.backgroundPosition = '0 -' + _h +'px'; 
				_h += messdata2[ii];
				}
				//アニメーション
				this.tl.cue({
					0: function(){
						sprite_arr[0].tl.fadeIn(10);//BGアニメーション
						sprite_arr[1].tl.fadeIn(10);//ロゴ
						},//アニメーション
					20: function(){sprite_arr[2].tl.fadeIn(10);}, //文字1
					40: function(){sprite_arr[3].tl.fadeIn(10);},
					60:function(){sprite_arr[4].tl.fadeIn(10);},
					70:function(){this.page = 2;},
				});
				
				sprite_arr.push(createDomSprite("effect/end.png", 640 , 480 ));//最終メッセージ
				sprite_arr[5].y = 380
				this.setallSprite(0,sprite_arr);
			break;
		}
		this.addEventListener(Event.TOUCH_START, function(e){
			switch(this.name){
			case "opening":
			break;
			case "prologue":
			game.popScene();
			break;
			case "epilogue":
			if(this.page == 3){
				this.page = 4;
				sprite_arr[0].tl.fadeOut(10);
				sprite_arr[1].tl.fadeOut(10);
				sprite_arr[5].tl.fadeOut(10).then(function(){
				game.popScene();
				});
			}
			if(this.page == 2){
				this.page = 3;
				sprite_arr[2].tl.fadeOut(10);
				sprite_arr[3].tl.fadeOut(10);
				sprite_arr[4].tl.fadeOut(10);
				sprite_arr[5].tl.fadeIn(10);
			}
			if(this.page == 3){//大窪が追加した処理
				//全てのページが終了したら店へ戻る
				
				//二週目へデータ移行を開始する
				Player.tutorial = false;	//二週目チュートリアル、とりあえずしない
				Player.round++;	//周回数を加算
				Player.story = 0;	//エンディングを終えたか判断する(1は現在エンディングのイベント中、2はエンディングが終了した後)
				Player.weapon=[//ナイフ
				        0,0,0,0,0,0,
				        //斧
				        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				        //剣
				        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,];
				        
				Player.slot=[	//現在のスロット、[アイテムの種類(0:武器、1:鉱石、2:家具)][どこのスロットか][0:各種ID,1:数]
				[ [-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1] ],
				[ [-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1] ],
				[ [-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1] ],
					];
				        
				Player.knife=[2,5,];			// 短剣の数
				Player.axe=[2,5,];				// 斧の数
				Player.sword=[2,5,];			// 剣の数
		    	//Player.coins:0,			//コイン数
				Player.life=3;					// ライフ
				Player.lifemax=3;				// ライフ最大値
		    	Player.lifetimer=3;					// ライフタイマー
		    	Player.lifetimerLock=false;
				Player.pickel=0;				// ピッケルの種類
				Player.stone=[0,0,0,0,0,0,];	// 鉱石の数
		    	Player.stoneGet=[false,false,false,false,false,false,];// 一度でも獲得した鉱石は true になる
				Player.coal=0;					// 石炭の数
				//Player.diamond:0,				// ダイヤモンドの数
				Player.shopLevel=0;			// 店舗拡張レベル
				Player.shopLevelMax=0;			// どのレベルまで店舗を拡張できるか
				Player.shopLevelTime=0;		// 増築が開始された時間を記録する
				Player.shopLevelNowExtend=false;//現在増築中か否か
				Player.mineLevel=0;			// 鉱山のレベル
				Player.mineLevelMax=0;		// どのレベルまで鉱山のレベルが上がるか(速報メッセージを出す時に使用)
				Player.mineLevelTime=0;		// メッゼージを出すためのタイマ
		  		Player.kagu=[0,0,0,0,0,0,0,0];	//8種類の家具、及びレベル(順番「タンス、かまど、ソファ、バスタブ、テーブル、子供ベッド、夫婦ベッド、ペット」
		  		Player.kaguPos=[ [0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0] ];	//家具の位置、0:0なら読み込み無効
				Player.kama = [
					  {state:1, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
				      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},];
				Player.recipe = [//ナイフ
				        0,0,0,0,0,0,
				        //斧
				        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				        //剣
				        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,];
				        
				Player.iventIndex=0;		//イベント進行度、0からスタート
				Player.iventWeaponCount=0;	//武器を何個売ったかカウント、イベントが進行する度初期化
				Player.onIvent=false;		//現在イベント中か否か
				
				//子供関係
				kid_books=0,	//子供に買い与えた本の種類(0:なし 1:魔法使いの宅急便 2:魔王とボク 3:勇者の大冒険)
				kid_call=false,	//現在客呼び込みをしているかどうか
				kid_callTimer=0,	//子供が店呼び込みを終了する時間
				kid_callCoolTimer=0,	//子供が店呼び込みを終了して、復活する時間
				kid_mineTimer=0,	//子供が鉱石を獲得した後、記憶される箱
				
				
				game.pushScene(new FadeScene(shopAssets, makeNewScene(ShopScene)));
				}
			
			}
		});
		this.addEventListener(Event.ENTER_FRAME, function(){
			switch(this.name){
			case "opening":
			  if(this.age > 10+30+10){
					//game.pushScene(new FadeScene(shopAssets, makeNewScene(ShopScene)));
					if(Player.tutorial)game.pushScene(new FadeScene(tutorial_1_Assets, makeNewScene(Tutorial_1)));
					//game.pushScene(new SceneEvent("prologue"));
					
			  }
			break;
			case "prologue":
			  if(this.age > 200){
					game.popScene()
					//game.pushScene(new FadeScene(shopAssets, makeNewScene(ShopScene)));
			  }
			break;
			case "epilogue":
			break;
			}
		});
	},
//関数
	setallSprite: function(val,arr){
			for( var i=0;i<arr.length;i++){
				arr[i].opacity = val;
				this.addChild(arr[i]);
			}
	}
	
});
