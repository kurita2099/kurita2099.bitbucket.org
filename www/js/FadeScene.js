﻿const FADE_STATE_OUT = 0;
const FADE_STATE_IN = 1;
const FADE_STATE_IN_2 = 2;
const FADE_STATE_OUT_2 = 3;

FadeScene = Class.create(Scene,{
	initialize:function(assets, nextScene){
		Scene.call(this);
		this.nextScene = nextScene;
		this.speed = 0.2;

		this.backgroundColor = 'rgba(0, 0, 0, 0.0)';
		this.alpha = 0;
		this.state = FADE_STATE_OUT;
		
		//増築中の判定
		if(Player.shopLevelNowExtend)
		{
			var time = (new Date() - Player.shopLevelTime) / 1000;
			if(time > SHOP_EXTEND_TIME)
			{
				Player.shopLevelNowExtend = false;
				Player.shopLevel++;
//多重起動するので　ここは呼ばないほうがよい
//				game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
			}
			
		}
		
		this.addEventListener(Event.ENTER_FRAME, function(){
			if(this.age == 1){
			    releaseAllImage();
			}
			if(this.state == FADE_STATE_OUT){
				
				this.alpha += this.speed;
				if(1.0 <=  this.alpha){
					loadImage(assets);
					this.alpha = 1.0;
					this.state = FADE_STATE_OUT_2;
				}
				this.backgroundColor = 'rgba(0, 0, 0, ' + this.alpha + ')';
			}
			else if(this.state == FADE_STATE_OUT_2 && loadImageCounter == assets.length){
				
				this.state = FADE_STATE_IN;
				game.popScene(); // FadeScene
				var scene = game.popScene(); // フェード前のシーン
				
				// フェード前の全ての子供を削除する。TODO 必要ないかも
				while(0 < scene.childNodes.length){
					scene.removeChild(scene.firstChild);
				}
				
				//releaseAllImage();
				//nextScene.initialize2();
				//stopBgm();
				//TODO 読み込みが完了するまでフェード開始を行わないようにしなくてはならない。
				calcgazou();//画像の使用量計測
				game.pushScene(nextScene()); // フェード後のシーン
				game.pushScene(this); // FadeScene
			}
			else if(this.state == FADE_STATE_IN){

				this.alpha -= this.speed;
				if(this.alpha <= 0.0){
					this.alpha = 0.0;
					this.state = FADE_STATE_IN_2;
				}
				this.backgroundColor = 'rgba(0, 0, 0, ' + this.alpha + ')';
			}
			else if(this.state == FADE_STATE_IN_2){

			if(PC){	
	            	localStorage.setItem("team_k002",JSON.stringify(Player));
				}
				game.popScene(); // FadeScene
			}
		});
	},

});
