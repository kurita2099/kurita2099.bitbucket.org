AddCoalDialog = Class.create(Group2,{
	initialize:function(){
		Group2.call(this);
		
		this.noTouchArea = new Sprite(WIDTH, HEIGHT);
		this.noTouchArea.visible = false;
		this.noTouchArea.addEventListener(Event.TOUCH_START, function(e){
			playSe(se.tap_2);
			smithyScene.addCoalDialog.close();
		});
		
//		this.closeSp = new Sprite2(90,90);
//		this.closeSp.image = getImage('close.png');
//		this.closeSp.x = 540;
//		this.closeSp.y = 134;
//		this.closeSp.addEventListener(Event.TOUCH_START, function(e){
//			playSe(se.tap_2);
//			smithyScene.addCoalDialog.close();
//		});
		
		
		this.message = new Sprite2(512, 90);
		this.message.image = getImage('kazi/w_coal_1.png');
		this.message.setPos(WIDTH/2, 0);
		this.addChild(this.message);
		this.wait = 0;
		this.addEventListener(Event.ENTER_FRAME, function(){
			
			if(!this.visible){
				return;
			}
			
			if(FPS * 2 < this.wait){
				this.close();
			}
			this.wait++;
		});

		this.close();
	},

	open : function(kama){
		playSe(se.window);

		this.noTouchArea.visible = true;
		this.visible = true;
		this.message.y = kama.y + (kama.height / 2) + smithyScene.smithyMain.y;
		this.wait = 0;
	},

	close : function(){
		this.noTouchArea.visible = false;
		this.visible = false;
	}
});
