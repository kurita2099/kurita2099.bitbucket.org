const RECIPE_MIN_SPEED = 64;
const RECIPE_MAX_SPEED = 192;
const RECIPE_BRAKE_SPEED = 8;
const RECIPE_WEAPON = [	WEAPON_KNIFE,
						WEAPON_AXE,
						WEAPON_SWORD,
						WEAPON_LEGEND,
						38];

// レシピグループ
RecipeGroup = Class.create(Group2,{
	initialize:function(){
		Group.call(this);

		this.selectTab = 0;
		
		this.noTouchArea = new Sprite2(WIDTH, HEIGHT);
		
		this.cancel = new Sprite2(90, 90);
		this.cancel.image = getImage('close.png');
		this.cancel.x = 540;
		this.cancel.y = 134;
		
		// 下に表示するタブ
		this.tabBack = new Array(4);
		for(var i = 0; i < this.tabBack.length; i++){
			this.tabBack[i] = new Sprite2(128, 90);
			this.tabBack[i].image = getImage('recipe/w_weapon_tab_' + (1 + i) + '.png');
			this.tabBack[i].x = (WIDTH - 512) / 2 + 8 + (124 * i);
			this.tabBack[i].id = i;
			this.tabBack[i].addEventListener(Event.TOUCH_START, function(e){
				smithyScene.recipeGroup.selectTab = this.id;
				smithyScene.recipeGroup.visible = true;
				playSe(se.tap_1);
			});
			this.addChild(this.tabBack[i]);
		}

		this.recipe = new Recipe(0);
		this.recipe.x = (WIDTH - 512) / 2;
		this.recipe.y = 90 - 4;
		this.recipe.number.x = this.recipe.x + 12;
		this.recipe.number.y = this.recipe.y + 174;
		this.addChild(this.recipe);
		this.addChild(this.recipe.number);
		
		// 上に表示するタブ
		this.tab = new Array(4);
		for(var i = 0; i < this.tab.length; i++){
			this.tab[i] = new Sprite2(128, 90);
			this.tab[i].image = getImage('recipe/w_weapon_tab_' + (1 + i) + '.png');
			this.tab[i].x = (WIDTH - 512) / 2 + 8 + (124 * i);
			this.addChild(this.tab[i]);
		}
		
		this.next = new Sprite2(192, 90);
		this.next.image = getImage('recipe/w_next.png');
		this.next.x = (WIDTH - 512) / 2 + 512 - 192;
		this.next.y = 90 - 4 + 200 + 12;
		this.next.addEventListener(Event.TOUCH_START, function(e){
			var group = smithyScene.recipeGroup;
			group.recipe.id += 1;
			group.visibleButton();
			playSe(se.tap_1);
		});
		this.addChild(this.next);
		
		this.back = new Sprite2(192, 90);
		this.back.image = getImage('recipe/w_back.png');
		this.back.x = (WIDTH - 512) / 2;
		this.back.y = 90 - 4 + 200 + 12;
		this.back.addEventListener(Event.TOUCH_START, function(e){
			var group = smithyScene.recipeGroup;
			group.recipe.id -= 1;
			group.visibleButton();
			playSe(se.tap_1);
		});
		this.addChild(this.back);

		this.cancel.addEventListener(Event.TOUCH_START, function(e){
			playSe(se.tap_2);
			smithyScene.recipeGroup.close();
		});

		// フレーム処理。
		this.addEventListener(Event.ENTER_FRAME, function(){
		});
	},

	open : function(){
		this.visible = true;
		this.cancel.visible = true;
		this.noTouchArea.visible = true;
	},

	close : function(){
		this.visible = false;
		this.cancel.visible = false;
		this.noTouchArea.visible = false;
	},
	
	visible : {
		get : function(){
			return this._visible;
		},
		set : function(visible){
			this._visible = visible;

			for(var i = 0; i < this.childNodes.length; i++){
				this.childNodes[i].visible = visible;
			}
			
			if(visible){
				// 選択されている前面タブだけを表示する
				for(var i = 0; i < this.tab.length; i++){
					this.tab[i].visible = false;
				}
				this.tab[this.selectTab].visible = true;
				
				// 選択されているタブの先頭の武器を表示する
				this.recipe.id = RECIPE_WEAPON[this.selectTab];
				
				// 伝説タブは伝説の武器が一度でも作成されてから表示する
				if(		Player.recipe[WEAPON_LEGEND]
					||	Player.recipe[WEAPON_LEGEND + 1]
					||	Player.recipe[WEAPON_LEGEND + 2]){
					this.tabBack[3].visible = true;
				}
				else{
					this.tabBack[3].visible = false;
				}
				
				// 次、前のボタンの表示
				this.visibleButton();
			}
		}
	},
	
	visibleButton : function(){
		
		if(RECIPE_WEAPON[this.selectTab + 1] - 1 < this.recipe.id){
			this.recipe.id = RECIPE_WEAPON[this.selectTab];
			this.next.visible = true;
		}
		else{
			this.next.visible = true;
		}
		
		if(this.recipe.id < RECIPE_WEAPON[this.selectTab]){
			this.recipe.id = RECIPE_WEAPON[this.selectTab + 1] - 1;
			this.back.visible = true;
		}
		else{
			this.back.visible = true;
		}
	},
	
	getMakedNum : function(){
		var num = 0;
		for(var i = 0; i < Player.recipe.length; i++){
			if(Player.recipe[i] != 0){
				num++;
			}
		}
		return num;
	}
});
