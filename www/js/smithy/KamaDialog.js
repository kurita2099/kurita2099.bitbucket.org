/**
 * 釜の購入・強化ダイアログ。
 */
const KAMA_DIALOG_BUY = 0;
const KAMA_DIALOG_GRADE_UP = 1;

KamaDialog = Class.create(Group,{
	initialize:function(){
		Group.call(this);

		this.y = 256;
		
		this.noTouchArea = new Sprite(WIDTH, HEIGHT);
		this.noTouchArea.visible = false;
		
		this.closeSp = new Sprite2(90,90);
		this.closeSp.image = getImage('close.png');
		this.closeSp.x = 540;
		this.closeSp.y = 134;
		this.closeSp.addEventListener(Event.TOUCH_START, function(e){
			playSe(se.tap_2);
			smithyScene.kamaDialog.close();
		});

		this.buySp  = new Sprite2(512,90);
		this.buySp.image = getImage('kazi/w_kama_1.png');
		this.buySp.x = 64;

		this.gradeUpSp  = new Sprite2(512,90);
		this.gradeUpSp.image = getImage('kazi/w_kama_2.png');
		this.gradeUpSp.x = 64;

		this.diamond1Sp  = new Sprite2(192,90);
		this.diamond1Sp.image = getImage('w_dia_buy_01.png');
		this.diamond1Sp.setPos(WIDTH/2,0);
		this.diamond1Sp.y = 90 + 12 + this.y + smithyScene.smithyMain.y;
		this.diamond1Sp.addEventListener(Event.TOUCH_START, function(e){
			if(1 <= Player.diamond){
				playSe(se.tap_1);
				Player.diamond--;
				smithyScene.kamaDialog.selectKama.state = KAMA_LEVEL_1;
				smithyScene.kamaDialog.close();
				smithyScene.save();
			}
			else{
				playSe(se.tap_2);
			}
		});

		this.diamond3Sp  = new Sprite2(192,90);
		this.diamond3Sp.image = getImage('w_dia_buy_03.png');
		this.diamond3Sp.setPos(WIDTH/2,0);
		this.diamond3Sp.y = 90 + 12 + this.y + smithyScene.smithyMain.y;
		this.diamond3Sp.addEventListener(Event.TOUCH_START, function(e){
			if(3 <= Player.diamond){
				playSe(se.tap_1);
				Player.diamond -= 3;
				smithyScene.kamaDialog.selectKama.state = KAMA_LEVEL_2;
				smithyScene.kamaDialog.close();
				smithyScene.save();
			}
			else{
				playSe(se.tap_2);
			}
		});

		this.addChild(this.buySp);
		this.addChild(this.gradeUpSp);

		this.close();
	},

	open : function(kama, type){

		playSe(se.window);

		this.selectKama = kama;

		this.noTouchArea.visible = true;
		this.closeSp.visible = true;
		if(type == KAMA_DIALOG_BUY){
			this.buySp.visible = true;
			this.diamond1Sp.visible = true;
		}
		else if(type == KAMA_DIALOG_GRADE_UP){
			this.gradeUpSp.visible = true;
			this.diamond3Sp.visible = true;
		}
	},

	close : function(){
		this.noTouchArea.visible = false;
		this.closeSp.visible = false;
		this.buySp.visible = false;
		this.diamond1Sp.visible = false;
		this.gradeUpSp.visible = false;
		this.diamond3Sp.visible = false;
	}
});
