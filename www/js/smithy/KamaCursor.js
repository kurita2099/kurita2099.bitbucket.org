KamaCursor = Class.create(Sprite2,{
	initialize:function(imageFile){
		Sprite2.call(this,24,12);

		this.image = getImage(imageFile);
		this.baseY = 0;
		this.addY = 0;

		this.addEventListener(Event.ENTER_FRAME, function(){

			this.y = this.baseY - this.addY;

			this.addY += 2;
			if(14 <= this.addY){
				this.addY = 0;
			}
		});
	},

});
