SmithySlotStone = Class.create(Group2,{
	initialize:function(){
		Group2.call(this);

		this._num = 0;
		
		this.stone = new SmithyStone(64, 64);
		this.stone.x = 8;
		this.stone.y = 8;
		
		this.number = new Number();
		this.number.figure = 2;
		this.number.x = 80 - 32 - 4;
		this.number.y = 80 - 18 - 4;
		
		this.count = 0;
		
		this.addChild(this.stone);
		this.addChild(this.number);
	},
	
	type:{
		get : function(){
			return this.stone.type;
		},
		set : function(type){
			this.stone.type = type;
		},
	},
	
	count:{
		get : function(){
			return this._num;
		},
		set : function(num){
			this._num = num;
			
			if(99 < num){
				num = 99;
			}
			
			this.number.number = num;
		}
	},
});
