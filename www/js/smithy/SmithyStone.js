
const SMITHY_STONE_NONE = 99;
const SMITHY_STONE_DOU = 0;
const SMITHY_STONE_TETU = 1;
const SMITHY_STONE_MISURIRU = 2;
const SMITHY_STONE_AMADAIN = 3;
const SMITHY_STONE_ORIHARUKON = 4;
const SMITHY_STONE_METEORON = 5;
const SMITHY_STONE_COAL = 6;
const SMITHY_STONE_DIAMOND = 7;

SmithyStone = Class.create(Sprite2,{
	initialize:function(){
		Sprite2.call(this, 64, 64);

		this._type = SMITHY_STONE_NONE;
	},

	type : {
		get : function(){
			return this._type;
		},
		set : function(type){
			this._type = type;
			if(SMITHY_STONE_DOU <= type && type <= SMITHY_STONE_METEORON){
				this.image = getImage('stone_' + (type+1) + '.png');
			}
			else if(type == SMITHY_STONE_COAL){
				this.image = getImage('stone_0.png');
			}
			else if(type == SMITHY_STONE_DIAMOND){
				this.image = getImage('stone_7.png');
			}
			else{
				this.image = null;
				this.visible = false;
			}
		}
	}
});
