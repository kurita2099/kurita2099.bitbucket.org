// レシピの並び順とセーブデータの並び順が違うので変換用テーブル
const RECIPE_CONV = [
                      0, 1, 2, 3, 4,
                      6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,
                     22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,
                     5,21,37,
                     ];
// レシピ
Recipe = Class.create(Sprite2,{
	initialize:function(id){
		Sprite2.call(this, 512, 200);

		this.number = new Number();
		this.number.figure = 2;
		this.id = id;
		this.visible = true;
	},
	
	id : {
		get : function(){
			return this._id;
		},
		set : function(id) {
			this._id = id;
			
			if(id <= 4){
				this.number.number = id + 1;
			}
			else if(id <= 19){
				this.number.number = id + 1 - 5;
			}
			else if(id <= 34){
				this.number.number = id + 1 - 20;
			}
			else{
				this.number.number = id + 1 - 35;
			}
			
			if(Player.recipe[RECIPE_CONV[id]]){
				this.image = getImage(WEAPON[this._id]);
			}
			else{
				
				if(id <= 4 || RECIPE_CONV[id] == 5){
					this.image = getImage("recipe/w_weapon_q1.png");
				}
				else if(id <= 19 || RECIPE_CONV[id] == 21){
					this.image = getImage("recipe/w_weapon_q2.png");
				}
				else if(id <= 34 || RECIPE_CONV[id] == 37){
					this.image = getImage("recipe/w_weapon_q3.png");
				}
				else{
					this.image = getImage("recipe/w_weapon_none.png");
				}
			}
		}
	},
});
