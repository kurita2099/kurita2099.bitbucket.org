/**
 * 武器作成完了メッセージ。
 */


const MAKE_COMP_WEAPON = [
"weapon_01.png",
"weapon_02.png",
"weapon_03.png",
"weapon_04.png",
"weapon_05.png",
"weapon_36.png",
"weapon_06.png",
"weapon_07.png",
"weapon_08.png",
"weapon_09.png",
"weapon_10.png",
"weapon_11.png",
"weapon_12.png",
"weapon_13.png",
"weapon_14.png",
"weapon_15.png",
"weapon_16.png",
"weapon_17.png",
"weapon_18.png",
"weapon_19.png",
"weapon_20.png",
"weapon_37.png",
"weapon_21.png",
"weapon_22.png",
"weapon_23.png",
"weapon_24.png",
"weapon_25.png",
"weapon_26.png",
"weapon_27.png",
"weapon_28.png",
"weapon_29.png",
"weapon_30.png",
"weapon_31.png",
"weapon_32.png",
"weapon_33.png",
"weapon_34.png",
"weapon_35.png",
"weapon_38.png"
];

const MAKE_COMP_MESSAGE = [
"kazi/w_complete_01.png",
"kazi/w_complete_02.png",
"kazi/w_complete_03.png",
"kazi/w_complete_04.png",
"kazi/w_complete_05.png",
"kazi/w_complete_36.png",
"kazi/w_complete_06.png",
"kazi/w_complete_07.png",
"kazi/w_complete_08.png",
"kazi/w_complete_09.png",
"kazi/w_complete_10.png",
"kazi/w_complete_11.png",
"kazi/w_complete_12.png",
"kazi/w_complete_13.png",
"kazi/w_complete_14.png",
"kazi/w_complete_15.png",
"kazi/w_complete_16.png",
"kazi/w_complete_17.png",
"kazi/w_complete_18.png",
"kazi/w_complete_19.png",
"kazi/w_complete_20.png",
"kazi/w_complete_37.png",
"kazi/w_complete_21.png",
"kazi/w_complete_22.png",
"kazi/w_complete_23.png",
"kazi/w_complete_24.png",
"kazi/w_complete_25.png",
"kazi/w_complete_26.png",
"kazi/w_complete_27.png",
"kazi/w_complete_28.png",
"kazi/w_complete_29.png",
"kazi/w_complete_30.png",
"kazi/w_complete_31.png",
"kazi/w_complete_32.png",
"kazi/w_complete_33.png",
"kazi/w_complete_34.png",
"kazi/w_complete_35.png",
"kazi/w_complete_38.png"
];

const MAKE_COMPLETE_GROUP_STATE_MOVE = 0;
const MAKE_COMPLETE_GROUP_STATE_WAIT = 1;

MakeCompleteGroup = Class.create(Object,{
	initialize:function(){
		Object.call(this);

		this.cursor = new Sprite2(24,12);
		this.cursor.image = getImage('kazi/cursor.png');
		this.cursor.visible = false;
		
		this.message = new Sprite2(512, 90);		// メッセージウィンドウ
		this.weapon = new Sprite2(64,64);			// 武器
		this.closeArea = new Sprite(WIDTH, HEIGHT);	// メッセージウィンドウを閉じるタッチ範囲
		this.newWeaponType = -1;
		this.stone1 = -1;
		this.stone2 = -1;
		this.pickel = false;

		this.moveY = 0;
		this.state = MAKE_COMPLETE_GROUP_STATE_MOVE;
		this.waitTime = 0;

		this.message.setPos(SMITHY_WIDTH / 2, 0);

		this.closeArea.addEventListener(Event.TOUCH_START, this.closeEvent);

		// 飛び出るアニメーション
		this.weapon.addEventListener(Event.ENTER_FRAME, function(){

			var instance = smithyScene.makeCompleteGroup;
			switch(smithyScene.makeCompleteGroup.state){
			case MAKE_COMPLETE_GROUP_STATE_MOVE:
				var speed = 16;
				this.y -= speed;
				instance.moveY -= speed;
				if(instance.moveY <= -64){
					instance.state = MAKE_COMPLETE_GROUP_STATE_WAIT;
					instance.waitTime = 0;
				}
				break;
				
			case MAKE_COMPLETE_GROUP_STATE_WAIT:
				
				if(!instance.cursor.visible){
					// 自動で閉じる
					if(FPS * 2 <= instance.waitTime){
						instance.closeEvent();
					}
					
					instance.waitTime++;
				}
				break;
			}
		});
	},
	
	closeEvent : function(){
		if(smithyScene.makeCompleteGroup.state == MAKE_COMPLETE_GROUP_STATE_WAIT){
			// ピッケルを作ってない場合はピッケルが作れるかチェックする
			if(!smithyScene.makeCompleteGroup.pickel){

				smithyScene.makeCompleteGroup.createPickel();
			}
			// すでにピッケルを作ったのでウィンドウを閉じる
			else{
				smithyScene.makeCompleteGroup.close();
			}
		}
	},

	// ウィンドウを開く
	open : function(kama, axe){

		type = this.newWeapon(kama, axe);
		if(type == -1){
			return -1;
		}

		playSe(se.made);

		this.pickel = false;

		Player.weapon[type]++;	// 生成した武器を増やす
		Player.recipe[type] = 1;
		smithyScene.recipeNum.number = smithyScene.recipeGroup.getMakedNum();

		// 釜の下にメッセージを表示。
		this.message.image = getImage(MAKE_COMP_MESSAGE[type]);
		this.message.y = smithyScene.smithyMain.y + kama.y + (kama.height / 2);

		if(this.checkPickel()){
			this.cursor.x = this.message.x + this.message.width - 48;
			this.cursor.y = this.message.y + this.message.height - 24;
			this.cursor.visible = true;
		}
		
		// 生成された武器の表示。
		this.weapon.image = getImage(MAKE_COMP_WEAPON[type]);
		this.weapon.x = smithyScene.smithyMain.x + kama.x;
		this.weapon.y = smithyScene.smithyMain.y + kama.y;
		this.weapon.baseY = smithyScene.smithyMain.y + kama.y;

		// 生成された武器が飛び出るアニメーション
		this.state = MAKE_COMPLETE_GROUP_STATE_MOVE;
		this.moveY = 0;

		this.visible = true;
		this.weapon.visible = true;
		this.message.visible = true;
		this.closeArea.visible = true;

		return type;
	},

	// ウィンドウを閉じる
	close : function(){

		this.visible = false;
		this.weapon.visible = false;
		this.message.visible = false;
		this.closeArea.visible = false;
		this.cursor.visible = false;
	},

	// 生成される武器を取得する
	newWeapon : function(kama, axe){
		var stone1 = kama.makeStone1 < kama.makeStone2 ? kama.makeStone1 : kama.makeStone2;
		var stone2 = kama.makeStone1 < kama.makeStone2 ? kama.makeStone2 : kama.makeStone1;
		this.stone1 = stone1;
		this.stone2 = stone2;

		if(	stone1 == SMITHY_STONE_DOU &&
			stone2 == SMITHY_STONE_NONE){

			return 0;
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_NONE){

			return 1;
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_NONE){

			return 2;
		}
		else if(stone1 == SMITHY_STONE_AMADAIN &&
				stone2 == SMITHY_STONE_NONE){

			return 3;
		}
		else if(stone1 == SMITHY_STONE_ORIHARUKON &&
				stone2 == SMITHY_STONE_NONE){

			return 4;
		}
		else if(stone1 == SMITHY_STONE_METEORON &&
				stone2 == SMITHY_STONE_NONE){

			return 5;
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_DOU){

			if(axe){		return 6;}
			else{			return 22;}
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_TETU){

			if(axe){		return 7;}
			else{			return 23;}
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_TETU){

			if(axe){		return 8;}
			else{			return 24;}
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_MISURIRU){

			if(axe){		return 9;}
			else{			return 25;}
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_MISURIRU){

			if(axe){		return 10;}
			else{			return 26;}
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_MISURIRU){

			if(axe){		return 11;}
			else{			return 27;}
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_AMADAIN){

			if(axe){		return 12;}
			else{			return 28;}
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_AMADAIN){

			if(axe){		return 13;}
			else{			return 29;}
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_AMADAIN){

			if(axe){		return 14;}
			else{			return 30;}
		}
		else if(stone1 == SMITHY_STONE_AMADAIN &&
				stone2 == SMITHY_STONE_AMADAIN){

			if(axe){		return 15;}
			else{			return 31;}
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			if(axe){		return 16;}
			else{			return 32;}
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			if(axe){		return 17;}
			else{			return 33;}
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			if(axe){		return 18;}
			else{			return 34;}
		}
		else if(stone1 == SMITHY_STONE_AMADAIN &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			if(axe){		return 19;}
			else{			return 35;}
		}
		else if(stone1 == SMITHY_STONE_ORIHARUKON &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			if(axe){		return 20;}
			else{			return 36;}
		}
		else if(stone1 == SMITHY_STONE_METEORON &&
				stone2 == SMITHY_STONE_METEORON){

			if(axe){		return 21;}
			else{			return 37;}
		}

		return -1;
	},
	
	checkPickel : function(){
		
		var newPickel = true;

		// 同じ鉱石を使って武器を作った場合はつるはしを作成する
		if(Player.pickel < PICKEL_TETU && this.stone1 == SMITHY_STONE_TETU && this.stone2 == SMITHY_STONE_TETU){
		}
		else if(Player.pickel < PICKEL_DMISURIRU && this.stone1 == SMITHY_STONE_MISURIRU && this.stone2 == SMITHY_STONE_MISURIRU){
		}
		else if(Player.pickel < PICKEL_AMADAIN && this.stone1 == SMITHY_STONE_AMADAIN && this.stone2 == SMITHY_STONE_AMADAIN){
		}
		else if(Player.pickel < PICKEL_ORIHARUKON && this.stone1 == SMITHY_STONE_ORIHARUKON && this.stone2 == SMITHY_STONE_ORIHARUKON){
		}
		else if(Player.pickel < PICKEL_METEORON && this.stone1 == SMITHY_STONE_METEORON && this.stone2 == SMITHY_STONE_METEORON){
		}
		else{
			newPickel = false;
		}

		return newPickel;
	},

	// ピッケルの作成。
	createPickel : function(){

		var newPickel = true;
		this.pickel = true;

		// 同じ鉱石を使って武器を作った場合はつるはしを作成する
		if(Player.pickel < PICKEL_TETU && this.stone1 == SMITHY_STONE_TETU && this.stone2 == SMITHY_STONE_TETU){
			Player.pickel = PICKEL_TETU;
			this.message.image = getImage('kazi/w_pickel_2.png');
			this.weapon.image = getImage('pickel_2.png');
		}
		else if(Player.pickel < PICKEL_DMISURIRU && this.stone1 == SMITHY_STONE_MISURIRU && this.stone2 == SMITHY_STONE_MISURIRU){
			Player.pickel = PICKEL_DMISURIRU;
			this.message.image = getImage('kazi/w_pickel_3.png');
			this.weapon.image = getImage('pickel_3.png');
		}
		else if(Player.pickel < PICKEL_AMADAIN && this.stone1 == SMITHY_STONE_AMADAIN && this.stone2 == SMITHY_STONE_AMADAIN){
			Player.pickel = PICKEL_AMADAIN;
			this.message.image = getImage('kazi/w_pickel_4.png');
			this.weapon.image = getImage('pickel_4.png');
		}
		else if(Player.pickel < PICKEL_ORIHARUKON && this.stone1 == SMITHY_STONE_ORIHARUKON && this.stone2 == SMITHY_STONE_ORIHARUKON){
			Player.pickel = PICKEL_ORIHARUKON;
			this.message.image = getImage('kazi/w_pickel_5.png');
			this.weapon.image = getImage('pickel_5.png');
		}
		else if(Player.pickel < PICKEL_METEORON && this.stone1 == SMITHY_STONE_METEORON && this.stone2 == SMITHY_STONE_METEORON){
			Player.pickel = PICKEL_METEORON;
			this.message.image = getImage('kazi/w_pickel_6.png');
			this.weapon.image = getImage('pickel_6.png');
		}
		else{
			newPickel = false;
			smithyScene.makeCompleteGroup.close();
		}

		if(newPickel){
			playSe(se.made);
			this.state = MAKE_COMPLETE_GROUP_STATE_MOVE;
			this.moveY = 0;
			this.weapon.y = this.weapon.baseY;
			this.cursor.visible = false;
		}
	}

});
