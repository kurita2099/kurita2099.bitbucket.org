﻿const	RECIPE_WIDTH = 512;
const	RECIPE_HEIGHT = 200;
const	RECIPE_SPACE = 32;
const	RECIPE_SPAN = RECIPE_WIDTH + RECIPE_SPACE;

const	SMITHY_WIDTH = WIDTH;
const	SMITHY_HEIGHT = 704;

const	KAMA_MESSAGE_SPACE = 16;

var smithyScene;

// 石炭を追加してくださいダイアログ
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/AddCoalDialog.js'></script>");

// 釜クラス
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/Kama.js'></script>");

// 釜カーソル
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/KamaCursor.js'></script>");

// 釜購入・強化ダイアログ
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/KamaDialog.js'></script>");

// 鉱石
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/SmithyStone.js'></script>");

// スロット表示鉱石
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/SmithySlotStone.js'></script>");

// 釜への鉱石選択メッセージ
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/ChoiceStoneGroup.js'></script>");

// 釜へ投入する鉱石
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/DropStoneGroup.js'></script>");

// 武器製作完了
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/MakeCompleteGroup.js'></script>");

// レシピ
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/Recipe.js'></script>");

// レシピグループ
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/RecipeGroup.js'></script>");

// レシピの棚
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/RecipeTable.js'></script>");

// 石炭の箱
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/CoalBox.js'></script>");

// 製作中の釜の状態
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/MakeInfoDialog.js'></script>");

// 鍛冶シーンクラス
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/SmithyScene.js'></script>");

