
const	DROP_STONE_GROUP_MOVE_STATE_WAIT = 0;
const	DROP_STONE_GROUP_MOVE_STATE_DROP = 1;
const	DROP_STONE_GROUP_MOVE_STATE_END = 2;

/**
 * 釜に入れる鉱石。
 */
DropStoneGroup = Class.create(Group2,{
	initialize:function(){
		Group2.call(this);

		this.moveState = DROP_STONE_GROUP_MOVE_STATE_WAIT;
		this.moveY = 0;

		this.stones = new Array(2);
		for(var i = 0; i < this.stones.length; i++){
			this.stones[i] = new SmithyStone();
			this.stones[i].visible = false;
			this.addChild(this.stones[i]);

			// 鉱石をタッチしたら削除する。
			this.stones[i].addEventListener(Event.TOUCH_START, function(e){
				// 待機中以外の状態ならスキップ
				if(smithyScene.dropStoneGroup.moveState != DROP_STONE_GROUP_MOVE_STATE_WAIT){
					return;
				}
				smithyScene.dropStoneGroup.removeStone(this);
			});
		}

		// 投入アニメーション
		this.addEventListener(Event.ENTER_FRAME, function(e){

			// 投入アニメーション以外の状態ならスキップ
			if(this.moveState != DROP_STONE_GROUP_MOVE_STATE_DROP){
				return;
			}

			var speed = 16;
			this.moveY += speed;

			for(var i = 0; i < this.stones.length; i++){
				this.stones[i].y += speed;
			}

			if(128 <= this.moveY){
				if(this.stones[1].visible && this.stones[1].image != null){
					playSe(se.get);
				}
				this.stones[1].visible = false;
				this.moveState = DROP_STONE_GROUP_MOVE_STATE_END; // アニメーション終了。
			}
			else if(64 <= this.moveY){
				if(this.stones[0].visible && this.stones[0].image != null){
					playSe(se.get);
				}
				this.stones[0].visible = false;
			}
		});

		this.visible = false;
	},

	removeStone : function(stone){

		playSe(se.tap_2);
		
		smithyScene.choiceStoneGroup.visibleNofusion(false);
		
		var i;
		// 削除する鉱石を探す。
		for(i = 0; i < this.stones.length; i++){
			if(this.stones[i].y == stone.y){
				smithyScene.slotStones[stone.type].count += 1;
				break;
			}
		}

		// 鉱石を詰める。
		for(var j = i; j < this.stones.length - 1; j++){
			this.stones[j].type = this.stones[j + 1].type;
		}
		
		// 最後を空にする。
		this.stones[this.stones.length - 1].type = SMITHY_STONE_NONE;
		this.stones[this.stones.length - 1].visible = false;

		// 投入する鉱石が０個ならメッセージを変更する。
		if(!this.isDrop()){
//			smithyScene.choiceStoneGroup.message.image = getImage('kazi/w_select.png');
		}
	},

	// 投入する釜を選択
	select : function(kama){
		// 待機状態にする
		this.moveState = DROP_STONE_GROUP_MOVE_STATE_WAIT;

		// 鉱石の初期化と表示位置の調整
		for(var i = 0; i < this.stones.length; i++){
			this.stones[i].type = SMITHY_STONE_NONE
			this.stones[i].setPos(smithyScene.smithyMain.x + kama.x + kama.width / 2,0);
			this.stones[i].y = smithyScene.smithyMain.y + kama.y - kama.height - (this.stones[i].height * i);
		}
	},

	// 鉱石を追加
	addStone : function(type){

		// 組み合わせ可能な鉱石か判定する
		if(this.stones[1].type == SMITHY_STONE_NONE){
			// 釜にひとつだけ投入しているときの判定
			if(!this.checkStone(type, this.stones[0].type)){
				smithyScene.choiceStoneGroup.visibleNofusion(true);
				return false;
			}
		}
		else{
			// 釜にふたつ投入しているときの判定
			if(!this.checkStone(type, this.stones[1].type)){
				smithyScene.choiceStoneGroup.visibleNofusion(true);
				return false;
			}
		}

		playSe(se.put);

		// 釜に入れる鉱石を追加。
		for(var i = 0; i < this.stones.length; i++){
			if(this.stones[i].type == SMITHY_STONE_NONE){
				this.stones[i].type = type;
				this.stones[i].visible = true;
				return true;
			}
		}

		// すでに満杯だったので空きをつくる。
		smithyScene.slotStones[this.stones[0].type].count += 1;
		for(var i = 1; i < this.stones.length; i++){
			this.stones[i - 1].type = this.stones[i].type;
		}

		// 空きに追加。
		this.stones[this.stones.length - 1].type = type;
		
		return true;
	},

	// 投入する鉱石があるか判定
	isDrop : function(){
		for(i = 0; i < this.stones.length; i++){
			if(this.stones[i].image != null){
				return true;
			}
		}
		return false;
	},

	// 投入アニメーション開始
	drop : function(){
		// 投入状態にする
		this.moveState = DROP_STONE_GROUP_MOVE_STATE_DROP;
		this.moveY = 0;
	},

	// 組み合わせ可能な鉱石か調べる
	checkStone : function(_stone1, _stone2){
		var stone1 = _stone1 < _stone2 ? _stone1 : _stone2;
		var stone2 = _stone1 < _stone2 ? _stone2 : _stone1;

		if(	stone1 == SMITHY_STONE_DOU &&
			stone2 == SMITHY_STONE_NONE){

			return true;
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_NONE){

			return true;
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_NONE){

			return true;
		}
		else if(stone1 == SMITHY_STONE_AMADAIN &&
				stone2 == SMITHY_STONE_NONE){

			return true;
		}
		else if(stone1 == SMITHY_STONE_ORIHARUKON &&
				stone2 == SMITHY_STONE_NONE){

			return true;
		}
		else if(stone1 == SMITHY_STONE_METEORON &&
				stone2 == SMITHY_STONE_NONE){

			return true;
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_DOU){

			return true;
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_TETU){

			return true;
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_TETU){

			return true;
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_MISURIRU){

			return true;
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_MISURIRU){

			return true;
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_MISURIRU){

			return true;
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_AMADAIN){

			return true;
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_AMADAIN){

			return true;
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_AMADAIN){

			return true;
		}
		else if(stone1 == SMITHY_STONE_AMADAIN &&
				stone2 == SMITHY_STONE_AMADAIN){

			return true;
		}
		else if(stone1 == SMITHY_STONE_DOU &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			return true;
		}
		else if(stone1 == SMITHY_STONE_TETU &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			return true;
		}
		else if(stone1 == SMITHY_STONE_MISURIRU &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			return true;
		}
		else if(stone1 == SMITHY_STONE_AMADAIN &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			return true;
		}
		else if(stone1 == SMITHY_STONE_ORIHARUKON &&
				stone2 == SMITHY_STONE_ORIHARUKON){

			return true;
		}
		else if(stone1 == SMITHY_STONE_METEORON &&
				stone2 == SMITHY_STONE_METEORON){

			return true;
		}

		return false;
	},
});
