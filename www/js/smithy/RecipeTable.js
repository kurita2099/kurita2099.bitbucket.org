// レシピの棚
RecipeTable = Class.create(Sprite2,{
	initialize:function(){
		Sprite2.call(this, 64, 64);
		
		this.touchArea = new Sprite(64,64);
		
		this.touchArea.addEventListener(Event.TOUCH_START, function(){

			if(smithyScene.recipeGroup.visible){
				smithyScene.recipeGroup.nextWeapon();
			}
			else{
				playSe(se.window);
				smithyScene.recipeGroup.open();
			}
		});
	},
});
