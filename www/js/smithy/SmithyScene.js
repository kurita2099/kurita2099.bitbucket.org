﻿// 鍛冶シーンクラス
SmithyScene = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		playBgm(se.bgm_main);	//bgm
		smithyScene = this;

		this.smithyLevel = Player.shopLevel;

		this.smithyMain = new Group();
		this.kamaGroup = new Group();
		this.cursor = new KamaCursor('kazi/cursor.png');
		this.cursor1 = new KamaCursor('kazi/cursor.png');
		this.choiceStoneGroup = new ChoiceStoneGroup();
		this.recipeTable = new RecipeTable();
		this.coalBox = new CoalBox();
		this.recipeGroup = new RecipeGroup(this.smithyMain);
		this.tong = new Sprite2(64,192);
		this.coal = new Sprite2(64,64);
		this.dropStoneGroup = new DropStoneGroup();
		this.makeCompleteGroup = new MakeCompleteGroup();
		this.makeInfoDialog = new MakeInfoDialog();
		this.addCoalDialog = new AddCoalDialog();
		this.coalNum = new Number();
		this.recipeNum = new Number();
		this.message = new GetMessage();
		this.addChild(this.message);

		this.makeCompleteGroup.close();
		this.choiceStoneGroup.close();
		this.recipeGroup.close();

		this.smithyMain.x = 0;
/*
		if(Player.shopLevel == 3){
			this.smithyMain.y = INTERFACE_OFFSET_HEIGHT + 30;
		}
		else{
			this.smithyMain.y = 156;
		}
*/
		//他のシーンと位置を合わせる
		this.smithyMain.y = INTERFACE_OFFSET_HEIGHT;
		this.addChild(this.smithyMain);
		
		//メニュークリック
		this.addEventListener(Event.TOUCH_START, function(e){
			game.UI.TouchButton([e.localX,e.localY]);
		});
		// のぼり階段
		this.upStairs = new Sprite(64,64);
		this.upStairs.visible = true;
		this.upStairs.addEventListener(Event.TOUCH_START, function(e){
			if(!game.UI.getInterfaceIvent())
			{
				//鉱山メニューが開かれていなければ移動できる
				smithyScene.save();
				smithyScene = null;
				playSe(se.move);
				game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
			}
		});

		// おりる階段
		this.downStairs = new Sprite(64,64);
		this.downStairs.visible = true;
		this.downStairs.addEventListener(Event.TOUCH_START, function(e){
			if(!game.UI.getInterfaceIvent())
			{
				//鉱山メニューが開かれていなければ移動できる
				smithyScene.save();
				smithyScene = null;
				playSe(se.move);
				game.pushScene(new FadeScene(soukoAssets,makeNewScene(StoreScene)));
			}
		});

		var kama;
		this.smithy = createMap(mapdata[WORK_OFFSET + this.smithyLevel][0],this);

		switch(this.smithyLevel){
		case 0:
			//this.smithy = createSprite('kazi/work_1.png',640, 704);
			this.smithy.visible = true;
			this.smithyMain.addChild(this.smithy);
			
			// 石炭の箱
			this.coalBox.x = 224;
			this.coalBox.y = 384;
			this.coalBox.visible = true;
			this.smithyMain.addChild(this.coalBox);
			this.smithyMain.addChild(this.coalBox.touchArea);
			
			// 釜を並べる。
			kama = new Kama();
			kama.x = 160 + 64 * 2;
			kama.y = 192 +  64 * 2;
			kama.visible = true;
			this.kamaGroup.addChild(kama);
			this.smithyMain.addChild(this.kamaGroup);

			// レシピの棚
			this.recipeTable.x = 352;
			this.recipeTable.y = 384;
			this.recipeTable.visible = true;

			// のぼり階段
			this.upStairs.x = 224;
			this.upStairs.y = 256;

			// おりる階段
			this.downStairs.x = 352;
			this.downStairs.y = 256;
			break;
		case 1:
			//this.smithy = createSprite('kazi/work_2.png',640, 704);
			this.smithy.visible = true;
			this.smithyMain.addChild(this.smithy);

			// 石炭の箱
			this.coalBox.x = 160;
			this.coalBox.y = 448;
			this.coalBox.visible = true;
			this.smithyMain.addChild(this.coalBox);
			this.smithyMain.addChild(this.coalBox.touchArea);
			
			// 釜を並べる。
			for(var i = 0; i < 2; i++){
				for(var j = 0; j < 2; j++){
					kama = new Kama();
					kama.x = 96 + 64 * 2 + (64 * 2 * j);
					kama.y = 128 + 64 * 2 + (64 * 2 * i);
					kama.visible = true;
					this.kamaGroup.addChild(kama);
				}
			}
			this.smithyMain.addChild(this.kamaGroup);

			// レシピの棚
			this.recipeTable.x = 416;
			this.recipeTable.y = 448;
			this.recipeTable.visible = true;

			// のぼり階段
			this.upStairs.x = 160;
			this.upStairs.y = 192;

			// おりる階段
			this.downStairs.x = 416;
			this.downStairs.y = 192;
			break;
		case 2:
			//this.smithy = createSprite('kazi/work_3.png',640, 704);
			this.smithy.visible = true;
			this.smithyMain.addChild(this.smithy);

			// 石炭の箱
			this.coalBox.x = 96;
			this.coalBox.y = 512;
			this.coalBox.visible = true;
			this.smithyMain.addChild(this.coalBox);
			this.smithyMain.addChild(this.coalBox.touchArea);
			
			// 釜を並べる。
			for(var i = 0; i < 3; i++){
				for(var j = 0; j < 3; j++){
					kama = new Kama();
					kama.x = 32 + 64 * 2 + (64 * 2 * j);
					kama.y = 64 + 64 * 2 + (64 * 2 * i);
					kama.visible = true;
					this.kamaGroup.addChild(kama);
				}
			}
			this.smithyMain.addChild(this.kamaGroup);

			// レシピの棚
			this.recipeTable.x = 480;
			this.recipeTable.y = 512;
			this.recipeTable.visible = true;

			// のぼり階段
			this.upStairs.x = 96;
			this.upStairs.y = 128;

			// おりる階段
			this.downStairs.x = 480;
			this.downStairs.y = 128;
			break;
		case 3:
			//this.smithy = createSprite('kazi/work_4.png',640, 704);
			this.smithy.visible = true;
			this.smithyMain.addChild(this.smithy);

			// 石炭の箱
			this.coalBox.x = 32;
			this.coalBox.y = 576;
			this.coalBox.visible = true;
			this.smithyMain.addChild(this.coalBox);
			this.smithyMain.addChild(this.coalBox.touchArea);
			
			// 釜を並べる。
			for(var i = 0; i < 4; i++){
				for(var j = 0; j < 4; j++){
					kama = new Kama();
					kama.x = 64 * 2 - 32 + (64 * 2 * j);
					kama.y = 64 * 2 + (64 * 2 * i);
					this.kamaGroup.addChild(kama);
					kama.visible = true;
				}
			}
			this.smithyMain.addChild(this.kamaGroup);

			// レシピの棚
			this.recipeTable.x = 544;
			this.recipeTable.y = 576;
			this.recipeTable.visible = true;

			// のぼり階段
			this.upStairs.x = 32;
			this.upStairs.y = 64;

			// おりる階段
			this.downStairs.x = 542;
			this.downStairs.y = 64;
			break;
		}
		this.smithyMain.addChild(this.upStairs);
		this.smithyMain.addChild(this.downStairs);
		
		// 石炭の数
		this.smithyMain.addChild(this.coalNum);
		this.coalNum.figure = 2;
		if(99 < Player.coal){
			this.coalNum.number = 99;
		}
		else{
			this.coalNum.number = Player.coal;
		}
		this.coalNum.x = this.coalBox.x + 16;
		this.coalNum.y = this.coalBox.y + 40;
		this.coalBox.touchArea.x = this.coalBox.x - 64;
		this.coalBox.touchArea.y = this.coalBox.y - 64;
		
		//  作成した武器の数
		this.smithyMain.addChild(this.recipeNum);
		this.recipeNum.figure = 2;
		this.recipeNum.number = this.recipeGroup.getMakedNum();
		this.recipeNum.x = this.recipeTable.x + 16;
		this.recipeNum.y = this.recipeTable.y + 40;

		// レシピテーブルの生成。
		this.smithyMain.addChild(this.recipeTable);
		this.smithyMain.addChild(this.recipeTable.touchArea);
		this.recipeTable.touchArea.x = this.recipeTable.x;
		this.recipeTable.touchArea.y = this.recipeTable.y;

		this.coal.image = getImage('stone_0.png');
		this.coal.visible = false;
		this.coal.originPosY = -160;
		this.smithyMain.addChild(this.coal);

		this.tong.image = getImage('kazi/tong.png');
		this.tong.visible = false;
		this.tong.originPosY = -160;
		this.smithyMain.addChild(this.tong);

		game.UI.ViewState(this,SCENE_STATE_SMITHY,0,0);
		game.UI.ViewStateUpdate();
		
		// 釜の選択カーソル
		this.cursor.visible = false;
		this.smithyMain.addChild(this.cursor);
		this.cursor1.visible = false;
		this.smithyMain.addChild(this.cursor1);
		for(var i = 0; i < this.choiceStoneGroup.slotMaterial.length; i++){
			this.smithyMain.addChild(this.choiceStoneGroup.slotMaterial[i]);
		}
		
		// 釜のタッチエリアの設定。
		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){
			this.kamaGroup.childNodes[i].touchArea.x = this.kamaGroup.childNodes[i].x;
			this.kamaGroup.childNodes[i].touchArea.y = this.kamaGroup.childNodes[i].y;
			this.smithyMain.addChild(this.kamaGroup.childNodes[i].touchArea);
		}
		
		// 釜ダイアログの生成。
		this.kamaDialog = new KamaDialog();
		this.smithyMain.addChild(this.kamaDialog);

		// 鉱石の選択
		this.addChild(this.choiceStoneGroup.message);
		this.addChild(this.choiceStoneGroup.noTouchArea);
		this.addChild(this.choiceStoneGroup.make);
		this.addChild(this.choiceStoneGroup.cancel);
		
		// 鉱石
		this.slotStones = new Array(6);
		for(var i = 0; i < this.slotStones.length; i++){
			this.slotStones[i] = new SmithySlotStone();
			this.slotStones[i].type = i;
			this.slotStones[i].count = Player.stone[i];
			this.slotStones[i].x = 80 * i;
			this.slotStones[i].y = 48;
			this.addChild(this.slotStones[i]);
			this.slotStones[i].addEventListener(Event.TOUCH_START, function(e){

				// 製作画面中
				if(smithyScene.choiceStoneGroup.visible){
					// 保持しているときのみ追加できる
					if(0 < this.count){	
						if(smithyScene.dropStoneGroup.addStone(this.type)){
							// 追加できたら個数を減らす
							smithyScene.choiceStoneGroup.visibleNofusion(false);
							this.count -= 1;
						}
					}
				}
				// 通常画面中
				else{
					
					if(smithyScene.choiceStoneGroup.selectKama != null){
						smithyScene.choiceStoneGroup.open(smithyScene.choiceStoneGroup.selectKama);
						// 保持しているときのみ追加できる
						if(0 < this.count){	
							if(smithyScene.dropStoneGroup.addStone(this.type)){
								// 追加できたら個数を減らす
								smithyScene.choiceStoneGroup.visibleNofusion(false);
								this.count -= 1;
							}
						}
					}
					else{
						var add = false;
						var fireOff = false;
						
						for(var j = 0; j < smithyScene.kamaGroup.childNodes.length; j++){
							
							var kama = smithyScene.kamaGroup.childNodes[j];
							if(	kama.state != KAMA_LEVEL_NONE &&	// 釜を購入
								kama.make == KAMA_MAKE_NONE &&		// 製作中ではない
								kama.fire == KAMA_FIRE_NONE){		// 火が消えている
								fireOff = true;
							}
						}
						
						for(var j = 0; j < smithyScene.kamaGroup.childNodes.length; j++){
							
							var kama = smithyScene.kamaGroup.childNodes[j];
							if(	kama.state != KAMA_LEVEL_NONE &&	// 釜を購入
								kama.make == KAMA_MAKE_NONE &&		// 製作中ではない
								kama.fire != KAMA_FIRE_NONE){		// 火がついている
								
								smithyScene.choiceStoneGroup.open(kama);
								// 保持しているときのみ追加できる
								if(0 < this.count){	
									if(smithyScene.dropStoneGroup.addStone(this.type)){
										// 追加できたら個数を減らす
										smithyScene.choiceStoneGroup.visibleNofusion(false);
										this.count -= 1;
										add = true;
										break;
									}
								}
							}
						}
						
						if(!add && fireOff){
							playSe(se.tap_2);
							smithyScene.message.disp(10);
						}
					}
				}
			});
			
			this.slotStones[i].visible = Player.stoneGet[i];
		}

		// レシピの生成。
		this.recipeGroup.visible = false;
		this.recipeGroup.y = this.smithyMain.y + 128;
		this.addChild(this.recipeGroup.noTouchArea);
		this.addChild(this.recipeGroup);
		this.addChild(this.recipeGroup.cancel);
		
		// 釜に投入する鉱石
		this.addChild(this.dropStoneGroup);
		
		// 釜購入のボタン
		this.addChild(this.kamaDialog.noTouchArea);
		this.addChild(this.kamaDialog.diamond1Sp);
		this.addChild(this.kamaDialog.diamond3Sp);
		this.addChild(this.kamaDialog.closeSp);

		// 武器製作完了
		this.addChild(this.makeCompleteGroup.weapon);
		this.addChild(this.makeCompleteGroup.message);
		this.addChild(this.makeCompleteGroup.cursor);
		this.addChild(this.makeCompleteGroup.closeArea);

		this.addChild(this.makeInfoDialog.noTouchArea);
		this.addChild(this.makeInfoDialog.closeSp);
		this.addChild(this.makeInfoDialog);
		
		this.addChild(this.addCoalDialog.noTouchArea);
		this.addChild(this.addCoalDialog);

//		TODO 削除予定
//		var sp = new Sprite(64,64);
//		sp.image = getImage('pickel_6.png');
//		sp.y = 1000;
//		sp.visible = true;
//		sp.addEventListener(Event.TOUCH_START, function(e){
//			playSe(se.get);
//			smithyScene.save();
//		});
//		this.addChild(sp);
//
//		sp = new Sprite(64,64);
//		sp.image = getImage('pickel_5.png');
//		sp.x = 128;
//		sp.y = 1000;
//		sp.visible = true;
//		sp.addEventListener(Event.TOUCH_START, function(e){
//			playSe(se.get);
//			smithyScene.load();
//		});
//		this.addChild(sp);

		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){
			var kama = this.kamaGroup.childNodes[i];
			kama.state			= Player.kama[i].state;
			kama.makeStone1		= Player.kama[i].makeStone1;
			kama.makeStone2		= Player.kama[i].makeStone2;
			kama.makeStartTime	= Player.kama[i].makeStartTime;
			kama.make			= Player.kama[i].make;
			kama.fireOffTime	= Player.kama[i].fireOffTime;
			kama.fireTime		= Player.kama[i].fireTime;
			kama.fire			= Player.kama[i].fire;
		}
		
		// 下タブメニュー対応。
		this.saveWait = 0;
		this.addEventListener(Event.ENTER_FRAME, function(){
			
			if(this.saveWait == 5){
				this.save();
				this.saveWait = 0;
			}
			else{
				this.saveWait++;
			}
		});
	},

	save : function(){

		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){

			var kama = this.kamaGroup.childNodes[i];

			Player.kama[i].state			= kama.state;
			Player.kama[i].makeStone1		= kama.makeStone1;
			Player.kama[i].makeStone2		= kama.makeStone2;
			Player.kama[i].makeStartTime	= kama.makeStartTime;
			Player.kama[i].make				= kama.make;
			Player.kama[i].fireOffTime		= kama.fireOffTime;
			Player.kama[i].fireTime			= kama.fireTime;
			Player.kama[i].fire				= kama.fire;
		}
	},

	load : function(){

		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){

			var kama = this.kamaGroup.childNodes[i];

			kama.state			= Player.kama[i].state;
			kama.makeStone1		= Player.kama[i].makeStone1;
			kama.makeStone2		= Player.kama[i].makeStone2;
			kama.makeStartTime	= Player.kama[i].makeStartTime;
			kama.make			= Player.kama[i].make;
			kama.fireOffTime	= Player.kama[i].fireOffTime;
			kama.fireTime		= Player.kama[i].fireTime;
			kama.fire			= Player.kama[i].fire;
			kama.resume(new Date().getTime());
		}
	},

	initKama : function(){

		var kama;
		var group = new Group();

		this.smithyMain.insertBefore(group, this.kamaGroup);
		this.smithyMain.removeChild(this.kamaGroup);
		this.kamaGroup = group;

		switch(this.smithyLevel){
		case 0:

			// 釜を並べる。
			kama = new Kama();
			kama.x = 160 + 64 * 2;
			kama.y = 192 +  64 * 2;
			kama.visible = true;
			this.kamaGroup.addChild(kama);

			break;
		case 1:
			// 釜を並べる。
			for(var i = 0; i < 2; i++){
				for(var j = 0; j < 2; j++){
					kama = new Kama();
					kama.x = 96 + 64 * 2 + (64 * 2 * i);
					kama.y = 128 + 64 * 2 + (64 * 2 * j);
					kama.visible = true;
					this.kamaGroup.addChild(kama);
				}
			}

			break;
		case 2:
			// 釜を並べる。
			for(var i = 0; i < 3; i++){
				for(var j = 0; j < 3; j++){
					kama = new Kama();
					kama.x = 32 + 64 * 2 + (64 * 2 * i);
					kama.y = 64 + 64 * 2 + (64 * 2 * j);
					kama.visible = true;
					this.kamaGroup.addChild(kama);
				}
			}

			break;
		case 3:
			// 釜を並べる。
			for(var i = 0; i < 4; i++){
				for(var j = 0; j < 4; j++){
					kama = new Kama();
					kama.x = 64 * 2 - 32 + (64 * 2 * i);
					kama.y = 64 * 2 + (64 * 2 * j);
					this.kamaGroup.addChild(kama);
					kama.visible = true;
				}
			}

			break;
		}
	},


	onPause : function(){
	},

	onResume : function(){
//		var onPauseTime = parseInt(window.localStorage.getItem('onPauseTime'));
//		var time = new Date().getTime();
//
//		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){
//			this.kamaGroup.childNodes[i].resume(time);
//		}
	},
});
