const POP_WOR = 2;	//斧戦士の出やすさ
const POP_REN = 2;
const POP_KEN = 1;


NpcManager = Class.create(Object,{
	initialize:function(){
		
		//全てのNPCを管理、出力
		
		Object.call(this);    
	    this.Load();
	},	
	
	Load:function(){
		//この時点で全てのNPCを生成する
		
		//dummy、初回起動時のみ使う
		this.dummy = new NPC(null,64,64,0,0,[0,0,0]);
		
		//店主1
		var images = [getImage('player_3.png'),getImage('player_4.png')];
		this.tensyu1 = new NPC(images,64,64,0,0,[0,0,0]);
		
		//店主二週目
		var images = [getImage('player2_3.png'),getImage('player2_4.png')];
		this.tensyu2 = new NPC(images,64,64,0,0,[0,0,0]);
		
		//兵長
		images = [getImage('shop/c_g_1.png'),getImage('shop/c_g_2.png')];
		this.heityou = new NPC(images,64,64,0,0,[0,0,0]);
		
		//レンジャー1
		images = [getImage('shop/c_r_1.png'),getImage('shop/c_r_2.png')];
		this.ren1 = new NPC(images,64,64,0,0,[1,0,0]);		
		
		//レンジャー2
		images = [getImage('shop/c_r_3.png'),getImage('shop/c_r_4.png')];
		this.ren2 = new NPC(images,64,64,0,0,[1,0,0]);		

		//勇者1
		images = [getImage('shop/c_u_1.png'),getImage('shop/c_u_2.png')];
		this.yuusya1 = new NPC(images,64,64,0,0,[0,0,1]);
		
		//剣士
		images = [getImage('shop/c_u_3.png'),getImage('shop/c_u_4.png')];
		this.kensi = new NPC(images,64,64,0,0,[0,0,1]);
		
		//ウォーリア1
		images = [getImage('shop/c_w_1.png'),getImage('shop/c_w_2.png')];
		this.wor1 = new NPC(images,64,64,0,0,[0,1,0]);
		
		//ウォーリア2
		images = [getImage('shop/c_w_3.png'),getImage('shop/c_w_4.png')];
		this.wor2 = new NPC(images,64,64,0,0,[0,1,0]);
		
		//王様
		images = [getImage('shop/king_1.png'),getImage('shop/king_2.png')];
		this.king = new NPC(images,64,64,0,0,[0,0,0]);
		
		
	},
	
	setNpcforNpc:function(n1,n2)
	{
		//引数1のNPCを引数2のNPCへ一部データコピーする、自動的に使用する設定に。
		n2.poptime = n1.frame;	//出現したフレームを記憶する
		n2.animeSpeed = n1.animeSpeed;		//アニメーション速度

		n2.x = n1.x;
		n2.y = n1.y;
		
		n2.state = n1.state;				//NPCの状態を保存する、一目でわかるようにStringで制御
		n2.images = n1.images;				//使用する全ての画像を記憶
		n2.image = n1.images[0];		//初期画像
		n2.name = n1.name;					//このNPCの種類を記憶する
		n2.buyType = n1.buyType;				//購入する武器をビットで記憶する[ナイフ、斧、剣]、[1,1,1]なら全てを買う
		n2.canBuySlot = null;					//購入できるスロット番号を覚える
		n2.Used = true;					//これがtrueなら現在使用しているNPC
		n2.visible = true;		//描画フラグ
		
	},
	
	CreateNpc:function(name,x,y){
	//引数の名前で新しいNPCオブジェクトを返す
		switch(name)
		{
			case "dummy": var s = this.dummy.Copy(x,y); s.Used = false; return s; break;
			case "店主1":return this.tensyu1.Copy(x,y);break;
			case "店主2":return this.tensyu2.Copy(x,y);break;
			case "勇者1":return this.yuusya1.Copy(x,y);break;
			case "剣士":return this.kensi.Copy(x,y);break;
			case "兵隊長":return this.heityou.Copy(x,y);break;
			case "レンジャー1":return this.ren1.Copy(x,y);break;
			case "レンジャー2":return this.ren2.Copy(x,y);break;
			case "ウォーリア1":return this.wor1.Copy(x,y);break;
			case "ウォーリア2":return this.wor2.Copy(x,y);break;
			case "王様":return this.king.Copy(x,y);break;
		}
	},
	
	CreateRandomNpcAndSet:function(npc,name){
		//引数のNPCに、出現率に基づいてランダムに決定したNPCデータのコピーをセットする
		//第二引数が渡されたら、その第二引数のNPC名をコピーしてセットする
		
		var str = "none";
		if(!name)
		{
			var popNum = -1;	//出目
			var popLen = POP_WOR+POP_KEN+POP_REN;	//全ての要素数
			var voidNpc = null;
			popNum = Math.random()*100 % popLen;
			popNum = Math.floor(popNum);
			if(popNum < POP_WOR)
			{
				str = "ウォーリア2";
			}else if(popNum < POP_WOR+POP_REN)
			{
				str = "レンジャー2";
			}else
			{
				str = "剣士";
			}
		}else
		{
			str = name;
		}
		
		voidNpc = this.CreateNpc(str,0,0);	//要求NPCを生成
		this.setNpcforNpc(voidNpc,npc);	//要求NPCを引数npcにデータコピー
	},
	
	
});
