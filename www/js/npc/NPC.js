﻿
const	MOVE_X	=	64;	//チップの大きさ
const	MOVE_Y	=	64;

// NPC定義
NPC = Class.create(Sprite,{
	initialize:function(image,width,height,x,y,buytype){
		Sprite.call(this,width,height);
		this.poptime = game.frame;	//出現したフレームを記憶する
		this.time = null;					//汎用的な時間計測用タイマ
		this.visible = false;		//最初は描画しない
		this.moveSpeed = [0,-6];	//移動速度
		this.animeSpeed = 4;		//アニメーション速度

		this.x = x;
		this.y = y;
		
		this.state = "入店";				//NPCの状態を保存する、一目でわかるようにStringで制御
		this.images = image;				//使用する全ての画像を記憶
		if(image!=null)this.image = this.images[0];		//初期画像
		this.buyType = buytype;				//購入する武器をビットで記憶する[ナイフ、斧、剣]、[1,1,1]なら全てを買う
		this.buySlotIndex = -1;				//購入するスロットを記憶するための物
		this.Used = true;					//これがtrueなら現在使用しているNPC
	},
	
	Init:function()
	{
		//初期化
	},
	
	getCenterPos:function()
	{
		//センター座標x,yを返す(int)
		var x = Math.floor(this.x+this.width/2);
		var y = Math.floor(this.y+this.height/2);
		var pos = [x,y];
		return pos;
	},
	
	getBuySlots:function()
	{
		//自分が買う事のできる武器が入ったスロット番号を配列にして返す(checkCanBuyとしている事はほとんど変わらない）（返す内容が違う）
		var list = [];
		
		for(var i = 0; i < Player.slot[0].length; i++)
		{
			var ID = Player.slot[0][i][0];
			
			if(ID != -1)
			{
				//IDが正規データならば判定開始(-1じゃなければ数は1以上ある）
				var type = game.InventoryManager.getWeaponType(ID);
				var canBuy = this.buyType[type] == 1;	//スロットの武器タイプを特定し、そのタイプのビットが立っているか判定
				if(canBuy)list.push(i);	//push
			}
		}
		
		return list;
	},
	
	setBuyRandomSlot:function()
	{
		//メンバに自分が買うスロットをランダムで1つ記憶させる
		var slotList = this.getBuySlots();	//このNPCが購入可能なスロットリスト
		var num = -1;	//出目
		if(slotList.length == 0)alert("err:NPC.js	貰ったslotListの長さが0です、このエラーが出るならば処理の流れが正常ではありません");
		
		num = Math.random()*100 % slotList.length;
		num = Math.floor(num);
		this.buySlotIndex = slotList[num];	//記憶
	},
	
	onScene:function(scene)
	{
		if(!this.Used)return;
		//シーンに乗せる
		scene.addChild(this);
		this.visible = true;
	},
	
	ChangeImage:function()
	{
		if(!this.Used)return;
		//現在のフレームを見てキャラクタをアニメーション
		var frame = Math.floor(game.frame/this.animeSpeed);
		this.image = ( (this.poptime+frame) % 2 == 0) ?this.images[0]:this.images[1];
	},
	
	Check_CanBuy:function()
	{
		//このNPCがスロットの中から買える物があるのか判定、trueならば買える
		//ついでに買う事ができるスロットの位置を覚える
		
		for(var i = 0; i < SLOTMAX; i++)
		{
			//スロットの数だけ回し、1つ見つけたら購入してループから出る
			var syurui = 0;	//武器のレイヤースロットを参照する
			var ID = Player.slot[syurui][i][0];	//武器のID
			if(ID != -1)
			{
				//IDが正規データならば判定開始(-1じゃなければ数は1以上ある）
				var type = game.InventoryManager.getWeaponType(ID);
				var canBuy = this.buyType[type] == 1;	//スロットの武器タイプを特定し、そのタイプのビットが立っているか判定
				if(canBuy)return true;
			}
			
		}	
		return false;
	},
	
	BuyWeapon:function()
	{
		//canBuyで判定したslotの中から購入する
		//購入ウィンドウを出す
		var index = this.buySlotIndex;
		var weaponID = Player.slot[0][index][0];
		
		Player.slot[0][index][1]--;	//スロット所持数から減らす
		if(Player.slot[0][index][1] == 0)
		{
			Player.slot[0][index][0] = -1;
			Player.slot[0][index][1] = -1;
		}
		Player.weapon[weaponID]--;	//総所持数から減らす
		Player.coins += WEAPONPLACE[weaponID];	//所持金加算
		if(plugin){
			try{
				window.plugins.gamecenter.reportScore(function(e){;},function(e){;},CATEGORY , Player.coins);
			}catch(e){
				alert(e.stack);
			}
		}
		Player.iventWeaponCount++;	//売った数を加算
		this.buySlotIndex = -1;		//index初期化
		game.UI.CallWindow("購入",weaponID);
		game.UI.ViewStateUpdate();
		
	},
	
	Move:function()
	{
		//状態によって動きを変化
		switch(this.state)
		{
			case "入店":
			{
				//目標地点を決めて、状態を移行する
				this.y += this.moveSpeed[1];
				this.x += this.moveSpeed[0];
				break;
			}
			
			case "武器を手に取る":
			{
				this.time = new Date();
				this.state = "購入待機";
				this.BuyWeapon();
				break;
			}
			
			case "購入待機":
			{
				var timer = (new Date - this.time) / 1000;
				//2秒経過でシーケンス移行
				if(timer >= 2)this.state = "購入";
				
				break;
			}
			
			case "武器を見る":
			{
				//購入できない場合のシーケンス
				this.time = new Date();
				this.state = "購入不可待機";
				game.UI.CallWindow("買えない",-1);	//ウィンドウも出す
				break;
			}
			
			case "購入不可待機":
			{
				var timer = (new Date - this.time) / 1000;
				//2秒経過でシーケンス移行
				if(timer >= 2)this.state = "店を出る";
				
				break;
			}
			
			case "購入":
			{
				//武器を購入し、削除フラグを立てる
				this.visible = false;
				this.Used = false;
				break;
			}
			
			case "店を出る":
			{
				this.visible = false;
				this.Used = false;
				break;
			}
			
			case "挨拶":
			{
				//イベント用、このシーケンスに入った後メッセージ判定へ
				if(IVENT_NPCINDEX[Player.iventIndex] == 0)game.UI.CallWindow("勇者",Player.iventIndex);
				if(IVENT_NPCINDEX[Player.iventIndex] == 1)game.UI.CallWindow("兵隊長",Player.iventIndex);
				if(Player.story == 1)game.UI.CallWindow("最後",Player.iventIndex);	//通常エンディングは、window0番目は条件が満たされてから、1番目はここでcallされる
				this.state = "停止";
				break;
			}
			case "停止":
			{
				//何もしない
				break;
			}
			
		}
		
		
		
	},
	
	Copy:function(x,y)
	{
		//自身の一定のコピーオブジェクトを返す
		var ob = new NPC(this.images,this.width,this.height,x,y,this.buyType);
		return ob;
	},
	
	
});

