﻿//全てのスロットの生成、調整、管理、初期化
const SLOTMAX = 8;	//スロットの数
const SLOT_MAX_IN = [3,6,9,12];	//1スロットあたりの武器を置くことができる最大数(店のレベルにより変動)

const TIME_SELLWEAPON = 1;
const IVT_SETWEAPON_AUTO = 0;
const IVT_SETWEAPON_SELF = 1;


InventoryManager = Class.create(Object,{
	surface:null,		//描画サーフェイス
	surfaceSprite:null,	//乗せるスプライト
	surfaceOffset:null,	//surfaceの初期座標
	
	s_back:null,		//スロットの背景画像
	s_weaponImage:null,	//スロットに置かれている武器画像(中央寄せ)
	s_numbers:null,		//スロットに置かれている武器数画像(右下詰め)
	
	bank:null,			//在庫数用
	

	initialize:function()
	{
		//SurfaceInit
		this.surface = new Surface(WIDTH,80+48);
		this.surfaceSprite = new Sprite(WIDTH,80+48);
		this.surfaceSprite.y = 0;
		this.surfaceSprite.image = this.surface;
		this.surfaceOffset = [0,48];
		
		this.bank = new Array(Player.weapon.length);	//在庫配列、メソッドのみで使用
		
		//スロット(枠）
		var index = 8;	//8個分
		var x = 0;
		var y = this.surfaceOffset[1];	
		this.s_back = [];	//枠
		this.s_weaponImage = [];	//こちらが武器、鉱石を表示する現物
		this.s_numbers = new Array(index);	//武器などの数を表示する部分
		for(var i = 0; i < index; i++)
		{
			//まず枠
			this.s_back.push(createSprite('slot_1.png',80,80));
			this.s_back[i].x = x;
			this.s_back[i].y = y;
			//つぎに現物
			this.s_weaponImage.push(new Sprite(64,64));

			//backの相対的な位置からflontを真ん中の座標へ変更する
			this.setCenterPos(this.s_back[i],this.s_weaponImage[i]);
			//最後に数字部、スロット右下へ配置
			this.s_numbers[i] = [ new Sprite(16,18), new Sprite(16,18) ];
			var s = this.s_numbers[i];
			//右下寄せ
			s[0].x = this.s_back[i].width + this.s_back[i].x;
			s[0].y = this.s_back[i].height+ this.s_back[i].y;
			s[0].x += -s[0].width;
			s[0].y += -s[0].height;
			s[1].x = s[0].x - s[0].width;
			s[1].y = s[0].y;
			//少し左上に
			var offset = 4;
			s[0].x += -offset;
			s[0].y += -offset;
			s[1].x += -offset;
			s[1].y += -offset;
			x += 80;
		}
		
	},
	
 	clearSlot:function(){
    //一旦全ての描画フラグを初期化
		for(var i = 0; i < this.s_weaponImage.length; i++)
		{
			this.s_weaponImage[i].visible = false;
			this.s_numbers[i][0].visible = false;
			this.s_numbers[i][1].visible = false;
			
			//さらにimageも初期化
			this.s_weaponImage[i].image = null;
			this.s_numbers[i][0].image = null;
			this.s_numbers[i][1].image = null;
		}
	},
	
	setImage_slot:function(sceneIndex,layer)
	{
		//layer:参照するプレイヤスロット
		//PlayerSlotを参照し画像データを決定
		//スロット
		//シーンによって処理が違う
		
		this.clearSlot();	//スロット初期化
		if(sceneIndex == SCENE_STATE_HOUSE)
		{
			for(var i = 0; i < Player.kagu.length; i++)
			{
				this.s_weaponImage[i].visible = false;
				this.s_numbers[i][0].visible = false;
				this.s_numbers[i][1].visible = false;
				this.s_weaponImage[i].image = null;
				this.s_numbers[i][0].image = null;
				this.s_numbers[i][1].image = null;
				
				if(Player.kagu[i] > 0)
				{
					//もし家具を所持しているなら
					var str = "";
					var LV = Player.kagu[i];
					if( i < 2 || i == Player.kagu.length-1)
					{
						str = "house/object/kagu_0"+(i+1)+"_0"+LV+".png";
					}else
					{
						str = "house/object/kagu_0"+(i+1)+"_0"+LV+"_icon.png";
					}
					this.s_weaponImage[i].image = getImage(str);
					this.s_weaponImage[i].visible = true;
					
				}
			}
			
		}else
		{
			for(var i = 0; i < Player.slot[layer].length; i++)
			{
				//プレイヤスロット分回す
				var index = i;
				if(Player.slot[layer][index][0] != -1)
				{
					//各種IDが正規なら
					
					var str = "";
					var name =  "";
					var frame = 0;	//シートを使う場合のみ変化
					var numFrag = false;	//残量を描画するならtrue
					
					//呼ばれているシーン毎に画像pathを変更(呼び出す元でswitch分けしてるのにこれは無駄か、時間があれば修正）
					switch(sceneIndex)
					{
						case SCENE_STATE_SHOP:
						{
							//武具
							
							//IDを画像パスの数字へ変更
							var imageID = WEAPON_IMAGEID[ Player.slot[layer][index][0] ];	//イメージに応じた数字を獲得
							if(imageID < 10)str = 'weapon_0'+imageID+'.png';
							if(imageID >= 10)str = 'weapon_'+imageID+'.png';
							numFrag = true;
							break;
						}
						case SCENE_STATE_STORE:
						{
							//家具
							
							//IDを画像パスの数字へ変更
							var imageID = WEAPON_IMAGEID[ Player.slot[layer][index][0] ];	//イメージに応じた数字を獲得
							if(imageID < 10)str = 'weapon_0'+imageID+'.png';
							if(imageID >= 10)str = 'weapon_'+imageID+'.png';
							numFrag = true;
							break;
						}
					}
					this.s_weaponImage[index].image = getImage(str);
					this.s_weaponImage[index].frame = frame;
					this.s_weaponImage[index].visible = true;
					//数字を更新	
					var num = Player.slot[layer][index][1];
					var place = game.UI.getPlace(num);
					str = 'InterFace/'+place[0]+'.png';
					this.s_numbers[index][0].image = getImage(str);
					this.s_numbers[index][0].visible = true;
					str = 'InterFace/'+place[1]+'.png';
					this.s_numbers[index][1].image = getImage(str);
					this.s_numbers[index][1].visible = place[1] != 0;
			    	if(num <= 0)alert("武器数が0になっています、データが正しく挿入されていません");
					
				}
				
			}
		}
		
	},
	
	Draw:function()
	{
		//一旦全ての描画フラグを初期化
	    var s;
		//スロット描画
		for(i = 0; i < this.s_back.length; i++)
		{
			s = this.s_back[i];
			this.surface.draw(s.image,s.x,s.y);
			s = this.s_weaponImage[i];
			if(s.image != null)
			{
			    if(s.frame>0){
 		           	this.surface.draw(s.image,0,s.height*s.frame,s.width,s.height,s.x,s.y,s.width,s.height);
			    }else{
            	 	this.surface.draw(s.image,s.x,s.y);
			    }
			}
			s = this.s_numbers[i][0];
			if(s.image != null)this.surface.draw(s.image,s.x,s.y);
			
			s = this.s_numbers[i][1];
			if(s.image != null && s.visible)this.surface.draw(s.image,s.x,s.y);
		}
	},
	
	Draw_Slot:function()
	{
		//スロット描画のみ
	    var s;
		for(i = 0; i < this.s_back.length; i++)
		{
			s = this.s_back[i];
			this.surface.draw(s.image,s.x,s.y);
		}
	},
	
	Draw_SlotImage:function()
	{
		//一旦全ての描画フラグを初期化
	    var s;
		//内部Image描画
		for(i = 0; i < this.s_back.length; i++)
		{
			s = this.s_weaponImage[i];
			if(s.image != null)
			{
			    if(s.frame>0){
 		           	this.surface.draw(s.image,0,s.height*s.frame,s.width,s.height,s.x,s.y,s.width,s.height);
			    }else{
            	 	this.surface.draw(s.image,s.x,s.y);
			    }
			}
			s = this.s_numbers[i][0];
			if(s.image != null)this.surface.draw(s.image,s.x,s.y);
			
			s = this.s_numbers[i][1];
			if(s.image != null && s.visible)this.surface.draw(s.image,s.x,s.y);
		}
	},
	
	Atach:function(scene)
	{
		//参照SceneへSlotImageをaddChild
		for(var i = 0; i < this.s_weaponImage.length; i++){this.s_weaponImage[i].frame = 0;}
		scene.addChild(this.surfaceSprite);
		
	},
	
	TimedSellWeapon:function(time)
	{
		//引数の経過時間から、スロット中のアイテムを売却します
		var sec = Math.floor(time/1000);	//sec変換
		var sellNum = Math.floor(sec/(60*TIME_SELLWEAPON));	//とりあえず5秒ごと
		
		if(sellNum > 0)
		{
			//売却可能数が0より多ければ
			var max = 0;	//スロット内総合武器数
			var wBox = [];	//ランダム用の箱
			var sellCount = 0;	//いくつ売れたか
			
			for(var i = 0; i < Player.slot[0].length; i++)
			{
				if(Player.slot[0][i][0] != -1)
				{
					//武器が存在しているなら箱に詰める
					wBox.push(Player.slot[0][i]);
					max += Player.slot[0][i][1];
				}
			}
			
			while(max > 0 && sellNum > 0)
			{
				//どちらかが空になるまで売却処理を繰り返す
				var ran = Math.random()*100;
				ran = Math.floor(ran % wBox.length);
				
				var ID = wBox[ran][0];
				
				if(DEBUG)console.log("max:"+max+"  sellNum:"+sellNum + "Money+"+WEAPONPLACE[ID]);
				
				Player.weapon[ID]--;	//総所持数から減らす
				Player.coins += WEAPONPLACE[ID];	//所持金加算
				wBox[ran][1]--;
				sellNum--;
				max--;
				sellCount++;
				if(wBox[ran][1] == 0)
				{
					wBox[ran][0] = -1;
					wBox[ran][1] = -1;
					wBox.splice(ran,1);	//無くなった箇所は検索除外
				}
			}
			
			//プレイヤー店番設定
			Player.mise_Message.know = true;
			Player.mise_Message.num = sellCount;
			
		}
		
	},
	
	
	setCenterPos:function(sprite,sprite2)
	{
		//spriteの相対座標によってsprite2の座標を真ん中に持ってくる
	  	 //まずspriteの画像の中心に持ってくる
	  	 sprite2.x = sprite.x+sprite.width/2;
	  	 sprite2.y = sprite.y+sprite.height/2;
	  	 //次にsprite2の画像の半分だけ左上にずらす
	  	 sprite2.x -= sprite2.width/2;
	  	 sprite2.y -= sprite2.height/2;
	},
	
	setWeaponForSlot:function(MODE,weaponID,slotIndex)
	{
		//指定したスロットへ、指定した武器を挿入する処理、MODEによって処理変更。
		var bank = this.getWeaponBank();	//在庫取得
		
		if(bank[weaponID] <= 0)return;	//入れるべき武器が無いなら帰る
		
		switch(MODE)
		{
			case IVT_SETWEAPON_AUTO:
			{
				//指定した武器を空きスロットを検索して挿入
				var slot = null;
				for(var i = 0; i < Player.slot[0].length; i++)
				{
					slot = Player.slot[0][i];
					
					if(slot[0] == -1)
					{
						//スロット空きなら挿入
						slot[0] = weaponID;
						slot[1] = bank[weaponID];
						if(slot[1] >=  SLOT_MAX_IN[Player.shopLevel])slot[1] = SLOT_MAX_IN[Player.shopLevel];
						return;
						
					}else if(slot[0] == weaponID && slot[1] < SLOT_MAX_IN[Player.shopLevel])
					{
						//同種のアイテム&&スロットの武器数に空きがあるなら加算
						slot[1] += bank[weaponID];
						if(slot[1] >=  SLOT_MAX_IN[Player.shopLevel])slot[1] = SLOT_MAX_IN[Player.shopLevel];
						return;
					}
				}

			}
			case IVT_SETWEAPON_SELF:
			{
				//指定した武器を指定したスロットに挿入
				if(slot[0] == -1)
				{
					//スロット空きなら挿入
					slot[0] = weaponID;
					slot[1] = bank[weaponID];
					if(slot[1] >=  SLOT_MAX_IN[Player.shopLevel])slot[1] = SLOT_MAX_IN[Player.shopLevel];
					return;
					
				}else if(slot[0] == weaponID)
				{
					//同種のアイテムなら加算
					slot[1] += bank[weaponID];
					if(slot[1] >=  SLOT_MAX_IN[Player.shopLevel])slot[1] = SLOT_MAX_IN[Player.shopLevel];
					return;
				}
			}
		}
		
		
		
	},
	
	getTouchSlotIndex:function(touchPosition)
	{
		//クリックされたスロットの参照番号を返す、何もクリックされていなかったらNULLを返す
		for(var i = 0; i < this.s_back.length; i++)
		{
			if(game.UI.HitPos(this.s_back[i],touchPosition) )
			{
				return i;
			}
		}
		
		return null;
	},
	
	getWeaponType:function(index)
	{
		//渡された値が、どの種類の武器か返す(0:ナイフ、1:斧、2:剣)
		var type = null;
		var min = null;
		var max = null;
		if( !(index >= 0 && index < W_N+W_O+W_S) && DEBUG)alert("引数が武器の正規データではありません、INDEX　＝　"+index);	//念のためエラー処理
		
		//ナイフ判定
		min = 0; max = W_N;
		if(index >= min && index < max)type = 0;
		min = W_N; max = W_N+W_O;
		if(index >= min && index < max)type = 1;
		min = W_N+W_O; max = W_N+W_O+W_S;
		if(index >= min && index < max)type = 2;
		
		return type;
	},
	
	getWeaponCount:function(n)
	{
		//武器の種類を指定して、スロット内の在庫数を返す、倉庫の中の数はカウントしない
		//-1は全種類カウント
		var	count = 0;
		
		for(var i = 0; i < SLOTMAX; i++)
		{
			var syurui = 0;	//スロットレイヤ指定
			var ID = Player.slot[syurui][i][0];
			
			if(n == -1 && ID != -1)
			{
				count += Player.slot[syurui][i][1];
			}else if(ID != -1 && this.getWeaponType(ID) == n)
			{
				//引数のtypeとスロット内の武器typeが一致していたら
				//数を数える
				count += Player.slot[syurui][i][1];
			}
			
		}
		
		return count;
		
	},
	
	getWeaponBank:function()
	{
		//現在の在庫(スロットの中の武器数を減らした数)を返す
		for(var i = 0; i < this.bank.length; i++){this.bank[i] = Player.weapon[i];}	//在庫セット
		for(var i = 0; i < SLOTMAX; i++)
		{
			if(Player.slot[0][i][0] != -1)
			{
				//正規データなら減算
				var ID = Player.slot[0][i][0];
				this.bank[ ID ] += -Player.slot[0][i][1];
				if(this.bank[ID] < 0 && DEBUG){
					alert("武器の数がスロット内部と倉庫で一致しません、各処理を確認して下さい、次に各データを表示します");
					alert("武器番号["+ID+"]\nスロット番号["+i+"]\nスロットに存在した武器数["+Player.slot[0][i][1]+"]\n在庫数["+this.bank[i]+"]");
				}
			}
		}
		
		return this.bank;
	},


});


















