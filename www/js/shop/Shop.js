﻿const NPC_MAX = 16;			//どれほどのNPCオブジェクトを確保するか
const NPC_POPSEC = 9;		//lv0で何秒でお客さんが来るか
const NPC_POPSEC_LEN = 2;	//lv毎の秒数間隔
const NPC_STARTINDEX = 5;	//配列の何番目から通常NPCか(4以下はイベントNPC用)

//各仲間の指標、勇者進行度視点
const NPC_POPINDEX_REN = 5;	//レンジャーの仲間になるタイミング
const NPC_POPINDEX_WOR = 8;	//ウォーリアの仲間になるタイミング

ShopScene = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		game.Time = new Date();	//時間無理やり更新
		this.shopScene = this;
		
		//制御系
		this.timeLock = false;	//秒数制御
		this.touchLock = false;	//タップ制御
		this.mineExtendNow = false;//鉱山処理判定
		
		//ずらし
		this.OFFSET_X = 0;
		this.OFFSET_Y = INTERFACE_OFFSET_HEIGHT;
		
		//階段
		this.kup = [0,0,0,0];
		this.kdown = [0,0,0,0];
		//扉
		this.tobira = [0,0,0,0];
		
		playBgm(se.bgm_main);	//bgm
		
		//レベルに応じて階段座標を変更
		
		var pos;//計算した座標受け皿
		pos = ChipToPosition(this,1,1); 
		this.kdown[0] = pos[0];
		this.kdown[1] = pos[1];
		this.kdown[2] = pos[0]+CHIPSIZE;
		this.kdown[3] = pos[1]+CHIPSIZE;
		
		pos = ChipToPosition(this,3 + (Player.shopLevel*2),1);
		this.kup[0] = pos[0];
		this.kup[1] = pos[1];
		this.kup[2] = pos[0]+CHIPSIZE;
		this.kup[3] = pos[1]+CHIPSIZE;
		
		pos = ChipToPosition(this,2 + (Player.shopLevel*1),0)
		this.tobira[0] = pos[0];
		this.tobira[1] = pos[1];
		this.tobira[2] = pos[0]+CHIPSIZE;
		this.tobira[3] = pos[1]+CHIPSIZE;
		
		//ショップの画像
		var shop = createMap(mapdata[SHOP_OFFSET + Player.shopLevel][0],this);
		shop.visible = true;
		shop.x += this.OFFSET_X;
		shop.y += this.OFFSET_Y;
		this.addChild(shop);
		
		//お店の扉
		this.door;
		//ショップレベルによって、扉の大きさが変化
		if(Player.shopLevel < 2)
		{
			var doorPos = ChipToPosition(this, 2 + (Player.shopLevel*1), 4 + (Player.shopLevel*2) );
			this.door = new Sprite(64,64);
			this.door.frame = 8;
			this.door.x = doorPos[0];
			this.door.y = doorPos[1];
		}else
		{
			this.door = new Sprite(64*3,64);
			this.door.frame = 5;
			var doorPos = ChipToPosition(this, 3 + (( Player.shopLevel-2)*1), 4 + (Player.shopLevel*2) );
			this.door.x = doorPos[0];
			this.door.y = doorPos[1];
		}
		this.door.image = getImage("silk_icon_merged.png");
		this.door.visible = false;
		this.addChild(this.door);
		
		
		pos = ChipToPosition(this,2+Player.shopLevel,1);
		//店主を作成
		var str = Player.round == 0?"店主1":"店主2";
		this.tensyu = game.NpcManager.CreateNpc(str,pos[0],pos[1]);
		this.tensyu.onScene(this);
		this.counterWeapon = new Sprite(64,64);	//カウンター前の武器
		this.counterWeapon.x = pos[0];
		this.counterWeapon.y = pos[1]+64;
		this.counterWeapon.visible = false;
		this.addChild(this.counterWeapon);
		str = Player.round == 0?"":"2";
		this.tensyu_imgs1 = [getImage("player"+str+"_3.png"),getImage("player"+str+"_4.png")];	//店番切り替えで使用
		str = Player.round == 0?"starting":"endless";
		this.tensyu_imgs2 = [getImage("house/"+str+"/c_wife_1.png"),getImage("house/"+str+"/c_wife_2.png")];
		//店番用各Sprtie
		s = this.m_main = new Sprite(512,90);	//mainWindow
		s.image = getImage("w_miseban.png");
		s.x = WIDTH/2-s.width/2;
		s.y = HEIGHT/2;
		s.visible = false;
		s = this.m_nums = [new Sprite(22,22),new Sprite(22,22)];
		var tp = [	[387,34],	[362,34],	];
		for(var i = 0; i < s.length; i++)
		{
			s[i].x = this.m_main.x + tp[i][0];
			s[i].y = this.m_main.y + tp[i][1];
			s[i].visible = false;
		}
		this.addChild(this.m_main);
		this.addChild(s[0]);
		this.addChild(s[1]);
		
		//子供を生成(HouseSceneで使ってる物を流用)
		if(Player.kid_call)
		{
			var str = "";
			var str_round = Player.round == 0 ? "starting" : "endless";
			
			var LV = Player.shopLevel;
			if(LV == 2)str = ["c_boy_1","c_boy_2"];
			if(LV == 3)str = ["c_man_1","c_man_2"];
			imageBox = [];
			for(var i = 0; i < str.length; i++)imageBox.push( getImage("house/"+str_round+"/"+str[i]+".png") );	//必要な画像を持ってきてpush
			s = this.sprite_kid = new HouseSprite(imageBox,64,64);
			s.x = this.door.x - 64;
			s.y = this.door.y - 64;
			s.visible = true;
			this.addChild(s);
		}
		
		
		//全ての客NPC、現在4番以下はイベントNPC
		this.npcs = new Array(NPC_MAX);
		var NPC = this.npcs;
		for(var i = 0; i < NPC.length; i++)
		{
			NPC[i] = game.NpcManager.CreateNpc("dummy");
			this.addChild(NPC[i]);
		}
		//インターフェイス
		var wx = pos[0]+CHIPSIZE/2;
		var wy = pos[1]+64*3;
		game.UI.ViewState(this,SCENE_STATE_SHOP,wx,wy);
		game.UI.ViewStateUpdate();
		
		if(Player.onIvent)
		{
			//イベントが発生中の場合の処理
			if(Player.story == 0)
			{
				//エンディングではないなら
				
				//イベント用のNPCを生成、さらにカウンター前にワープ
				var str;
				pos = ChipToPosition(this,2+Player.shopLevel,3);	//カウンター前の座標
				if(IVENT_NPCINDEX[Player.iventIndex] == 0)str = "勇者1";
				if(IVENT_NPCINDEX[Player.iventIndex] == 1)str = "兵隊長";
				var n =  game.NpcManager.CreateNpc(str,pos[0],pos[1]);
				var pn = NPC[0];
				game.NpcManager.setNpcforNpc(n,pn);
				NPC[0].state = "挨拶";
				
				if(str == "勇者1")
				{
					//勇者がPOPした場合、各条件によって仲間を生成
					if(IventToIndex("勇者") >= NPC_POPINDEX_WOR)
					{
						pos[0] += -64;	//勇者と横並びにする
						var n =  game.NpcManager.CreateNpc("ウォーリア1",pos[0],pos[1]);
						var pn = NPC[1];
						game.NpcManager.setNpcforNpc(n,pn);
						NPC[1].state = "停止";
					}
					
					if(IventToIndex("勇者") >= NPC_POPINDEX_REN)
					{
						pos[0] += 128;
						var n =  game.NpcManager.CreateNpc("レンジャー1",pos[0],pos[1]);
						var pn = NPC[2];
						game.NpcManager.setNpcforNpc(n,pn);
						NPC[2].state = "停止";
					}
				}
				
			}
		}
		
		this.addEventListener(Event.TOUCH_START, function(e){
			
			//ロックが掛かっている状態なら各処理をした後戻る
			if(this.touchLock){
				
				//鉱山の開放処理中でタップされたなら、LVを上昇させ画像を戻す
				if(this.mineExtendNow){
					Player.mineLevel = Player.mineLevelMax;
					this.mineExtendNow = false;
					game.UI.window_state = MESSAGE_STATE_END;
					this.touchLock = false;
				}
				return;
			}
			
			var clickPos = [e.localX,e.localY];
			//店番判定
			if(Player.mise_Message.know && !Player.onIvent)
			{
				Player.mise_Message.know = false;
				
				//このシーンをUIから持ってくる
				var self = game.UI.scene;
				
				this.m_main.visible = false;
				this.m_nums[0].visible = false;
				this.m_nums[1].visible = false;
				this.tensyu.images[0] = this.tensyu_imgs1[0];
				this.tensyu.images[1] = this.tensyu_imgs1[1];
				
				return;
			}
			
			
			if(Player.story != 1)game.UI.TouchButton(clickPos);	//ボタン判定
			//イベント制御
			if(Player.onIvent || Player.story == 1)
			{
				game.UI.TouchWindow(clickPos);	//window判定
			}
			
			//ステータス更新
			game.UI.ViewStateUpdate();
			
			if(!game.UI.getInterfaceIvent() && Player.story != 1)
			{
				//倉庫を開いている場合は処理しない
				if((e.localX>this.kdown[0]  && e.localX<this.kdown[2] )&&(e.localY>this.kdown[1]  && e.localY< this.kdown[3])){
				      game.pushScene(new FadeScene(kaziAssets,makeNewScene(SmithyScene)));		//下り階段
				      playSe("sound/move.wav");
				}
				if((e.localX>this.kup[0]  && e.localX<this.kup[2] )&&(e.localY>this.kup[1]  && e.localY< this.kup[3])){
				      game.pushScene(new FadeScene(houseAssets,makeNewScene(HouseScene)));	//上り階段
					  playSe("sound/move.wav");
				}
				if((e.localX>this.tobira[0]  && e.localX<this.tobira[2] )&&(e.localY>this.tobira[1]  && e.localY< this.tobira[3])){
				  	//UIの鉱山メニューを呼び出す
					if(!game.UI.MM.ivent)game.UI.MM.IventOn();
					if(game.UI.MWP.ivent)game.UI.MWP.IventOff();//品出しmenuが開かれているなら品出しmenuを閉じる
					playSe(se.window);
				}
			}
			
			
		});
		
		
		//入り口の座標を取得
		this.oSetY = 4+Player.shopLevel*2;//入り口の座標を取得
		this.oSetY *= 64;
		
		this.addEventListener(Event.ENTER_FRAME, function(e){
			//isdisplogo = true;//アプリ起動時にタイトルロゴ表示するか？
			if(isdisplogo  && this.age  === 1){
				isdisplogo = false;
				game.pushScene(new TitleScene());
                	}
			var popSec = NPC_POPSEC - (Player.shopLevel * NPC_POPSEC_LEN);	//来店頻度
			var ontime = Date.parse(new Date());
			var time = (ontime - game.Time) / 1000;		//経過時間
			time = Math.floor(time+1);
			var Player_Npcs = this.npcs;	//NPCの参照
			var noSlot = game.InventoryManager.getWeaponCount(-1) == 0;	//スロットに武器が入っているか否か
			
			game.UI.EnterFrame();	//InterFace更新処理
			
			//各種判定
			
			//子供の客呼び込み効果
			if(Player.kid_call)
			{
				popSec = 3;	//呼び込みしてる間は3固定
				
				//時間判定
				if(Player.kid_callTimer > ontime)
				{
					//呼び込み時間内なら何もしない
				}else
				{
					//超えたら、見えなくする
					this.sprite_kid.tl.fadeOut(6);
					Player.kid_call = false;
				}
				//子供アニメ
				this.sprite_kid.AnimeUpdate();
			}
			
			//鉱山開放判定
			if(Player.mineLevel < Player.mineLevelMax)
			{
				//もし鉱山のレベルが上がっていないならば速報を出す
				if(!this.mineExtendNow){
					var mtime = (new Date() - Player.mineLevelTime) / 1000;
					if(mtime > MINE_EXTEND_TIME){
						game.UI.setImage_window("速報");	//windowを速報画像に更新
						game.UI.window.visible = true;		//windowを見えるように
						this.touchLock = true;	//他のタッチ機能をOFFにしておく
						this.mineExtendNow = true;
						playSe(se.clear);
					}
				}
				noSlot = true;	//鉱山の開放が全て終了するまでは、客を入れないようにする
			}
			
			
			//各種イベントに入るか判断
			var hantei = false;
			var END = false;
			
			//ストーリーが終わったら通常営業エンドレス
			if(Player.story < 2)
			{
				hantei = IVENT_NORM[Player.iventIndex] <= Player.iventWeaponCount;	//売却した武器の数が一定数以上ならば
				END = !(Player.story == 0);
				hantei = hantei && !END;												//エンディングではないなら
				hantei = hantei && Player.shopLevel >= Player.shopLevelMax;				//店舗が最大まで拡張されているなら
			}
			
			if(hantei)Player.onIvent = true;
			
			this.door.visible = noSlot;//もしスロットに武器が入っていないなら扉を見せる
			console.log("店番関係変数_Know:"+Player.mise_Message.know);
			if(Player.mise_Message.know && !Player.onIvent)
			{
				//店番状態&&イベント中ではない 店番windowを描画する(タッチでfalseへ)
				if(!this.m_main.visible)this.Set_M_Window();	//描画セット(visible見て二度は入らない)
				
			}else if(time % popSec == 0 && !this.timeLock)
			{
				//一定期間毎に生成
				this.timeLock = true;	//1秒間に1回のみ処理するのでロックする
				//if(Player.story == 1 && game.UI.windowSequence == -1)game.UI.CallWindow("最後",Player.iventIndex);	//もし、次がエンディングなら最後のwindowを呼び出す、ここのwindowがクリックされてエンディングの挙動が始まる
				
				if(DEBUG)console.log("P_INDEX"+Player.iventIndex+" story"+Player.story+" END:"+END+" WINDOWSEQUENCE:"+game.UI.windowSequence+" NPC[0]USED:"+Player_Npcs[0].Used);
				
				if(END && !Player_Npcs[0].Used)
				{
					//エンディングNPC生成 && windowがクリックされた && NPCが生成されていない
					var x = this.tobira[0];
					var y = this.tobira[1]+this.oSetY;
					var n;
					var pn;
					
					str = "王様";
					pn = Player_Npcs[0];
					n= game.NpcManager.CreateNpc(str,x,y);
					game.NpcManager.setNpcforNpc(n,pn);
					y += 64;
					str = "勇者1";
					pn = Player_Npcs[1];
					n= game.NpcManager.CreateNpc(str,x,y);
					game.NpcManager.setNpcforNpc(n,pn);
					//x += -64;
					y += 64;
					str = "レンジャー1";
					pn = Player_Npcs[2];
					n= game.NpcManager.CreateNpc(str,x,y);
					game.NpcManager.setNpcforNpc(n,pn);
					//x += 128;
					y += 64;
					str = "ウォーリア1";
					pn = Player_Npcs[3];
					n= game.NpcManager.CreateNpc(str,x,y);
					game.NpcManager.setNpcforNpc(n,pn);
					//x += -64;
					y += 64;
					str = "兵隊長";
					pn = Player_Npcs[4];
					n= game.NpcManager.CreateNpc(str,x,y);
					game.NpcManager.setNpcforNpc(n,pn);

				}
				
				if(Player.onIvent)
				{
					//イベント中ならば各種特殊NPCをPOP
					this.Nomal_Pop_Inpc(Player_Npcs);
				}else if(!noSlot)
				{
					//イベントではなく商品が現存するなら通常NPCPOP
					//POPLOCKが掛かっていない尚且つスロットに武器が存在しているならば処理へ
					this.Nomal_Pop_npc(Player_Npcs);
				}
				
			}else if(time % popSec != 0)
			{
				this.timeLock = false;
			}
			
			//各Update
			if(Player.story == 0 || Player.story == 2)
			{
				//通常営業
				this.Nomal_Update(Player_Npcs);
			}else
			{
				//エンディング
				this.End_Update(Player_Npcs);
			}
			//アニメーション更新
			this.tensyu.ChangeImage();	//店主アニメ
			
			
			//イベントが開始されているなら処理を進める
			if(Player.onIvent || Player.story == 1)
			{
				//会話が終了したらイベント進行度を進める
				if(game.UI.windowSequence >= game.UI.windowMaxIndex)
				{
					game.UI.windowSequence = -1;	//シークエンスを-1化
					game.UI.window.visible = false;
					game.UI.window_state = MESSAGE_STATE_STOP;	//一応初期化
					Player.iventIndex++;
					Player.iventWeaponCount = 0;
					Player.onIvent = false;
					//IventNpcを見えなくしてfalse化
					for(var i = 0; i < NPC_STARTINDEX; i++)
					{
						if(Player_Npcs[i].Used)
						{
							Player_Npcs[i].visible = false;
							Player_Npcs[i].Used = false;
						}
					}
					//店舗拡張準備
					for(var i = 0; i <IVENT_SHOPLEVELUP.length; i++)
					{
						//もしイベントが拡張シーケンスならば拡張可能度を上昇させる
						if(IVENT_SHOPLEVELUP[i] == Player.iventIndex)
						{
							Player.shopLevelMax++;
							//UIの増築メニューの更新処理を直呼び出し
							game.UI.MSP.CheckCanShopExtend();
							game.UI.MSP.ChangeVisible();
						}
					}
					//鉱山の開放処理予約
					for(var i = 0; i < IVENT_MINELEVELUP.length; i++)
					{
						if(IVENT_MINELEVELUP[i] == Player.iventIndex)
						{
							Player.mineLevelMax++;	//鉱山上限LV上昇(現在のLVが上昇するわけではない)
							Player.mineLevelTime = Date.parse(new Date());	//整数を記憶するように
							game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
						}
					}
					
					//物語進行度一話終了でプロローグを呼び出す
					if(Player.iventIndex == 1)
					{
						game.pushScene(new SceneEvent("prologue"));
					}
					
					//もし全てのイベントが終了していたらフラグをエンディング中へ
					if(Player.iventIndex >= IVENT_NPCINDEX.length)
					{
						if(Player.story == 0)
						{
							Player.story = 1;	//エンディング中へ移行
							game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
							if(DEBUG)console.log("pusSceneSeq:"+game.UI.windowSequence);
						}else if(Player.story == 1)
						{
							Player.story = 2;	//エンディング終了
							game.replaceScene(new SceneEvent("epilogue"))
						}
					}
					
				}
			}
		
		});
		
		
		
		
	},
	
	Nomal_Pop_Inpc:function(NPC)
	{
		if(NPC[0].Used)return;
		//通常営業のイベントNPC生成
		var str;
		var x = this.tobira[0];
		var y = this.tobira[1]+this.oSetY;
		if(IVENT_NPCINDEX[Player.iventIndex] == 0)str = "勇者1";
		if(IVENT_NPCINDEX[Player.iventIndex] == 1)str = "兵隊長";
		var n =  game.NpcManager.CreateNpc(str,x,y);
		var pn = NPC[0];
		game.NpcManager.setNpcforNpc(n,pn);
		if(IVENT_NPCINDEX[Player.iventIndex] == 0)
		{
			//勇者がPOPした場合、各条件によって仲間を生成
			if(IventToIndex("勇者") >= NPC_POPINDEX_WOR)
			{
				y += 64;
				var n =  game.NpcManager.CreateNpc("ウォーリア1",x,y);
				var pn = NPC[1];
				game.NpcManager.setNpcforNpc(n,pn);
			}
			
			if(IventToIndex("勇者") >= NPC_POPINDEX_REN)
			{
				y += 64;
				var n =  game.NpcManager.CreateNpc("レンジャー1",x,y);
				var pn = NPC[2];
				game.NpcManager.setNpcforNpc(n,pn);
			}
		}
	},
	
	Nomal_Pop_npc:function(NPC)
	{
		//通常営業の通常NPC生成
		for(var i = NPC_STARTINDEX; i < NPC.length; i++)
		{
			if(!NPC[i].Used)
			{
				//使用していないNPCを検索
				var pn = NPC[i];
				game.NpcManager.CreateRandomNpcAndSet(pn);	//NPCをランダム生成
				pn.x = this.tobira[0];	//座標修正
				pn.y = this.tobira[1]+this.oSetY;
				break;
			}
		}
	},
	
	Nomal_Update:function(NPC)
	{
		//通常営業の処理
		//カウンター前の座標
		var counterPos = [this.tensyu.x,this.tensyu.y+64*2];
		
		//イベントNPC制御
		if(Player.onIvent && NPC[0].Used)
		{
			//マップ外描画判定
			for(var i = 0; i < NPC.length; i++)
			{
				if(NPC[i].Used)
				{
					NPC[i].visible = (NPC[i].y < this.tobira[1]+this.oSetY);
				}
			}
			
			if(NPC[0].y <= counterPos[1])
			{
				if(NPC[0].state == "入店")NPC[0].state = "挨拶";//イベントNPCがカウンターに到着
				
				for(var i = 1; i < NPC_STARTINDEX; i++)
				{
					if(NPC[i].Used)
					{
						//主に勇者以外の仲間NPCを、横並びにする
						if(NPC[0].state == "停止")
						{
							//勇者がカウンターに到着したら、各キャラを横に移動させて上へ
							var checkPos;
							if(i == 1)
							{
								//ウォーリア
								checkPos = NPC[0].x - CHIPSIZE;
								NPC[i].moveSpeed = checkPos < NPC[i].x ? [-6,0] : [0,-6];	//横に移動出来てないなら移動を横へ、そうでないなら移動が完了してるのでカウンター前へ
							}
							
							if(i == 2)
							{
								//レンジャー
								checkPos = NPC[0].x + CHIPSIZE;
								NPC[i].moveSpeed = checkPos > NPC[i].x ? [6,0] : [0,-6];	//横に移動出来てないなら移動を横へ、そうでないなら移動が完了してるのでカウンター前へ
							}
							
							if(NPC[0].y > NPC[i].y)NPC[i].state = "停止";	//カウンター前に着いたら停止
							
						}
					}
				}
				
			}
			
			for(var i = 0; i < NPC_STARTINDEX; i++)if(NPC[i].Used)NPC[i].Move();	//全てのイベントNPCの移動
		}
		
		
		//全てのお客NPCの位置を検索
		for(var i = NPC_STARTINDEX; i < NPC.length; i++)
		{
			if(NPC[i].Used)
			{
				var s = NPC[i];
				
				var nx = NPC[i].x;
				var ny = NPC[i].y;
				if(ny <= counterPos[1])
				{
					//NPCがカウンター前に来たら
					
					if(NPC[i].Check_CanBuy() && NPC[i].state == "入店")
					{
						//アイテムを購入できるなら
						//シーケンスを移行
						NPC[i].state = "武器を手に取る";
						NPC[i].setBuyRandomSlot();	//自分が購入可能なスロットの中から1つをセット
						var ID = Player.slot[0][NPC[i].buySlotIndex][0];
						var imageID = WEAPON_IMAGEID[ID];
						var str = "";
						if(imageID < 10)str = "weapon_0"+imageID+".png";
						if(imageID >= 10)str = "weapon_"+imageID+".png";
						this.counterWeapon.image = getImage(str);
						this.counterWeapon.visible = true;
						
					}else if(NPC[i].state == "入店")
					{
						//購入できなかったら出口へ
						NPC[i].state = "武器を見る";
					}
					game.UI.ViewStateUpdate();
				}
				
				if(ny > this.tobira[1]+this.oSetY)
				{
					//外へ出たら、
					NPC[i].visible = false;
					NPC[i].Used = false;
				}
				
				NPC[i].Move();	//NPCのシーケンス行動
				if(NPC[i].state == "購入")
				{
					//もしNPCが武器を購入したならばカウンターの武器を非表示
					this.counterWeapon.visible = false;
				}
			}
		}
		
		//アニメション
		for(var i = 0; i < NPC.length;i++)
		{
			//全てのNPCをアニメション
			if(NPC[i].Used)NPC[i].ChangeImage();
		}
	},
	
	End_Update:function(NPC)
	{
		//エンディングの処理
		
		//カウンター前の座標
		var counterPos = [this.tensyu.x,this.tensyu.y+64*2];
		
		//マップ外描画判定
		for(var i = 0; i < NPC.length; i++) if( NPC[i].Used ) NPC[i].visible = (NPC[i].y < this.tobira[1]+this.oSetY);
		
		if(NPC[0].y <= counterPos[1] && NPC[0].state == "入店")
		{
			if(NPC[0].state == "入店")
			{
				NPC[0].state = "挨拶";//イベントNPCがカウンターに到着
				//game.UI.windowLock = false;	//windowLockを外す
			}
			for(var i = 1; i < NPC_STARTINDEX; i++)NPC[i].state = "停止";	//全キャラを停止
			
		}
		for(var i = 0; i < NPC_STARTINDEX; i++)if(NPC[i].Used)NPC[i].Move();	//全てのイベントNPCの移動
		
		//アニメション
		for(var i = 0; i < NPC_STARTINDEX;i++)if(NPC[i].Used)NPC[i].ChangeImage();//全てのNPCをアニメション
	},
	
	Set_M_Window:function()
	{
		//店番用の数値を更新する
		var s =	this.m_nums;
		var num = Player.mise_Message.num;
		var n1,n2;
		n1 = Math.floor(num%10);
		n2 = Math.floor(num/10%10);
		
		s[0].image = getImage("s"+n1+".png");
		s[1].image = getImage("s"+n2+".png");
		
		//描画設定
		this.m_main.visible = true;
		this.m_nums[0].visible = true;
		if(n2 > 0)this.m_nums[1].visible = true;
		
		this.tensyu.images[0] = this.tensyu_imgs2[0];
		this.tensyu.images[1] = this.tensyu_imgs2[1];
		
		//mainwindowのアニメーション設定
		this.m_main.opacity = 0;
		this.m_nums[0].opacity = 0;
		this.m_nums[1].opacity = 0;
		this.m_main.tl.fadeIn(5).then(function(){playSe(se.tap_1);});
		this.m_nums[0].tl.fadeIn(5);
		this.m_nums[1].tl.fadeIn(5);
		
	},
	
});
