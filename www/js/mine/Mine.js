﻿var mineScene;

const MINE_STONE_TYPE_COAL = 0;
const MINE_STONE_TYPE_DIAMOND = 1;
const MINE_STONE_TYPE_DOU = 2;
const MINE_STONE_TYPE_TETU = 3;
const MINE_STONE_TYPE_MISURIRU = 4;
const MINE_STONE_TYPE_AMADAIN = 5;
const MINE_STONE_TYPE_ORIHARUKON = 6;
const MINE_STONE_TYPE_METEORON = 7;

Stone = Class.create(Sprite2,{
	initialize:function(){
	
		Sprite2.call(this, 64, 64);
		this.image = getImage('stone.png');

		this.addEventListener(Event.TOUCH_START, function(){
			this.visible = false;

			var addFlag = false;
			var smithyStoneType = changeSmithyStoneType(this.stoneType);

			if(smithyStoneType == SMITHY_STONE_COAL){
				playSe(se.put);
			}
			else{
				playSe(se.get);
			}
			
			// すでに獲得したことのアイテムかどうか調べて、
			// 獲得したことのあるアイテムなら＋１する
			for(var i = 0; i < mineScene.slotStones.length; i++){
				if(mineScene.slotStones[i].type == smithyStoneType){
					
					mineScene.slotStones[i].count += 1;
					addFlag = true;
					break;
				}
			}
			
			// はじめて獲得したアイテム
			if(!addFlag){
				
				// 空きスロットを探す
				for(var i = 0; i < mineScene.slotStones.length; i++){
					if(mineScene.slotStones[i].type == SMITHY_STONE_NONE){
						
						mineScene.slotStones[i].type = smithyStoneType;
						mineScene.slotStones[i].visible = true;
						mineScene.slotStones[i].count = 1;
						break;
					}
				}
			}
			
			// メッセージを表示する。
			mineScene.getMessage.disp(this.stoneType);

			switch(this.stoneType){
			case MINE_STONE_TYPE_COAL:
				Player.coal++;
				break;
			case MINE_STONE_TYPE_DIAMOND:
				Player.diamond++;
				break;
			case MINE_STONE_TYPE_DOU:
				Player.stone[STONE_DOU]++;
				Player.stoneGet[STONE_DOU] = true;
				break;
			case MINE_STONE_TYPE_TETU:
				Player.stone[STONE_TETU]++;
				Player.stoneGet[STONE_TETU] = true;
				break;
			case MINE_STONE_TYPE_MISURIRU:
				Player.stone[STONE_MISURIRU]++;
				Player.stoneGet[STONE_MISURIRU] = true;
				break;
			case MINE_STONE_TYPE_AMADAIN:
				Player.stone[STONE_AMADAIN]++;
				Player.stoneGet[STONE_AMADAIN] = true;
				break;
			case MINE_STONE_TYPE_ORIHARUKON:
				Player.stone[STONE_ORIHARUKON]++;
				Player.stoneGet[STONE_ORIHARUKON] = true;
				break;
			case MINE_STONE_TYPE_METEORON:
				Player.stone[STONE_METEORON]++;
				Player.stoneGet[STONE_METEORON] = true;
				break;
			}
		    game.UI.ViewStateUpdate();
		});
	},
	stoneType : {
		get : function() {
			return this._stoneType;
		},
		set : function(type) {
			this._stoneType = type;
			this.frame = type;
		}
	},
});

const GET_MESSAGE_STATE_INVISIBLE = 0;
const GET_MESSAGE_STATE_VISIBLE = 1;
const GET_MESSAGE_STATE_FADEOUT = 2;
GetMessage = Class.create(Sprite2,{
	initialize:function(){
	
		Sprite2.call(this, 370, 30);
		this.image = null;
		this.x = WIDTH / 2 - this.width / 2;
		this.y = 135;
		this.state = GET_MESSAGE_STATE_INVISIBLE;
		this.visibleWait = 0;
		this.opacity = 0;
		this.opacity2 = 0;

		this.addEventListener(Event.ENTER_FRAME, function(){
		
			switch(this.state){
			case GET_MESSAGE_STATE_VISIBLE:
				
				if(this.opacity2 < 1){
					this.opacity2 += 0.25;
				}
				else{
					this.opacity = 1;
				}
				
				if(FPS * 2 <= this.visibleWait){
					this.state = GET_MESSAGE_STATE_FADEOUT;
				}
				
				this.visibleWait++;
				break;
				
			case GET_MESSAGE_STATE_FADEOUT:
				
				this.opacity = 0;
				this.opacity2 = 0;
				this.state = GET_MESSAGE_STATE_INVISIBLE;
				break;
			}
		});
	},
	
	disp : function(id){

		var images = ['mine/w_material_0.png',
		              'mine/w_material_7.png',
		              'mine/w_material_1.png',
		              'mine/w_material_2.png',
		              'mine/w_material_3.png',
		              'mine/w_material_4.png',
		              'mine/w_material_5.png',
		              'mine/w_material_6.png',
		              'mine/w_material_8.png',
		              'mine/w_material_9.png',
		              'kazi/w_info_coal.png',];
		
		this.image = getImage(images[id]);
		this.state = GET_MESSAGE_STATE_VISIBLE;
		this.opacity = 0;
		this.opacity2 = 0;
		this.visibleWait = 0;
	},
});


BlockPiece = Class.create(Group2,{
	initialize:function(){
		Group2.call(this);
		this._blockType = 0;
		this.tick = 0;

		for(var i = 0; i < 4; i++){
			var sp = new Sprite2(24,24);
			sp.visible = false;
			this.addChild(sp);
		}
		this.addEventListener(Event.ENTER_FRAME, function(){
			var speed = 6;
			this.childNodes[0].x -= speed;
			this.childNodes[0].y -= speed;
			this.childNodes[1].x += speed;
			this.childNodes[1].y -= speed;
			this.childNodes[2].x -= speed;
			this.childNodes[2].y += speed;
			this.childNodes[3].x += speed;
			this.childNodes[3].y += speed;

			if(4 <= this.tick){
				this.visible = false;
			}
			this.tick++;
		});
	},
	blockType : {
		get : function() {
			return this._blockType;
		},
		set : function(type) {
			this._blockType = type;

			for(var i = 0; i < this.childNodes.length; i++){
				var piece = this.childNodes[i];
				switch(type){
				case 0:	piece.image = getImage('mine/block_piece.png');		break;
				case 1:	piece.image = getImage('mine/block_piece_b.png');	break;
				case 2:	piece.image = getImage('mine/block_piece_c.png');	break;
				}
			}
		}
	},
	start : function(x, y){
		this.visible = true;
		this.tick = 0;
		this.x = x;
		this.y = y;
		this.childNodes[0].setPos(0,0);
		this.childNodes[1].setPos(80,0);
		this.childNodes[2].setPos(0,80);
		this.childNodes[3].setPos(80,80);
	},
});

/**
 * ピッケルでブロックを破壊するときのポイント。
 * 合計 3 ポイントでブロックを破壊できる。
 */
const BLOCK_BREAK_POINT =[	[1,0,0],
							[2,1,0],
							[3,2,1],
							[3,3,2],
							[3,3,3],
							[3,3,3],
							];

Block = Class.create(Sprite2,{
	initialize:function(){
		Sprite2.call(this, 80, 80);
		this.image = getImage('block.png');
		this.breakPoint = 0;
		this._blockType = 0;

		this.addEventListener(Event.TOUCH_START, function(){

			if(Block.BLOCK_FRAME <= this.breakPoint){
				return;
			}
			
			// ピッケルのライフを減らす。
			var damage = 0;
			if(Player.pickel == PICKEL_METEORON && this.blockType == 0){
				damage = 1;
			}
			else if(BLOCK_BREAK_POINT[Player.pickel][this.blockType] != 0){
				
				damage = 2;
			}
			else{
				// 破壊できないブロックなのでライフは減らさない
				damage = 0;
			}

			// ピッケルのダメージよりピッケルのライフが多いかチェックする
			if(1 <= damage && damage <= mineScene.pickelLife){
				// ピッケルのライフを減らす
				mineScene.pickelLife -= damage;
				// ブロックの破壊値を増やす。
				this.breakPoint += BLOCK_BREAK_POINT[Player.pickel][this.blockType];
				
				playSe(se.break_);
				// 破片の表示
				var blockPiece = new BlockPiece();
				blockPiece.blockType = this.blockType;
				blockPiece.start(this.x,this.y);
				mineScene.blockPieceGroup.addChild(blockPiece);
			}
			else{
				playSe(se.unbreakable);
				if(damage == 0){
					mineScene.getMessage.disp(9);
				}
				else{
					mineScene.getMessage.disp(8);
				}
			}

			// ピッケルのライフを更新する
			for(var i = 0; i < 24; i++){
				if(i < (((mineScene.pickelLife + 1) / 2) | 0)){
					mineScene.pickelLifeGroup.childNodes[i].visible = true;
				}
				else{
					mineScene.pickelLifeGroup.childNodes[i].visible = false;
				}
			}

			// ブロックの表示を切り替える
			if(Block.BLOCK_FRAME <= this.breakPoint){
				this.visible = false;
			}
			else{
				this.frame = this._blockType * Block.BLOCK_FRAME + this.breakPoint;
			}
		});
	},
	blockType : {
		get : function() {
			return this._blockType;
		},
		set : function(type) {
			this._blockType = type;
			this.frame = type * Block.BLOCK_FRAME;
		}
	},
});
Block.BLOCK_FRAME = 3;


MineScene = Class.create(Scene,{
	initialize:function(stage){
		
		Scene.call(this);
		game.UI.ViewState(this,SCENE_STATE_MINE,0,0);
		game.UI.ViewStateUpdate();
		
		this.getMessage = new GetMessage();
		
		var bgm = [se.bgm_01,se.bgm_02,se.bgm_03,se.bgm_04,se.bgm_05];
		playBgm(bgm[stage]);
		
		var gr = new Group();
		gr.y = 32+48+80;
		this.addChild(gr);
		mineScene = this;
		
		mine = createSprite('mine.png',640, 704);
		mine.visible = true;
		gr.addChild(mine);
		
		this.pickel = new Sprite2(64,64);
		this.pickel.image = getImage('pickel_' + (Player.pickel + 1) + '.png'); // 画像ファイル名が 1 から始まる
		this.pickel.x = 6;
		this.pickel.y = 160;
		this.addChild(this.pickel);
		
		this.pickelLife = 48;

		this.blockGroup = new Group2();
		this.blockPieceGroup = new Group2();
		this.itemGroup = new Group2();
		gr.addChild(this.itemGroup);

		var diamondNum = [0,1,2,3,4];
		var stoneTable = [
				[2,2,2,2,2,2,2,3],
				[2,2,3,3,3,3,3,4],
				[2,3,3,4,4,4,4,5],
				[2,3,4,4,5,5,5,6],
				[3,4,5,5,6,6,6,7],
				];
		var blockTable = [
				[0,0],
				[4,0],
				[4,2],
				[8,2],
				[8,4],
				];
		var MINE_SPOT_NUM = 64;

		// 鉱石の配置
		var mineSpot = new Array(MINE_SPOT_NUM);
		var blocks = new Array(MINE_SPOT_NUM);
		for(var i = 0; i < mineSpot.length; i++){
			mineSpot[i] = -1;
			blocks[i] = 0;
		}
		for(var i = 0; i < stoneTable[stage].length; i++){
			var pos = rand(MINE_SPOT_NUM);
			if(mineSpot[pos] == -1){
				stone = new Stone();
				var x = pos % 8;
				var y = (pos / 8) | 0;
				stone.grid = x + y * 8;
				stone.stoneType = stoneTable[stage][i];
				mineSpot[pos] = stone.stoneType;
				stone.setPos(80 * x + 40, 80 * y + 64 + 40);
				this.itemGroup.addChild(stone);
			}
			else{
				// すでに配置している場所だった場合は配置しなおし。
				i--;
			}
		}

		// 石炭の配置
		for(var i = 0; i < MINE_SPOT_NUM; i++){
			if(2 <= mineSpot[i]){
				var x = i % 8;
				var y = (i / 8) | 0;
				if(x != 0){
					if(mineSpot[i-1] == -1){
						mineSpot[i-1] = 0;
					}
				}
				if(x != 7){
					if(mineSpot[i+1] == -1){
						mineSpot[i+1] = 0;
					}
				}
				if(y != 0){
					if(mineSpot[i-8] == -1){
						mineSpot[i-8] = 0;
					}
				}
				if(y != 7){
					if(mineSpot[i+8] == -1){
						mineSpot[i+8] = 0;
					}
				}
			}
		}
		// 一部の石炭をダイアモンドに変更する
		for(var i = 0; i < diamondNum[stage]; i++){
			var diamond = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			for(j = 0; j < MINE_SPOT_NUM; j++){
				if(mineSpot[(j + diamond) % MINE_SPOT_NUM] == 0){
					mineSpot[(j + diamond) % MINE_SPOT_NUM] = 1;
					break;
				}
			}
		}
		// 石炭、ダイアモンドの生成
		for(var i = 0; i < MINE_SPOT_NUM; i++){
			if(0 <= mineSpot[i] && mineSpot[i] <= 1){
				stone = new Stone();
				var x = i % 8;
				var y = (i / 8) | 0;
				stone.grid = x + y * 8;
				if(mineSpot[i] == 0){	stone.stoneType = 0;}
				else{					stone.stoneType = 1;}
				stone.setPos(80 * x + 40, 80 * y + 64 + 40);
				this.itemGroup.addChild(stone);
			}
		}

		// 岩B の配置
		num = (blockTable[stage][0] / 4) | 0;
		for(var i = 0; i < num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			for(j = 0; j < MINE_SPOT_NUM; j++){
				if(2 <= mineSpot[(j + mine) % MINE_SPOT_NUM] &&
					blocks[(j + mine) % MINE_SPOT_NUM] == 0){
					blocks[(j + mine) % MINE_SPOT_NUM] = 1;
					break;
				}
			}
		}
		for(var i = 0; i < blockTable[stage][0] - num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			if(mineSpot[mine] < 2 &&
				blocks[mine] == 0){
				blocks[mine] = 1;
			}
			else{
				i--;
			}
		}

		// 岩C の配置
		num = (blockTable[stage][1] / 2) | 0;
		for(var i = 0; i < num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			for(j = 0; j < MINE_SPOT_NUM; j++){
				if(2 <= mineSpot[(j + mine) % MINE_SPOT_NUM] &&
					blocks[(j + mine) % MINE_SPOT_NUM] == 0){
					blocks[(j + mine) % MINE_SPOT_NUM] = 2;
					break;
				}
			}
		}
		for(var i = 0; i < blockTable[stage][1] - num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			if(mineSpot[mine] < 2 &&
				blocks[mine] == 0){
				blocks[mine] = 2;
			}
			else{
				i--;
			}
		}

		// ブロックの配置
		for(var i = 0; i < MINE_SPOT_NUM; i++){
			block = new Block();
			var x = i % 8;
			var y = (i / 8) | 0;
			block.setPos(80 * x + 40, 80 * y + 64 + 40);
			block.blockType = blocks[i];
			this.blockGroup.addChild(block);
		}
		gr.addChild(this.blockGroup);

		this.pickelLifeGroup = new Group();
		for(var i = 0; i < 24; i++){
			var sp = new Sprite(16,40);
			sp.image = getImage('pickel_g.png');
			sp.visible = true;
			sp.x = 80 + 20 * i;
			sp.y = 12;
			this.pickelLifeGroup.addChild(sp);
		}
		gr.addChild(this.pickelLifeGroup);

		this.slotStones = new Array(8);
		for(var i = 0; i < this.slotStones.length; i++){
			this.slotStones[i] = new SmithySlotStone();
			this.slotStones[i].type = SMITHY_STONE_NONE;
			this.slotStones[i].x = 80 * i;
			this.slotStones[i].y = 48;
			this.slotStones[i].visible = false;
			this.addChild(this.slotStones[i]);
		}
		
		gr.addChild(this.blockPieceGroup);
		
		var test = new Sprite(80,64);
		test.visible = true;
		test.x = 80 * 7;
		test.addEventListener(Event.TOUCH_START, function(e){
			playSe(se.run);
			game.pushScene(new FadeScene(kaziAssets,makeNewScene(SmithyScene)));
			mineScene = null;
		});
		gr.addChild(test);
		
		this.addChild(this.getMessage);
		
		// 終了条件を監視する
		this.endWait = 20;
		this.addEventListener(Event.ENTER_FRAME, function(){
			
			var end = false;
			var nonBreak = false;
			
			// ピッケルライフが０じゃないか調べる。
			if(this.pickelLife == 0){
				nonBreak = true;
			}
			else{
				// 壊せるブロックがあるか調べる。
				var breakBlock = false;
				for(var i = 0; i < this.blockGroup.childNodes.length; i++){
					var child = this.blockGroup.childNodes[i];
					if(Player.pickel == PICKEL_METEORON && child.blockType == 0){
						breakBlock = true;
					}
					else if(BLOCK_BREAK_POINT[Player.pickel][child.blockType] != 0){
						if(2 <= this.pickelLife){
							breakBlock = true;
						}
					}
				}
				
				if(!breakBlock){
					nonBreak = true;
				}
			}
			
			// ブロックがこわせない状態
			// マップにまだ取ってないアイテムがあるか調べる
			if(nonBreak){
				end = true;
				for(var i = 0; i < this.itemGroup.childNodes.length; i++){
					var item = this.itemGroup.childNodes[i];
					// アイテムがまだ取られてない
					if(item.visible){
						// アイテムの上のブロックが壊されている
						if(!this.blockGroup.childNodes[item.grid].visible){
							end = false;
						}
					}
				}
			}
			
			// 終了
			if(end){
				// 少しまってからフェードアウト
				this.endWait--;
				if(this.endWait <= 0){
					playSe(se.move);
					game.pushScene(new FadeScene(kaziAssets,makeNewScene(SmithyScene)));
					mineScene = null;
				}
			}
		});
	},
});

function changeSmithyStoneType(type){
	
	var smithyType = [
	                  SMITHY_STONE_COAL,
	                  SMITHY_STONE_DIAMOND,
	                  SMITHY_STONE_DOU,
	                  SMITHY_STONE_TETU,
	                  SMITHY_STONE_MISURIRU,
	                  SMITHY_STONE_AMADAIN,
	                  SMITHY_STONE_ORIHARUKON,
	                  SMITHY_STONE_METEORON];
	
	return smithyType[type];
}

