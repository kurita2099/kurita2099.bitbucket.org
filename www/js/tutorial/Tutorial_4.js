﻿Tutorial_4 = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		//プレイヤーの持ち物操作
		for(var i = 0; i < Player.weapon.length; i++)Player.weapon[i] = i == 0?1:0;	//武器数
		for(var i = 0; i < Player.recipe.length; i++)Player.recipe[i] = i == 0?1:0;	//レシピ開放
		
		//this.shopScene = this;
		tutorial_scene = this;
		this.popLock = false;	//NPCのPOP制御
		playBgm(se.bgm_main);	//bgm
		this.tookIndex = 0;	//チュートリアル進行度、最初のメッセージ出現で1へ移行(進行:1.メッセージ出現、スロットタップで移行  2:品出し出現、武器項目タップで移行  3:×印タップで移行、客NPCがPOPするように  4:客が入ってくる、スロット中の武器の数が0になったら移行  5:delay後最後のメッセージ出現、出現したら移行, 6:タップしたら本編へ
		
		//ずらし
		this.OFFSET_X = 0;
		this.OFFSET_Y = INTERFACE_OFFSET_HEIGHT;
		
		//階段
		this.kup = [0,0,0,0];
		this.kdown = [0,0,0,0];
		//扉
		this.tobira = [0,0,0,0];
		
		
		//レベルに応じて階段座標を変更
		
		var pos;//計算した座標受け皿
		pos = ChipToPosition(this,1,1); 
		this.kdown[0] = pos[0];
		this.kdown[1] = pos[1];
		this.kdown[2] = pos[0]+CHIPSIZE;
		this.kdown[3] = pos[1]+CHIPSIZE;
		
		pos = ChipToPosition(this,3 + (Player.shopLevel*2),1);
		this.kup[0] = pos[0];
		this.kup[1] = pos[1];
		this.kup[2] = pos[0]+CHIPSIZE;
		this.kup[3] = pos[1]+CHIPSIZE;
		
		pos = ChipToPosition(this,2 + (Player.shopLevel*1),0)
		this.tobira[0] = pos[0];
		this.tobira[1] = pos[1];
		this.tobira[2] = pos[0]+CHIPSIZE;
		this.tobira[3] = pos[1]+CHIPSIZE;
		
		//ショップの画像
		var shop = createMap(mapdata[SHOP_OFFSET + Player.shopLevel][0]);
		shop.visible = true;
		shop.x += this.OFFSET_X;
		shop.y += this.OFFSET_Y;
		this.addChild(shop);
		
		pos = ChipToPosition(this,2+Player.shopLevel,1);
		//店主を作成
		var str = Player.round == 0?"店主1":"店主2";
		this.tensyu = game.NpcManager.CreateNpc(str,pos[0],pos[1]);
		this.tensyu.onScene(this);
		this.counterWeapon = new Sprite(64,64);	//カウンター前の武器
		this.counterWeapon.x = pos[0];
		this.counterWeapon.y = pos[1]+64;
		this.counterWeapon.visible = false;
		this.addChild(this.counterWeapon);
		
		//お店の扉
		this.door;
		//ショップレベルによって、扉の大きさが変化
		if(Player.shopLevel < 2)
		{
			var doorPos = ChipToPosition(this, 2 + (Player.shopLevel*1), 4 + (Player.shopLevel*2) );
			this.door = new Sprite(64,64);
			this.door.frame = 8;
			this.door.x = doorPos[0];
			this.door.y = doorPos[1];
		}else
		{
			this.door = new Sprite(64*3,64);
			this.door.frame = 5;
			var doorPos = ChipToPosition(this, 3 + (( Player.shopLevel-2)*1), 4 + (Player.shopLevel*2) );
			this.door.x = doorPos[0];
			this.door.y = doorPos[1];
		}
		this.door.image = getImage("silk_icon_merged.png");
		this.door.visible = false;
		this.addChild(this.door);
		
		
		//全ての客NPC、現在4番以下はイベントNPC
		this.npcs = new Array(NPC_MAX);
		var NPC = this.npcs;
		for(var i = 0; i < NPC.length; i++)
		{
			NPC[i] = game.NpcManager.CreateNpc("dummy");
			this.addChild(NPC[i]);
		}
		
		//インターフェイス
		var wx = pos[0]+CHIPSIZE/2;
		var wy = pos[1]+64*3;
		game.UI.ViewState(this,SCENE_STATE_SHOP,wx,wy);
		game.UI.ViewStateUpdate();
		
		//チュートリアル処理
		
		//チュートリアルメッセージ
		var s;
		s = this.sprite_main = new Sprite(512,200);
		s.image = getImage("tutorial/start_11.png");
		s.x = wx -s.width/2;
		s.y = wy;
		s.visible = false;
		this.addChild(s);
		s.tl.delay(TUTORIAL_STARTDELAY).then(function(){this.visible = true; tutorial_scene.tookIndex = 1; playSe(se.window);for(var i = 0; i < tutorial_scene.sprite_yubi_slots.length; i++)tutorial_scene.sprite_yubi_slots[i].visible = true;});//進行、音、指描画
		
		//指スプライト
		//スロット群
		var group = new Group();
		s = this.sprite_yubi_slots = new Array(8);
		for(var i = 0; i < s.length; i++)
		{
			s[i] = new Sprite(52,80);
			s[i].image = getImage("tutorial/hand_info.png");
			s[i].visible = false;
			s[i].x = game.InventoryManager.s_back[i].x;
			s[i].y = game.InventoryManager.s_back[i].y + game.InventoryManager.s_back[i].height + 12;
			s[i].x += game.InventoryManager.s_back[i].width/2;
			s[i].x += -(s[i].width/2);
			s[i].rotate(180);
			if(i == 0)s[i].tl.moveBy(0,YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,-YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();	//指示でとりあえず1個だけにした
			if(i == 0)group.addChild(s[i]);//上記と同上
		}
		this.addChild(group);
		//武器項目、ここはdomSprite
		s = this.sprite_yubi_index = createDomSprite("tutorial/hand_info.png",52,80);
		s.x = game.UI.MWP.sprite_blackBox[0].x - 40;
		s.y = game.UI.MWP.sprite_blackBox[0].y
		s.visible = false;
		s.rotate(-90);
		s.tl.moveBy(-YUBI_MOVE,0,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(YUBI_MOVE,0,YUBI_MOVE_PUSHFRAME).loop();
		this.addChild(s);
		//×印
		s = this.sprite_yubi_close = new Sprite(52,80);
		s.image = getImage("tutorial/hand_info.png");
		s.x = CLOSE_OFFSET_X - 70;
		s.y = CLOSE_OFFSET_Y + 26;;
		s.visible = false;
		s.rotate(-90);
		s.tl.moveBy(-YUBI_MOVE,0,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(YUBI_MOVE,0,YUBI_MOVE_PUSHFRAME).loop();
		this.addChild(s);
		
		this.addEventListener(Event.TOUCH_START, function(e){
			var clickPos = [e.localX,e.localY];
			
			//チュートリアル仕様のため、Interface中身の処理ぶっこ抜き
			//他のメニューが開かれていないかチェック(増築メニュー、その他メニュー)
			if(!game.UI.MSP.ivent && !game.UI.M_SONOTA.ivent && this.tookIndex == 1)
			{
				for(var i = 0; i < game.InventoryManager.s_back.length; i++)
				{
					//スロット内部を全て見ている
					if(game.UI.HitPos(game.InventoryManager.s_back[i],clickPos) && game.UI.sceneIndex == SCENE_STATE_SHOP)
					{
						if(game.UI.MWP.ivent)
						{
						}else if(!game.UI.MWP.ivent)
						{
							//品出しが開かれていなければ
							game.UI.MWP.IventOn(game.InventoryManager.s_back[i],i);	//品出し開始
							game.UI.MWP.Set_Handslot(game.InventoryManager.s_back[i],i);	//スロット記憶
							playSe(se.window);
							
							this.tookIndex = 2;
							this.sprite_main.visible = false;
							for(var i = 0; i < this.sprite_yubi_slots.length; i++)this.sprite_yubi_slots[i].visible = false;
							this.sprite_yubi_index.visible = true;
						}			
					}
				}
			
			}//各メニューチェック終了
			
			
			//品出しの処理ぶっこ抜き
			if(game.UI.MWP.ivent)
			{
				//スプライトのタッチ判定(タッチスタート)
				//UIの方にあるメソッドの参照を記憶
				var HIT = game.UI.HitPos;
				var s;
				var change = false;
				
				var bank = game.InventoryManager.getWeaponBank();	//全ての在庫取得
				var IDs = [];	//参照するIDを覚える
				var ID = 0;
				if(game.UI.MWP.index_tab == 2)ID += W_N;
				if(game.UI.MWP.index_tab == 3)ID += W_N+W_O;
				for(var i = 0; i < 5; i++)
				{
					//ページの項目分回す
					IDs.push(i + ID + (game.UI.MWP.index_page-1) * 5);
				}
				if(game.UI.MWP.index_tab == 4)
				{
					//伝説タブなら指定IDを覚える
					IDs = [W_N-1,  W_N+W_O-1,  W_N+W_O+W_S-1,  -1,-1];
				}
				
				//項目内の選択処理
				if(this.tookIndex == 2)
				{
					for(var i = 0; i < game.UI.MWP.sprite_blackBox.length; i++)
					{
						s = game.UI.MWP.sprite_blackBox[i];
						if(!s.visible && HIT(s,clickPos) )
						{
							//もし目隠しが取れているなら判定開始
							var id = IDs[i];	//クリックした項目の武器ID
							if(game.UI.MWP.handSlot[0] == id)
							{
								//もしidが同じなら倉庫へ戻す
								game.UI.MWP.handSlot[0] = -1;
								game.UI.MWP.handSlot[1] = -1;
								playSe(se.tap_2);
							}else if(bank[ IDs[i] ] > 0)
							{
								//在庫があるならスロットに入れる
								game.UI.MWP.handSlot[0] = id;
								game.UI.MWP.handSlot[1] = bank[IDs[i]];						//一つの在庫数取得
								if(game.UI.MWP.handSlot[1] >= SLOT_MAX_IN[Player.shopLevel] )game.UI.MWP.handSlot[1] = SLOT_MAX_IN[Player.shopLevel];	//もしスロットの最大数を超えていたら、最大数まで戻す
								playSe(se.tap_1);
								
								//チュートリアル
								this.tookIndex = 3;
								this.sprite_yubi_index.visible = false;
								this.sprite_yubi_close.visible = true;
							}
							game.UI.MWP.index_index = i+1;
							change = true;
							
						}
					}
				}
				
				
				//もし変更されるならImage更新
				if(change || game.UI.MWP.oneLoad)
				{
					game.UI.MWP.setImage();
					game.UI.MWP.oneLoad = false;
				}
				
				//×ボタンが押されたら
				s = game.UI.MWP.sprite_close;
				if(HIT(s,clickPos) && s.visible && this.tookIndex == 3)
				{
					game.UI.MWP.IventOff();
					playSe(se.tap_2);
					this.tookIndex = 4;
					this.sprite_yubi_close.visible = false;
					game.Time = Date.parse(new Date());	//時間を更新
				}
			}
			
			//ステータス更新
			game.UI.ViewStateUpdate();
			
			//チュートリアル終了処理(品出しなどがdomSpriteのためイベントリスナが使えないのでここへ）
			if(this.tookIndex == 6 && game.UI.HitPos(this.sprite_main,clickPos))
			{
				//最後のメッセージの状態、タップしたら本編へ
				game.pushScene(new FadeScene(shopAssets, makeNewScene(ShopScene)));
				tutorial_scene = null;	//使用したシーン変数を全て初期化
				ssmithyScene = null;	//念のため鍛冶屋シーン変数も初期化
				//playSe(se.move);
				Player.tutorial = false;	//チュートリアルを起動させない
			}
			
		});
		
		
		//入り口の座標を取得
		this.oSetY = 4+Player.shopLevel*2;//入り口の座標を取得
		this.oSetY *= 64;
		
		this.addEventListener(Event.ENTER_FRAME, function(e){
			game.UI.EnterFrame();
			var popSec = NPC_POPSEC - (Player.shopLevel * NPC_POPSEC_LEN);	//来店頻度
			var ontime = Date.parse(new Date());
			var time = (ontime - game.Time) / 1000;		//経過時間
			popSec = 3;	//チュートリアルでは3秒
			time = Math.floor(time+1);
			var Player_Npcs = this.npcs;	//NPCの参照
			
			//各種判定
			
			//武器を持っているか判断(毎フレームやるのは重いか・・・？
			var noSlot = game.InventoryManager.getWeaponCount(0) == 0 && game.InventoryManager.getWeaponCount(1) == 0 && game.InventoryManager.getWeaponCount(2) == 0;
			//もしスロットに武器が入っていないなら扉を見せる
			this.door.visible = noSlot;
			if(noSlot && this.tookIndex == 4)
			{
				this.tookIndex = 5;
				this.sprite_main.tl.delay(50).then(function(){this.image = getImage("tutorial/start_12.png");playSe(se.window);this.visible = true; tutorial_scene.tookIndex = 6;});
			}
			//一定期間毎に生成
			if(time % (popSec+1) == 0)
			{
				//そうでないならば通常NPCPOP
				if(!this.popLock && !noSlot)
				{
					//POPLOCKが掛かっていない尚且つスロットに武器が存在しているならば処理へ
					if(this.tookIndex == 4)this.Nomal_Pop_npc(Player_Npcs);
				}
				
			}else
			{
				//もし生成タイミングではないなら
				//時間で管理してるので同じ秒数なら複数回生成される、のでそれを防ぐ
				//目標時間外ならばロックを外す
				this.popLock = false;
			}
			
			//通常営業
			this.Nomal_Update(Player_Npcs);
			
			this.tensyu.ChangeImage();	//店主アニメ
		
		});
		
	},
	
	Nomal_Pop_npc:function(NPC)
	{
		//通常営業の通常NPC生成
		for(var i = NPC_STARTINDEX; i < NPC.length; i++)
		{
			if(!NPC[i].Used)
			{
				//使用していないNPCを検索
				var pn = NPC[i];
				game.NpcManager.CreateRandomNpcAndSet(pn,"レンジャー2");	//NPCをランダム生成
				pn.x = this.tobira[0];	//座標修正
				pn.y = this.tobira[1]+this.oSetY;
				this.popLock = true;	//ロックを掛ける
				break;
			}
		}
	},
	
	Nomal_Update:function(NPC)
	{
		//通常営業の処理
		//カウンター前の座標
		var counterPos = [this.tensyu.x,this.tensyu.y+64*2];
		
		//イベントNPC制御
		if(Player.onIvent && NPC[0].Used)
		{
			//マップ外描画判定
			for(var i = 0; i < NPC.length; i++)
			{
				if(NPC[i].Used)
				{
					NPC[i].visible = (NPC[i].y < this.tobira[1]+this.oSetY);
				}
			}
			
			if(NPC[0].y <= counterPos[1])
			{
				if(NPC[0].state == "入店")NPC[0].state = "挨拶";//イベントNPCがカウンターに到着
				
				for(var i = 1; i < NPC_STARTINDEX; i++)
				{
					if(NPC[i].Used)
					{
						//主に勇者以外の仲間NPCを、横並びにする
						if(NPC[0].state == "停止")
						{
							//勇者がカウンターに到着したら、各キャラを横に移動させて上へ
							var checkPos;
							if(i == 1)
							{
								//ウォーリア
								checkPos = NPC[0].x - CHIPSIZE;
								NPC[i].moveSpeed = checkPos < NPC[i].x ? [-6,0] : [0,-6];	//横に移動出来てないなら移動を横へ、そうでないなら移動が完了してるのでカウンター前へ
							}
							
							if(i == 2)
							{
								//レンジャー
								checkPos = NPC[0].x + CHIPSIZE;
								NPC[i].moveSpeed = checkPos > NPC[i].x ? [6,0] : [0,-6];	//横に移動出来てないなら移動を横へ、そうでないなら移動が完了してるのでカウンター前へ
							}
							
							if(NPC[0].y > NPC[i].y)NPC[i].state = "停止";	//カウンター前に着いたら停止
							
						}
					}
				}
				
			}
			
			for(var i = 0; i < NPC_STARTINDEX; i++)if(NPC[i].Used)NPC[i].Move();	//全てのイベントNPCの移動
		}
		
		
		//全てのお客NPCの位置を検索
		for(var i = NPC_STARTINDEX; i < NPC.length; i++)
		{
			if(NPC[i].Used)
			{
				var s = NPC[i];
				
				var nx = NPC[i].x;
				var ny = NPC[i].y;
				if(ny <= counterPos[1])
				{
					//NPCがカウンター前に来たら
					
					if(NPC[i].Check_CanBuy() && NPC[i].state == "入店")
					{
						//アイテムを購入できるなら
						//シーケンスを移行
						NPC[i].state = "武器を手に取る";
						NPC[i].setBuyRandomSlot();	//自分が購入可能なスロットの中から1つをセット
						var ID = Player.slot[0][NPC[i].buySlotIndex][0];
						var imageID = WEAPON_IMAGEID[ID];
						var str = "";
						if(imageID < 10)str = "weapon_0"+imageID+".png";
						if(imageID >= 10)str = "weapon_"+imageID+".png";
						this.counterWeapon.image = getImage(str);
						this.counterWeapon.visible = true;
						
					}else if(NPC[i].state == "入店")
					{
						//購入できなかったら出口へ
						NPC[i].state = "武器を見る";
					}
					game.UI.ViewStateUpdate();
				}
				
				if(ny > this.tobira[1]+this.oSetY)
				{
					//外へ出たら、
					NPC[i].visible = false;
					NPC[i].Used = false;
				}
				
				NPC[i].Move();	//NPCのシーケンス行動
				if(NPC[i].state == "購入")
				{
					//もしNPCが武器を購入したならばカウンターの武器を非表示
					this.counterWeapon.visible = false;
				}
			}
		}
		
		//アニメション
		for(var i = 0; i < NPC.length;i++)
		{
			//全てのNPCをアニメション
			if(NPC[i].Used)NPC[i].ChangeImage();
		}
	},
	
});
