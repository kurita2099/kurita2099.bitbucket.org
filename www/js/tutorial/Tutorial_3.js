﻿// 鍛冶シーンクラス
Tutorial_3 = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		Player.stone[0] = 1;
		Player.coal = 1;
		Player.stoneGet[0] = true;
		this.tookIndex = 0;//流れ；1.初期メッセージ出現、石炭投入で移行　2.鉱石がタップできるように、タップで移行　3.作るボタンが押せるように、押したら移動　4.時間が経過すると生成するボタンが出現、生成ボタンを押したら移行 5:階段か下menuをタップできるように、タップしたら次のチュートリアルへ
		tutorial_scene = this;
		smithyScene = this;

		this.OFFSET_X = 0;
		this.OFFSET_Y = INTERFACE_OFFSET_HEIGHT;
		
		this.smithyLevel = Player.shopLevel;

		this.smithyMain = new Group();
		this.kamaGroup = new Group();
		this.cursor = new KamaCursor('kazi/cursor.png');
		this.cursor1 = new KamaCursor('kazi/cursor.png');
		this.choiceStoneGroup = new TutorialChoiceStoneGroup();
		this.recipeTable = new RecipeTable();
		this.coalBox = new TutorialCoalBox();
		this.recipeGroup = new RecipeGroup(this.smithyMain);
		this.tong = new Sprite2(64,192);
		this.coal = new Sprite2(64,64);
		this.dropStoneGroup = new DropStoneGroup();
		this.makeCompleteGroup = new MakeCompleteGroup();
		this.makeInfoDialog = new TutorialMakeInfoDialog();
		this.addCoalDialog = new AddCoalDialog();
		this.coalNum = new Number();
		this.recipeNum = new Number();
		this.message = new GetMessage();
		this.addChild(this.message);

		this.makeCompleteGroup.close();
		this.choiceStoneGroup.close();
		this.recipeGroup.close();

		this.smithyMain.x = 0;
		this.smithyMain.y = INTERFACE_OFFSET_HEIGHT;
		
		this.addChild(this.smithyMain);
		
		//メニュークリック
		this.addEventListener(Event.TOUCH_START, function(e){
			//各種ボタンがクリックされた場合の処理
			var clickPos = [e.localX,e.localY];
			//Menuボタン判定
			for(var i = 0; i < game.UI.buttonData.length; i++)
			{
				var s = game.UI.buttonData[i];
				if(game.UI.HitPos(s,clickPos) && this.tookIndex == 5)
				{
					//自身がクリックされて尚且つwindowがクリック可能シーケンスなら
					switch(i)
					{
						case 2:	//店舗
						{
						　　game.pushScene(new FadeScene(tutorial_4_Assets, makeNewScene(Tutorial_4)));
						　　smithyScene = null;
						　　playSe(se.move);
							break;
						}
					}
				}
			}
		
		});
		// のぼり階段
		this.upStairs = new Sprite(64,64);
		this.upStairs.visible = true;
		this.upStairs.addEventListener(Event.TOUCH_START, function(e){
			if(!game.UI.getInterfaceIvent() && tutorial_scene.tookIndex == 5)
			{
				
				playSe(se.move);
				game.pushScene(new FadeScene(tutorial_4_Assets, makeNewScene(Tutorial_4)));
				smithyScene = null;
			}
		});

		var kama;
		this.smithy = createMap(mapdata[WORK_OFFSET + this.smithyLevel][0]);

		this.smithy.visible = true;
		this.smithyMain.addChild(this.smithy);
		
		// 石炭の箱
		this.coalBox.x = 224;
		this.coalBox.y = 384;
		this.coalBox.visible = true;
		this.smithyMain.addChild(this.coalBox);
		this.smithyMain.addChild(this.coalBox.touchArea);
		
		// 釜を並べる。
		kama = new TutorialKama();
		kama.x = 160 + 64 * 2;
		kama.y = 192 +  64 * 2;
		kama.visible = true;
		this.kamaGroup.addChild(kama);
		this.smithyMain.addChild(this.kamaGroup);

		// レシピの棚
		this.recipeTable.x = 352;
		this.recipeTable.y = 384;
		this.recipeTable.visible = true;

		// のぼり階段
		this.upStairs.x = 224;
		this.upStairs.y = 256;
			
		this.smithyMain.addChild(this.upStairs);
		
		// 石炭の数
		this.smithyMain.addChild(this.coalNum);
		this.coalNum.figure = 2;
		if(99 < Player.coal){
			this.coalNum.number = 99;
		}
		else{
			this.coalNum.number = Player.coal;
		}
		this.coalNum.x = this.coalBox.x + 16;
		this.coalNum.y = this.coalBox.y + 40;
		this.coalBox.touchArea.x = this.coalBox.x - 64;
		this.coalBox.touchArea.y = this.coalBox.y - 64;
		
		//  作成した武器の数
		this.smithyMain.addChild(this.recipeNum);
		this.recipeNum.figure = 2;
		this.recipeNum.number = this.recipeGroup.getMakedNum();
		this.recipeNum.x = this.recipeTable.x + 16;
		this.recipeNum.y = this.recipeTable.y + 40;

		// レシピテーブルの生成。
		this.smithyMain.addChild(this.recipeTable);
		//this.smithyMain.addChild(this.recipeTable.touchArea);
		this.recipeTable.touchArea.x = this.recipeTable.x;
		this.recipeTable.touchArea.y = this.recipeTable.y;

		this.coal.image = getImage('stone_0.png');
		this.coal.visible = false;
		this.coal.originPosY = -160;
		this.smithyMain.addChild(this.coal);

		this.tong.image = getImage('kazi/tong.png');
		this.tong.visible = false;
		this.tong.originPosY = -160;
		this.smithyMain.addChild(this.tong);

		game.UI.ViewState(this,SCENE_STATE_SMITHY,0,0);
		game.UI.ViewStateUpdate();
		
		//チュートリアル処理
		//メッセージスプライト
		var s = this.sprite_main = new Sprite(512,200);
		s.image = getImage("tutorial/start_6.png");
		s.visible = true;
		var pos = ChipToPosition(this,2+Player.shopLevel,1);
		var wx = pos[0]+CHIPSIZE/2;
		var wy = pos[1]+64*3;
		s.x = wx-s.width/2;	//PositionはInterfaceのメッセージ位置と同じ
		s.y = wy;
		s.visible = false;
		this.addChild(s);
		s.tl.delay(TUTORIAL_STARTDELAY).then(function(){tutorial_scene.tookIndex = 1;this.visible = true;playSe(se.window);tutorial_scene.sprite_yubi_1.visible = true;});
		
		// 釜の選択カーソル
		this.cursor.visible = false;
		this.smithyMain.addChild(this.cursor);
		this.cursor1.visible = false;
		this.smithyMain.addChild(this.cursor1);
		for(var i = 0; i < this.choiceStoneGroup.slotMaterial.length; i++){
			this.smithyMain.addChild(this.choiceStoneGroup.slotMaterial[i]);
		}
		
		// 釜のタッチエリアの設定。
		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){
			this.kamaGroup.childNodes[i].touchArea.x = this.kamaGroup.childNodes[i].x;
			this.kamaGroup.childNodes[i].touchArea.y = this.kamaGroup.childNodes[i].y;
			this.smithyMain.addChild(this.kamaGroup.childNodes[i].touchArea);
		}
		
		// 釜ダイアログの生成。
		this.kamaDialog = new KamaDialog();
		this.smithyMain.addChild(this.kamaDialog);

		// 鉱石の選択
		this.addChild(this.choiceStoneGroup.message);
		this.addChild(this.choiceStoneGroup.noTouchArea);
		this.addChild(this.choiceStoneGroup.make);
		this.addChild(this.choiceStoneGroup.cancel);
		
		// 鉱石
		this.slotStones = new Array(6);
		for(var i = 0; i < this.slotStones.length; i++){
			this.slotStones[i] = new SmithySlotStone();
			this.slotStones[i].type = i;
			this.slotStones[i].count = Player.stone[i];
			this.slotStones[i].x = 80 * i;
			this.slotStones[i].y = 48;
			this.addChild(this.slotStones[i]);
			this.slotStones[i].addEventListener(Event.TOUCH_START, function(e){
			
				if(tutorial_scene.tookIndex != 2)return;
				// 製作画面中
				if(smithyScene.choiceStoneGroup.visible){
					// 保持しているときのみ追加できる
					if(0 < this.count){	
						if(smithyScene.dropStoneGroup.addStone(this.type)){
							// 追加できたら個数を減らす
							smithyScene.choiceStoneGroup.visibleNofusion(false);
							this.count -= 1;
						}
					}
				}
				// 通常画面中
				else{
					
					if(smithyScene.choiceStoneGroup.selectKama != null){
						smithyScene.choiceStoneGroup.open(smithyScene.choiceStoneGroup.selectKama);
						// 保持しているときのみ追加できる
						if(0 < this.count){	
							if(smithyScene.dropStoneGroup.addStone(this.type)){
								// 追加できたら個数を減らす
								smithyScene.choiceStoneGroup.visibleNofusion(false);
								this.count -= 1;
								
								tutorial_scene.tookIndex = 3;
								tutorial_scene.sprite_main.image = getImage("tutorial/start_8.png");
								tutorial_scene.sprite_yubi_2.visible = false;
								tutorial_scene.sprite_yubi_3.visible = true;
							}
						}
					}
					else{
						var add = false;
						var fireOff = false;
						
						for(var j = 0; j < smithyScene.kamaGroup.childNodes.length; j++){
							
							var kama = smithyScene.kamaGroup.childNodes[j];
							if(	kama.state != KAMA_LEVEL_NONE &&	// 釜を購入
								kama.make == KAMA_MAKE_NONE &&		// 製作中ではない
								kama.fire == KAMA_FIRE_NONE){		// 火が消えている
								fireOff = true;
							}
						}
						
						for(var j = 0; j < smithyScene.kamaGroup.childNodes.length; j++){
							
							var kama = smithyScene.kamaGroup.childNodes[j];
							if(	kama.state != KAMA_LEVEL_NONE &&	// 釜を購入
								kama.make == KAMA_MAKE_NONE &&		// 製作中ではない
								kama.fire != KAMA_FIRE_NONE){		// 火がついている
								
								smithyScene.choiceStoneGroup.open(kama);
								// 保持しているときのみ追加できる
								if(0 < this.count){	
									if(smithyScene.dropStoneGroup.addStone(this.type)){
										// 追加できたら個数を減らす
										smithyScene.choiceStoneGroup.visibleNofusion(false);
										this.count -= 1;
										add = true;
										break;
									}
								}
							}
						}
						
						if(!add && fireOff){
							playSe(se.tap_2);
							smithyScene.message.disp(10);
						}
					}
				}
			});
			
			this.slotStones[i].visible = Player.stoneGet[i];
		}

		// レシピの生成。
		this.recipeGroup.visible = false;
		this.recipeGroup.y = this.smithyMain.y + 128;
		this.addChild(this.recipeGroup.noTouchArea);
		this.addChild(this.recipeGroup);
		this.addChild(this.recipeGroup.cancel);
		
		// 釜に投入する鉱石
		this.addChild(this.dropStoneGroup);
		
		// 釜購入のボタン
		this.addChild(this.kamaDialog.noTouchArea);
		this.addChild(this.kamaDialog.diamond1Sp);
		this.addChild(this.kamaDialog.diamond3Sp);
		this.addChild(this.kamaDialog.closeSp);

		// 武器製作完了
		this.addChild(this.makeCompleteGroup.weapon);
		this.addChild(this.makeCompleteGroup.message);
		this.addChild(this.makeCompleteGroup.cursor);
		this.addChild(this.makeCompleteGroup.closeArea);

		this.addChild(this.makeInfoDialog.noTouchArea);
		this.addChild(this.makeInfoDialog.closeSp);
		this.addChild(this.makeInfoDialog);
		
		this.addChild(this.addCoalDialog.noTouchArea);
		this.addChild(this.addCoalDialog);
		
		//チュートリアルスプライト
		//石炭箱-釜
		var s = this.sprite_yubi_1 = new Sprite(52,80);
		s.image = getImage("tutorial/hand_info.png");
		s.x = this.coalBox.x + this.coalBox.width/2;
		s.y = this.coalBox.y + 64;
		s.x += -(s.width/2);
		s.visible = false;
		this.addChild(s);
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		//鉱石
		s = this.sprite_yubi_2 = new Sprite(52,80);
		s.image = getImage("tutorial/hand_info.png");
		s.x = game.InventoryManager.s_back[0].x + game.InventoryManager.s_back[0].width/2;
		s.x += -(s.width/2);
		s.y = game.InventoryManager.s_back[0].y + game.InventoryManager.s_back[0].height + 12;
		s.visible = false;
		s.rotate(180);
		this.addChild(s);
		s.tl.moveBy(0,YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,-YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		//作る-生成
		s = this.sprite_yubi_3 = new Sprite(52,80);
		s.image = getImage("tutorial/hand_info.png");
		s.x = this.makeInfoDialog.w_w1_2.x - s.width;
		s.y = this.kamaGroup.childNodes[0].y + (this.kamaGroup.childNodes[0].height / 2) + smithyScene.smithyMain.y;
		s.visible = false;
		s.rotate(-90);
		this.addChild(s);
		s.tl.moveBy(-YUBI_MOVE,0,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(YUBI_MOVE,0,YUBI_MOVE_PUSHFRAME).loop();
		//階段
		s = this.sprite_yubi_4 = new Sprite(52,80);
		s.image = getImage("tutorial/hand_info.png");
		s.x = this.upStairs.x;
		s.y = this.upStairs.y + 32;
		s.visible = false;
		//this.addChild(s);
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		//店舗
		s = this.sprite_yubi_5 = new Sprite(52,80);
		s.image = getImage("tutorial/hand_info.png");
		s.x = game.UI.buttonData[2].x + game.UI.buttonData[2].width/2;
		s.y = game.UI.buttonData[2].y -s.height;
		s.x += -(s.width/2);
		s.visible = false;
		this.addChild(s);
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();

		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){
			var kama = this.kamaGroup.childNodes[i];
			kama.state			= Player.kama[i].state;
			kama.makeStone1		= Player.kama[i].makeStone1;
			kama.makeStone2		= Player.kama[i].makeStone2;
			kama.makeStartTime	= Player.kama[i].makeStartTime;
			kama.make			= Player.kama[i].make;
			kama.fireOffTime	= Player.kama[i].fireOffTime;
			kama.fireTime		= Player.kama[i].fireTime;
			kama.fire			= Player.kama[i].fire;
		}
		
		// 下タブメニュー対応。
		this.saveWait = 0;
		this.addEventListener(Event.ENTER_FRAME, function(){
			
			if(this.saveWait == 5){
				this.save();
				this.saveWait = 0;
			}
			else{
				this.saveWait++;
			}
		});
	},

	save : function(){

		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){

			var kama = this.kamaGroup.childNodes[i];

			Player.kama[i].state			= kama.state;
			Player.kama[i].makeStone1		= kama.makeStone1;
			Player.kama[i].makeStone2		= kama.makeStone2;
			Player.kama[i].makeStartTime	= kama.makeStartTime;
			Player.kama[i].make				= kama.make;
			Player.kama[i].fireOffTime		= kama.fireOffTime;
			Player.kama[i].fireTime			= kama.fireTime;
			Player.kama[i].fire				= kama.fire;
		}
	},

	load : function(){

		for(var i = 0; i < this.kamaGroup.childNodes.length; i++){

			var kama = this.kamaGroup.childNodes[i];

			kama.state			= Player.kama[i].state;
			kama.makeStone1		= Player.kama[i].makeStone1;
			kama.makeStone2		= Player.kama[i].makeStone2;
			kama.makeStartTime	= Player.kama[i].makeStartTime;
			kama.make			= Player.kama[i].make;
			kama.fireOffTime	= Player.kama[i].fireOffTime;
			kama.fireTime		= Player.kama[i].fireTime;
			kama.fire			= Player.kama[i].fire;
			kama.resume(new Date().getTime());
		}
	},

	initKama : function(){

		var kama;
		var group = new Group();

		this.smithyMain.insertBefore(group, this.kamaGroup);
		this.smithyMain.removeChild(this.kamaGroup);
		this.kamaGroup = group;
		// 釜を並べる。
		kama = new Kama();
		kama.x = 160 + 64 * 2;
		kama.y = 192 +  64 * 2;
		kama.visible = true;
		this.kamaGroup.addChild(kama);
	},


	onPause : function(){
	},

	onResume : function(){
	},
});
