
TutorialMakeInfoDialog = Class.create(Group,{
	initialize:function(){
		Group.call(this);

		this.closeSp = new Sprite2(90,90);
		this.closeSp.image = getImage('close.png');
		this.closeSp.x = 540;
		this.closeSp.y = 134;
		this.closeSp.visible = false;
		this.closeSp.addEventListener(Event.TOUCH_START, function(e){
		});
		
		this.noTouchArea = new Sprite(WIDTH, HEIGHT);
		this.noTouchArea.visible = false;
		this.noTouchArea.addEventListener(Event.TOUCH_START, function(e){
		});

		this.noTouchArea.addEventListener(Event.ENTER_FRAME, function(e){

			var instance = smithyScene.makeInfoDialog;
			
			if(instance.noTouchArea.visible == false){
				return;
			}
			
			// 鉄の釜で火が消えた場合は石炭追加のダイアログを表示する。
			if(instance.kama.fire == KAMA_FIRE_NONE){
				if(instance.kama.state == KAMA_LEVEL_1){
					if(instance.w_w1_2.visible == false &&
						instance.w_w2_2.visible == false &&
						instance.w_w3_2.visible == false){
						smithyScene.addCoalDialog.open(instance.kama);
						instance.close();
						return;
					}
					else{
						return;
					}
				}
			}

			var time = instance.kama.makeTime() | 0;
			var time1 = 0;
			var time2 = 0;

			if(instance.kama.makeStone2 == SMITHY_STONE_NONE){
				time1 = ((instance.kama.getKnifeTime() - time) / 1000);
				if(time1 < 0)	{	time1 = -1;}
				else			{	time1 |= 0;}
			}
			else{
				time1 = ((instance.kama.getSwordTime() - time) / 1000);
				time2 = ((instance.kama.getAxeTime() - time) / 1000);
				if(time1 < 0)	{	time1 = -1;}
				else			{	time1 |= 0;}
				if(time2 < 0)	{	time2 = -1;}
				else			{	time2 |= 0;}
			}

			time1++;
			time2++;

			if(time1 <= 0){
				time1 = 0;
				for(var i = 0; i < instance.time[0].length; i++){
					instance.time[0][i].visible = false;
				}

				if(instance.kama.makeStone2 == SMITHY_STONE_NONE){
					instance.w_w1_1.visible = false;
					instance.w_w1_2.visible = true;
				}
				else{
					instance.w_w3_1.visible = false;
					instance.w_w3_2.visible = true;
				}
			}

			if(time2 <= 0){
				time2 = 0;
				for(var i = 0; i < instance.time[1].length; i++){
					instance.time[1][i].visible = false;
				}
				instance.w_w2_1.visible = false;
				instance.w_w2_2.visible = true;
			}

			instance.time[0][0].image = getImage('s' + ((((time1 / 60) / 10) % 10) | 0) + '.png');
			instance.time[0][1].image = getImage('s' + (((time1 / 60) % 10) | 0) + '.png');
			instance.time[0][2].image = getImage('s' + ((((time1 % 60) / 10) % 10) | 0) + '.png');
			instance.time[0][3].image = getImage('s' + (((time1 % 60) % 10) | 0) + '.png');
			instance.time[1][0].image = getImage('s' + ((((time2 / 60) / 10) % 10) | 0) + '.png');
			instance.time[1][1].image = getImage('s' + (((time2 / 60) % 10) | 0) + '.png');
			instance.time[1][2].image = getImage('s' + ((((time2 % 60) / 10) % 10) | 0) + '.png');
			instance.time[1][3].image = getImage('s' + (((time2 % 60) % 10) | 0) + '.png');
		});

		this.w_w1_1 = new Sprite2(250, 90);
		this.w_w1_1.image = getImage('kazi/w_w1_1.png');
		this.w_w1_1.visible = false;
		this.w_w1_1.setPos(WIDTH / 2, 0);
		this.w_w1_2 = new Sprite2(250, 90);
		this.w_w1_2.image = getImage('kazi/w_w1_2.png');
		this.w_w1_2.visible = false;
		this.w_w1_2.setPos(WIDTH / 2, 0);
		this.w_w2_1 = new Sprite2(250, 90);
		this.w_w2_1.image = getImage('kazi/w_w2_1.png');
		this.w_w2_1.visible = false;
		this.w_w2_1.x = 64;
		this.w_w2_2 = new Sprite2(250, 90);
		this.w_w2_2.image = getImage('kazi/w_w2_2.png');
		this.w_w2_2.visible = false;
		this.w_w2_2.x = 64;
		this.w_w3_1 = new Sprite2(250, 90);
		this.w_w3_1.image = getImage('kazi/w_w3_1.png');
		this.w_w3_1.visible = false;
		this.w_w3_1.x = 325;
		this.w_w3_2 = new Sprite2(250, 90);
		this.w_w3_2.image = getImage('kazi/w_w3_2.png');
		this.w_w3_2.visible = false;
		this.w_w3_2.x = 325;

		this.time = new Array(2);
		for(var i = 0; i < this.time.length; i++){
			this.time[i] = new Array(4);
			for(var j = 0; j < this.time[i].length; j++){
				this.time[i][j] = new Sprite2(22,22);
				this.time[i][j].visible = false;
				this.time[i][j].image = getImage('s0.png');

				switch(j){
				case 0:		this.time[i][j].x = this.w_w1_1.x + 76;		break;
				case 1:		this.time[i][j].x = this.w_w1_1.x + 76 + 22;	break;
				case 2:		this.time[i][j].x = this.w_w1_1.x + 154;		break;
				case 3:		this.time[i][j].x = this.w_w1_1.x + 154 + 22;	break;
				}
			}
		}

		this.w_w1_2.addEventListener(Event.TOUCH_START, function(e){
			if(smithyScene.makeCompleteGroup.open(smithyScene.makeInfoDialog.kama, false) != -1){
				smithyScene.makeInfoDialog.kama.make = KAMA_MAKE_NONE;
				smithyScene.makeInfoDialog.close();
				
				tutorial_scene.tookIndex = 5;
				tutorial_scene.sprite_main.image = getImage("tutorial/start_10.png");
				tutorial_scene.sprite_yubi_3.visible = false;
				tutorial_scene.sprite_yubi_4.visible = true;
				tutorial_scene.sprite_yubi_5.visible = true;
			}
		});
		this.w_w2_2.addEventListener(Event.TOUCH_START, function(e){
			if(smithyScene.makeCompleteGroup.open(smithyScene.makeInfoDialog.kama, true) != -1){
				smithyScene.makeInfoDialog.kama.make = KAMA_MAKE_NONE;
				smithyScene.makeInfoDialog.close();
			}
		});
		this.w_w3_2.addEventListener(Event.TOUCH_START, function(e){
			if(smithyScene.makeCompleteGroup.open(smithyScene.makeInfoDialog.kama, false) != -1){
				smithyScene.makeInfoDialog.kama.make = KAMA_MAKE_NONE;
				smithyScene.makeInfoDialog.close();
			}
		});

		this.addChild(this.w_w1_1);
		this.addChild(this.w_w1_2);
		this.addChild(this.w_w2_1);
		this.addChild(this.w_w2_2);
		this.addChild(this.w_w3_1);
		this.addChild(this.w_w3_2);

		for(var i = 0; i < this.time.length; i++){
			for(var j = 0; j < this.time[i].length; j++){
				this.addChild(this.time[i][j]);
			}
		}
	},

	open : function(kama){

		this.kama = kama;
		
		// 釜にカーソルをセットする。
		smithyScene.cursor1.setPos(kama.x + kama.width / 2, kama.y);
		smithyScene.cursor1.baseY = kama.y - 6;
		smithyScene.cursor1.visible = true;

		var timeY = 34;
		var y1 = kama.y + (kama.height / 2) + smithyScene.smithyMain.y;
		var y2 = y1;

		if(kama.makeStone2 == SMITHY_STONE_NONE){
			// 短剣
			this.w_w1_1.y = y1;
			this.w_w1_2.y = y1;

			if(kama.make <= KAMA_MAKE_0){
				this.w_w1_1.visible = true;

				for(var i = 0; i < this.time[0].length; i++){
					this.time[0][i].visible = true;
					this.time[0][i].y = y1 + timeY;
				}
				
				this.noTouchArea.visible = true;
				this.closeSp.visible = true;
				
				// 秒数の座標位置を調整
				this.time[0][0].x = this.w_w1_1.x + 76;
				this.time[0][1].x = this.w_w1_1.x + 76 + 22;
				this.time[0][2].x = this.w_w1_1.x + 154;
				this.time[0][3].x = this.w_w1_1.x + 154 + 22;
			}
			else{
				// 短剣生成
				if(smithyScene.makeCompleteGroup.open(smithyScene.makeInfoDialog.kama, false) != -1){
					smithyScene.makeInfoDialog.kama.make = KAMA_MAKE_NONE;
					smithyScene.makeInfoDialog.close();
				}
				return;
			}
		}
		else{
			// 剣、斧

			this.w_w3_1.y = y1;
			this.w_w3_2.y = y1;
			this.w_w2_1.y = y2;
			this.w_w2_2.y = y2;

			if(kama.make <= KAMA_MAKE_1){
				this.w_w3_1.visible = true;
				this.w_w2_1.visible = true;

				for(var i = 0; i < this.time[0].length; i++){
					this.time[0][i].visible = true;
					this.time[0][i].y = y1 + timeY;
				}

				for(var i = 0; i < this.time[1].length; i++){
					this.time[1][i].visible = true;
					this.time[1][i].y = y2 + timeY;
				}
			}
			else if(kama.make == KAMA_MAKE_2){
				this.w_w3_1.y = y1;
				this.w_w3_1.visible = true;
				this.w_w2_2.y = y2;
				this.w_w2_2.visible = true;

				for(var i = 0; i < this.time[0].length; i++){
					this.time[0][i].visible = true;
					this.time[0][i].y = y1 + timeY;
				}
			}
			else if(kama.make == KAMA_MAKE_3){
				this.w_w3_2.y = y1;
				this.w_w3_2.visible = true;
				this.w_w2_2.y = y2;
				this.w_w2_2.visible = true;
			}

			this.noTouchArea.visible = true;
			this.closeSp.visible = true;
			
			// 秒数の座標位置を調整
			this.time[1][0].x = this.w_w2_1.x + 76;
			this.time[1][1].x = this.w_w2_1.x + 76 + 22;
			this.time[1][2].x = this.w_w2_1.x + 154;
			this.time[1][3].x = this.w_w2_1.x + 154 + 22;
			this.time[0][0].x = this.w_w3_1.x + 76;
			this.time[0][1].x = this.w_w3_1.x + 76 + 22;
			this.time[0][2].x = this.w_w3_1.x + 154;
			this.time[0][3].x = this.w_w3_1.x + 154 + 22;
		}
		
		playSe(se.window);
	},

	close : function(){
		this.w_w1_1.visible = false;
		this.w_w1_2.visible = false;
		this.w_w2_1.visible = false;
		this.w_w2_2.visible = false;
		this.w_w3_1.visible = false;
		this.w_w3_2.visible = false;
		this.noTouchArea.visible = false;
		this.closeSp.visible = false;
		smithyScene.cursor1.visible = false;

		for(var i = 0; i < this.time.length; i++){
			for(var j = 0; j < this.time[i].length; j++){
				this.time[i][j].visible = false;
			}
		}
	},
});
