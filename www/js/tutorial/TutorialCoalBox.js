﻿/**
 * 石炭の箱。
 * 石炭を釜へドラッグする処理を行う。
 * Tutorial用に
 */
TutorialCoalBox = Class.create(Sprite2,{
	initialize:function(){
		Sprite2.call(this, 64, 64);

		this.drag = false;
		this.addKamaNum = -1;
		this.touchArea = new Sprite(64*3,64*3);

		this.touchArea.addEventListener(Event.TOUCH_START, function(e){

			if(Player.coal <= 0 || smithyScene.tookIndex != 1){
				return;
			}
			smithyScene.coalBox.addKamaNum.addKamaNum = -1;
			this.startPos = e;
			this.touchStartTime = new Date().getTime();
			this.drag = true;
			smithyScene.tong.setPos(e.x - smithyScene.smithyMain.x, e.y - smithyScene.smithyMain.y);
			smithyScene.coal.setPos(e.x - smithyScene.smithyMain.x, e.y - smithyScene.smithyMain.y);
			
			//指操作
			tutorial_scene.sprite_yubi_1.x = tutorial_scene.kamaGroup.childNodes[0].x;
			tutorial_scene.sprite_yubi_1.y = tutorial_scene.kamaGroup.childNodes[0].y -tutorial_scene.sprite_yubi_1.height + 64*2;
		});

		this.touchArea.addEventListener(Event.TOUCH_MOVE, function(e){

			if(!this.drag || smithyScene.tookIndex != 1){
				return;
			}
			
			// 一定距離動いたら表示する
//			if(this.startPos.x - e.x > 50 ||
//				this.startPos.x - e.x < -50 ||
//				this.startPos.y - e.y > 50 ||
//				this.startPos.y - e.y < -50){
				
				if(!smithyScene.coal.visible){
					playSe(se.coal);
				}
				smithyScene.coal.visible = true;
				smithyScene.tong.visible = true;
//			}

			smithyScene.tong.setPos(e.x - smithyScene.smithyMain.x, e.y - smithyScene.smithyMain.y);
			smithyScene.coal.setPos(e.x - smithyScene.smithyMain.x, e.y - smithyScene.smithyMain.y);
			
			// 釜に石炭を追加
			smithyScene.coalBox.addKamaNum = smithyScene.coalBox.dropCoal(e);
			// 石炭がなくなる
			if(Player.coal <= 0){
				this.drag = false;
				smithyScene.tong.visible = false;
				smithyScene.coal.visible = false;
			}
		});

		this.touchArea.addEventListener(Event.TOUCH_END, function(e){

			if(!this.drag || smithyScene.tookIndex != 1){
				return;
			}

			this.drag = false;
			smithyScene.tong.visible = false;
			smithyScene.coal.visible = false;
			if(smithyScene.tong.visible){
			}
			else if(new Date().getTime() - this.touchStartTime < 200){
				smithyScene.coalBox.tap();
			}
			
			//指操作
			tutorial_scene.sprite_yubi_1.x = tutorial_scene.coalBox.x;
			tutorial_scene.sprite_yubi_1.y = tutorial_scene.coalBox.y+48;
		});
	},
	
	tap:function(e){
		
		var kamaNum = -1;
		var passageNum = -1;
		var passageTime = 1000 * 60 * 60 * 24 * 30; // 30日分の時間
		var kamaFire = 0;
		
		for(var i = 0; i < smithyScene.kamaGroup.childNodes.length; i++){
			
			var kama = smithyScene.kamaGroup.childNodes[i];
			var time = kama.fireTime - new Date().getTime();
			
			if(!kama.isAddCoal()){
				continue;
			}
			
			// 一番はやく火が消える時間を探す
			if(0 < time && time < passageTime){
				passageTime = time;
				passageNum = i;
			}
			
			// 火のついてない釜を探す
			if(kama.fire == KAMA_FIRE_NONE){
				kamaNum = i;
				break;
			}
		}
		
		// すべて火がついていた場合は一番早く火が消える釜に石炭を追加
		if(kamaNum == -1){
			kamaNum = passageNum;
		}
		
		// 釜に追加
		kamaNum = 0;
		if(0 <= kamaNum){
			var kama = smithyScene.kamaGroup.childNodes[kamaNum];
			if(kama.addCoal()){
				Player.coal--;
				playSe(se.coal);
				tutorial_scene.tookIndex = 2;
				tutorial_scene.sprite_main.image = getImage("tutorial/start_7.png");
				tutorial_scene.sprite_yubi_1.visible = false;
				tutorial_scene.sprite_yubi_2.visible = true;
				// 石炭の数を減らす
				if(99 < Player.coal){
					smithyScene.coalNum.number = 99;
				}
				else{
					smithyScene.coalNum.number = Player.coal;
				}
				
				// 最後に投入された釜を選択状態にする
				if(kama.state != KAMA_LEVEL_NONE 
				&& kama.fire != KAMA_FIRE_NONE
				&& kama.make == KAMA_MAKE_NONE){
					smithyScene.choiceStoneGroup.select(kama);
				}
			}
		}
	},
	
	dropCoal:function(e){
		
		// 石炭が釜にドロップされたかチェックする。
		for(var i = 0; i < smithyScene.kamaGroup.childNodes.length; i++){
			
			var kama = smithyScene.kamaGroup.childNodes[i];
			
			if(kama.isHit(
					e.x - smithyScene.smithyMain.x,
					e.y - smithyScene.smithyMain.y + smithyScene.coal.originPosY + 32)){
				
				// 同じ場所には追加しない
				if(this.addKamaNum == i){
					return i;
				}
				
				if(kama.addCoal()){
					Player.coal--;
					
					tutorial_scene.tookIndex = 2;
					tutorial_scene.sprite_main.image = getImage("tutorial/start_7.png");
					tutorial_scene.sprite_yubi_1.visible = false;
					tutorial_scene.sprite_yubi_2.visible = true;
					this.drag = false;
					smithyScene.tong.visible = false;
					smithyScene.coal.visible = false;
				}
				
				// 最後に投入された釜を選択状態にする
				if(kama.state != KAMA_LEVEL_NONE 
				&& kama.fire != KAMA_FIRE_NONE
				&& kama.make == KAMA_MAKE_NONE){
					smithyScene.choiceStoneGroup.select(kama);
				}
				
				// 石炭の数を減らす
				if(99 < Player.coal){
					smithyScene.coalNum.number = 99;
				}
				else{
					smithyScene.coalNum.number = Player.coal;
				}
				
				return i;
			}
		}
		
		return -1;
	},
});
