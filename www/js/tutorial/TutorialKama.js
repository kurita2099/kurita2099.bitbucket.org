/**
 * 釜クラス、チュートリアル仕様
 */

/*
if(MIYAGAWA_DEBUG){
	//石炭が加えられたときに消火まで延びる時間（ミリ秒）
	const KAMA_FIRE_ADD_COAL_TIME = 1000 * 15;
	
	// 消火までの時間によって火の強さの絵を変える（ミリ秒）
	const KAMA_FIRE_TIME_LOW = 1000 * 5;
	const KAMA_FIRE_TIME_MIDDLE = 1000 * 10;
	const KAMA_FIRE_TIME_HIGH = 1000 * 15;
	const KAMA_FIRE_TIME_HIGH_2 = 1000 * 30;
	const KAMA_FIRE_TIME_HIGH_3 = 1000 * 45;
}
else{
	// 石炭が加えられたときに消火まで延びる時間（ミリ秒）
	const KAMA_FIRE_ADD_COAL_TIME = 1000 * 60 * 10;
	
	// 消火までの時間によって火の強さの絵を変える（ミリ秒）
	const KAMA_FIRE_TIME_LOW = 1000 * 60 * 2;
	const KAMA_FIRE_TIME_MIDDLE = 1000 * 60 * 5;
	const KAMA_FIRE_TIME_HIGH = 1000 * 60 * 10;
	const KAMA_FIRE_TIME_HIGH_2 = 1000 * 60 * 20;
	const KAMA_FIRE_TIME_HIGH_3 = 1000 * 60 * 30;
}
*/

/*
// 溶解の時間で製作される武器が変わる（ミリ秒）
if(MIYAGAWA_DEBUG || DEBUG){
	const KAMA_MAKE_TIME_KNIFE = 1000 * 5;
	const KAMA_MAKE_TIME_AXE = 1000 * 10;
	const KAMA_MAKE_TIME_SWORD = 1000 * 15;
}
else{
	const KAMA_MAKE_TIME_KNIFE = 1000 * 60 * 1;
	const KAMA_MAKE_TIME_AXE = 1000 * 60 * 2;
	const KAMA_MAKE_TIME_SWORD = 1000 * 60 * 6;
}
*/

TutorialKama = Class.create(Group2,{
	initialize:function(){
		Group2.call(this);
		
		this.KAMA_MAKE_TIME_KNIFE = 1000 * 5;	//チュートリアル用に、短剣の生成時間を短くする変数を定義
		
		this.width = 64;
		this.height = 64;

		// スプライト生成
		this.touchArea = new Sprite2(64, 64);
		this.kamaSprite = new Sprite2(64, 64);		// 釜
		this.fireSprite = new Sprite2(64, 64);		// 釜の火力
		this.makeSprite = new Sprite2(64, 64);		// 釜の中身
		this.smokeSprite = new Sprite2(64, 64);		// 煙
		this.smokeSprite.baseY = -32;
		this.smokeSprite.moveY = 0;
		this.smokeSprite.vecY = SMOKE_VEC_Y;
		this.smokeSprite.moveWait = 0;
		this.smokeSprite.alpha = 0;
		this.smokeSprite.opacity = 1;
		this.smokeSprite.opacity2 = 1;

		// グループに追加
		this.addChild(this.kamaSprite);
		this.addChild(this.fireSprite);
		this.addChild(this.makeSprite);
		this.addChild(this.smokeSprite);

		// 煙が動くアニメ
		this.smokeSprite.addEventListener(Event.ENTER_FRAME, function(){

			if(this.image == null){
				return;
			}

			this.y = this.baseY + this.moveY;
			this.moveY += this.vecY;
			this.vecY += SMOKE_SPEED;
			this.opacity2 -= 0.1;
			if(this.opacity2 < 0){
				this.opacity2 = 0;
			}
			this.opacity = this.opacity2;
			if(0 < this.vecY){

				if(15 <= this.moveWait){
					this.vecY = SMOKE_VEC_Y;
					this.opacity2 = 1;
					this.moveY = 0;
					this.moveWait = 0;
				}
				else{
					this.vecY = 0;
					this.moveWait++;
				}
			}
		});

		// 釜の状態を初期化
		this.state = KAMA_LEVEL_NONE;

		// 釜に入れる鉱石
		this.makeStone1 = SMITHY_STONE_NONE;
		this.makeStone2 = SMITHY_STONE_NONE;

		// 武器生成開始
		this.makeStartTime = 0;
		this.make = KAMA_MAKE_NONE;

		// 火が消えた時間
		this.fireOffTime = 0;

		// 火が消えるまでの時間
		this.fireTime = 0;
		this.fire = KAMA_FIRE_NONE;

		this.touchArea.kama = this;
		this.touchArea.addEventListener(Event.TOUCH_START, function(){
			if(tutorial_scene.tookIndex != 2)return;
			var kama = this.kama;
			
			// 火が消えている。
			if(kama.fire == KAMA_FIRE_NONE){

				// 製作前。
				if(kama.make == KAMA_MAKE_NONE){
					// 釜の購入。
					if(kama.state == KAMA_LEVEL_NONE){
						smithyScene.kamaDialog.open(kama, KAMA_DIALOG_BUY);
					}
					// 釜の購入。
					else if(kama.state == KAMA_LEVEL_1){
						smithyScene.kamaDialog.open(kama, KAMA_DIALOG_GRADE_UP);
					}
				}
				// 金の釜の場合は火が消えていても製作したものを取り出せる
				else if(kama.state == KAMA_LEVEL_2){
					if(kama.make == KAMA_MAKE_0 ||
						kama.make == KAMA_MAKE_1 ||
						kama.make == KAMA_MAKE_2 ||
						kama.make == KAMA_MAKE_3){

						smithyScene.makeInfoDialog.open(kama);
					}
				}
				// 鉄の釜の場合はウィンドウ表示
				else if(kama.state == KAMA_LEVEL_1){
					smithyScene.addCoalDialog.open(kama);
				}
			}
			// 火がついてる。
			else{

				// 製作前。
				if(kama.make == KAMA_MAKE_NONE){
					// 鉱石の投入。
					smithyScene.choiceStoneGroup.open(kama);
				}
				// 武器の完成。
				else if(kama.make == KAMA_MAKE_0 ||
						kama.make == KAMA_MAKE_1 ||
						kama.make == KAMA_MAKE_2 ||
						kama.make == KAMA_MAKE_3){

					smithyScene.makeInfoDialog.open(kama);
//					if(smithyScene.makeCompleteGroup.open(this) != -1){
//						this.make = KAMA_MAKE_NONE;
//					}
				}
			}
		});

		this.addEventListener(Event.ENTER_FRAME, function(){

			// 火がついている
			if(this.fire != KAMA_FIRE_NONE){

				var passageTime = this.fireTime - new Date().getTime();
				
				/*
				// 経過時間によって火の強さを変える。チュートリアル中に消えると困るのでカット
				if(passageTime < 0){
					this.fire = KAMA_FIRE_NONE;
					this.fireOffTime = this.fireTime;
				}
				else if(passageTime < KAMA_FIRE_TIME_LOW){
					this.fire = KAMA_FIRE_1;
				}
				else if(passageTime < KAMA_FIRE_TIME_MIDDLE){
					this.fire = KAMA_FIRE_2;
				}
				else if(passageTime < KAMA_FIRE_TIME_HIGH){
					this.fire = KAMA_FIRE_3;
				}
				else if(passageTime < KAMA_FIRE_TIME_HIGH_2){
					this.fire = KAMA_FIRE_4;
				}
				else{
					this.fire = KAMA_FIRE_5;
				}
				*/

				// 経過時間によって釜の中を変える
				var makeTime = new Date().getTime() - this.makeStartTime;
				if(this.make == KAMA_MAKE_NONE){
					// 何もしない。
				}
				else if(this.getSwordTime() < makeTime){
					this.make = KAMA_MAKE_3;
				}
				else if(this.getAxeTime() < makeTime){
					this.make = KAMA_MAKE_2;
				}
				else if(this.getKnifeTime() < makeTime){
					this.make = KAMA_MAKE_1;
				}
				else if(0 < makeTime){
					this.make = KAMA_MAKE_0;
				}
				else{
					this.make = KAMA_MAKE_NONE;
				}
			}
			// 火が消えている
			else{
				// 製作中
				if(this.make != KAMA_MAKE_NONE){
					// 普通の釜
					if(this.state == KAMA_LEVEL_1){
						// 製作の状態を最初にもどす。
						this.make = KAMA_MAKE_0;
					}
				}
				
				if(smithyScene.choiceStoneGroup.selectKama == this){
					if(!smithyScene.choiceStoneGroup.cancel.visible){
						smithyScene.choiceStoneGroup.close();
					}
				}
			}
		});
	},

	state : {
		get : function(){
			return this._state;
		},
		set : function(state){
			this._state = state;
			if(state == KAMA_LEVEL_1){
				this.kamaSprite.image = getImage("kazi/kama_1.png");
			}
			else if(state == KAMA_LEVEL_2){
				this.kamaSprite.image = getImage("kazi/kama_2.png");
			}
			else{
				this.kamaSprite.image = getImage("kazi/kama_0.png");
			}
		}
	},
	
	getSwordTime : function(){
		var time = KAMA_MAKE_TIME_SWORD;
		
		if(this.state == KAMA_LEVEL_2){
			time /= 2;
		}
		
		if(this.makeStone1 == SMITHY_STONE_METEORON){
			time *= 10;
		}
		
		return time;
	},
	getAxeTime : function(){
		var time = KAMA_MAKE_TIME_AXE;
		
		if(this.state == KAMA_LEVEL_2){
			time /= 2;
		}
		
		if(this.makeStone1 == SMITHY_STONE_METEORON){
			time *= 10;
		}
		
		return time;
	},
	getKnifeTime : function(){
		var time = this.KAMA_MAKE_TIME_KNIFE;
		
		if(this.state == KAMA_LEVEL_2){
			time /= 2;
		}
		
		if(this.makeStone1 == SMITHY_STONE_METEORON){
			time *= 10;
		}
		
		return time;
	},
	
	isAddCoal : function(){
		// まだ釜を購入していない。
		if(this.state == KAMA_LEVEL_NONE){
			return false;
		}

		// 石炭を追加できるか
		if(this.fire == KAMA_FIRE_5){
			return false;
		}
		
		return true;
	},

	addCoal : function(){

		if(!this.isAddCoal()){
			return false;
		}
		
		playSe(se.fire);

		// 火が消える時間を計算
		if(this.fire != KAMA_FIRE_NONE){
			this.fireTime += KAMA_FIRE_ADD_COAL_TIME;
		}
		else{
			this.fireTime = new Date().getTime() + KAMA_FIRE_ADD_COAL_TIME;
		}

		// 製作途中で火が消えてしまった。
		if(this.fire == KAMA_FIRE_NONE &&
			this.make != KAMA_MAKE_NONE){
			// 普通の釜は最初からスタート。
			if(this.state == KAMA_LEVEL_1){
				this.makeStartTime = new Date().getTime();
			}
			// 金の釜は消えた時点から再スタート
			else if(this.state == KAMA_LEVEL_2){
				// 火が消えた時間から再点火までの時間を製作開始時間に足すことで再スタートの時間が計算できる
				this.makeStartTime += new Date().getTime() - this.fireOffTime;
			}
		}

		this.fire = KAMA_FIRE_3;
		
		return true;
	},

	/**
	 * 製作時間の取得
	 */
	makeTime : function(){

		if(this.fire == KAMA_FIRE_NONE &&
			this.make != KAMA_MAKE_NONE){
			// 普通の釜は最初からスタート。
			if(this.state == KAMA_LEVEL_1){
				return  0;
			}
			// 金の釜は消えた時点から再スタート
			else if(this.state == KAMA_LEVEL_2){
				// 火が消えた時間から再点火までの時間を製作開始時間に足すことで再スタートの時間が計算できる
				return this.fireOffTime - this.makeStartTime;
			}
		}
		else{
			return new Date().getTime() - this.makeStartTime;
		}
	},

	makeStart : function(){

		playSe(se.tap_1);

		// 生成開始の状態にする
		this.make = KAMA_MAKE_0;
		// 生成に使う鉱石を保存
		this.makeStone1 = smithyScene.dropStoneGroup.stones[0].type;
		this.makeStone2 = smithyScene.dropStoneGroup.stones[1].type;
		
		if(this.makeStone1 != SMITHY_STONE_NONE) Player.stone[this.makeStone1]--;
		if(this.makeStone2 != SMITHY_STONE_NONE) Player.stone[this.makeStone2]--;
		
		// 生成開始時間を保存
		this.makeStartTime = new Date().getTime();
	},

	make : {
		get : function(){
			return this._make;
		},
		set : function(make){

			this._make = make;
			
			if(make == KAMA_MAKE_NONE){
				this.makeSprite.image = null;
				this.smokeSprite.image = null;
			}
			else if(make == KAMA_MAKE_0){
				this.makeSprite.image = getImage('kazi/kama_a.png');
				this.smokeSprite.image = null;
			}
			else if(make == KAMA_MAKE_3){
				
				if(this.makeStone2 == SMITHY_STONE_NONE){
					this.makeSprite.image = getImage('kazi/kama_b.png');
				}
				else{
					this.makeSprite.image = getImage('kazi/kama_c.png');
				}
				this.smokeSprite.image = getImage('kazi/smoke.png');
			}
			else if(make == KAMA_MAKE_1 ||
					make == KAMA_MAKE_2){
				
				if((make == KAMA_MAKE_1 && this.makeStone2 == SMITHY_STONE_NONE) ||
					(make == KAMA_MAKE_2)){
					this.makeSprite.image = getImage('kazi/kama_b.png');
					this.smokeSprite.image = getImage('kazi/smoke.png');
				}
				else{
					this.makeSprite.image = getImage('kazi/kama_a.png');
					this.smokeSprite.image = null;
				}
			}
		}
	},

	fire : {
		get : function(){
			return this._fire;
		},
		set : function(fire){
			this._fire = fire;

			if(this.state == KAMA_LEVEL_1){
				if(fire == KAMA_FIRE_NONE){
					this.fireSprite.image = null;
				}
				else if(fire == KAMA_FIRE_1){
					this.fireSprite.image = getImage('kazi/kama_1_1.png');
				}
				else if(fire == KAMA_FIRE_2){
					this.fireSprite.image = getImage('kazi/kama_1_2.png');
				}
				else if(fire == KAMA_FIRE_3){
					this.fireSprite.image = getImage('kazi/kama_1_3.png');
				}
				else if(fire == KAMA_FIRE_4){
					this.fireSprite.image = getImage('kazi/kama_1_4.png');
				}
				else if(fire == KAMA_FIRE_5){
					this.fireSprite.image = getImage('kazi/kama_1_5.png');
				}
			}
			else if(this.state == KAMA_LEVEL_2){
				if(fire == KAMA_FIRE_NONE){
					this.fireSprite.image = null;
				}
				else if(fire == KAMA_FIRE_1){
					this.fireSprite.image = getImage('kazi/kama_2_1.png');
				}
				else if(fire == KAMA_FIRE_2){
					this.fireSprite.image = getImage('kazi/kama_2_2.png');
				}
				else if(fire == KAMA_FIRE_3){
					this.fireSprite.image = getImage('kazi/kama_2_3.png');
				}
				else if(fire == KAMA_FIRE_4){
					this.fireSprite.image = getImage('kazi/kama_2_4.png');
				}
				else if(fire == KAMA_FIRE_5){
					this.fireSprite.image = getImage('kazi/kama_2_5.png');
				}
			}
		},
	},

	/**
	 * レジューム処理。
	 * @param time レジュームが発生した時間
	 */
	resume : function(time){

		// 釜の火の残り時間よりも長く休止していた場合。
		if(this.fire != KAMA_FIRE_NONE && this.fireTime <= time){

			// 火を消す
			this.fire = KAMA_FIRE_NONE;
			this.fireOffTime = this.fireTime;

			// 製作中
			if(this.make != KAMA_MAKE_NONE){
				// 普通の釜
				if(this.state == KAMA_LEVEL_1){
					// 製作の状態を最初にもどす。
					this.make = KAMA_MAKE_0;
				}
				// 金の釜
				else if(this.state == KAMA_LEVEL_2){

					// 経過時間によって釜の中を変える
					// 火が消えていた時間は製作時間にカウントしない
					var makeTime = this.fireTime - this.makeStartTime;
					if(this.getSwordTime() < makeTime){
						this.make = KAMA_MAKE_3;
					}
					else if(this.getAxeTime() < makeTime){
						this.make = KAMA_MAKE_2;
					}
					else if(this.getKnifeTime() < makeTime){
						this.make = KAMA_MAKE_1;
					}
					else if(0 < makeTime){
						this.make = KAMA_MAKE_0;
					}
				}
			}
		}
	}
});
