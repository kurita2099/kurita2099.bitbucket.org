﻿//店舗シーンのチュートリアル1、余計な機能を削除したタイプ
//基本的にtutorial_scene.tookIndexを参照して制御する

Tutorial_1 = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		tutorial_scene = this;
		playBgm(se.bgm_main);	//bgm
		
		//ずらし
		this.OFFSET_X = 0;
		this.OFFSET_Y = INTERFACE_OFFSET_HEIGHT;
		
		//扉
		this.tobira = [0,0,0,0];
		
		
		//レベルに応じて階段座標を変更
		
		var pos;//計算した座標受け皿
		pos = ChipToPosition(this,2 + (Player.shopLevel*1),0)
		this.tobira[0] = pos[0];
		this.tobira[1] = pos[1];
		this.tobira[2] = pos[0]+CHIPSIZE;
		this.tobira[3] = pos[1]+CHIPSIZE;
		
		//ショップの画像
		var shop = createMap(mapdata[SHOP_OFFSET + Player.shopLevel][0]);
		shop.visible = true;
		shop.x += this.OFFSET_X;
		shop.y += this.OFFSET_Y;
		this.addChild(shop);
		
		pos = ChipToPosition(this,2+Player.shopLevel,1);
		//店主を作成
		var str = Player.round == 0?"店主1":"店主2";
		this.tensyu = game.NpcManager.CreateNpc(str,pos[0],pos[1]);
		this.tensyu.onScene(this);
		this.counterWeapon = new Sprite(64,64);	//カウンター前の武器
		this.counterWeapon.x = pos[0];
		this.counterWeapon.y = pos[1]+64;
		this.counterWeapon.visible = false;
		this.addChild(this.counterWeapon);
		
		//お店の扉
		this.door;
		//ショップレベルによって、扉の大きさが変化
		if(Player.shopLevel < 2)
		{
			var doorPos = ChipToPosition(this, 2 + (Player.shopLevel*1), 4 + (Player.shopLevel*2) );
			this.door = new Sprite(64,64);
			this.door.frame = 8;
			this.door.x = doorPos[0];
			this.door.y = doorPos[1];
		}else
		{
			this.door = new Sprite(64*3,64);
			this.door.frame = 5;
			var doorPos = ChipToPosition(this, 3 + (( Player.shopLevel-2)*1), 4 + (Player.shopLevel*2) );
			this.door.x = doorPos[0];
			this.door.y = doorPos[1];
		}
		this.door.image = getImage("silk_icon_merged.png");
		this.door.visible = true;
		this.addChild(this.door);
		
		//インターフェイス,チュートリアル仕様のため、中身をもってきて余計な処理を省いた
		var wx = pos[0]+CHIPSIZE/2;
		var wy = pos[1]+64*3;
		//ViewStateの余分な物を削除した処理↓
		game.UI.scene = this;
		game.UI.sceneIndex = SCENE_STATE_SHOP;	//現在のアイテムの描画種別を記憶
		game.UI.stateSprite.visible = true;
		game.UI.menuSprite.visible = true;
		this.addChild(game.UI.stateSprite);	//描画用のSpriteをシーンに乗せる
		this.addChild(game.UI.menuSprite);
		game.InventoryManager.Atach(this);
		game.InventoryManager.Draw();
		
		//下メニューの位置と画像の設定
		for(var i = 0; i < game.UI.buttonData.length; i++)
		{
			var s = game.UI.buttonData[i];
			//位置調整、中央寄せ、YはmenuSpriteに乗っているため描画では減算
			s.x = WIDTH/2;
			var wid = s.width-4;
			s.x += -(wid*game.UI.buttonData.length)/2;
			s.x += i*wid;
			s.y = HEIGHT;
			s.y += -100;
			s.y += -s.height;
			s.visible = true;
			
			//各シーン判定
			s.image = game.UI.sceneIndex == game.UI.buttonSceneID[i]?getImage('InterFace/main_tab_'+(i+1)+'b.png'):getImage('InterFace/main_tab_'+(i+1)+'.png');
		}
		game.UI.MM.Atach();	//鉱山
		
		//Surfaceテスト、ピクセルクリア
		game.UI.surface.clear();
		game.UI.surfaceMenu.clear();
	    //一旦全ての描画フラグを初期化
	    game.InventoryManager.clearSlot();
		//ステータスを更新する
		for(i = 1; i < game.UI.coin.length; i++)game.UI.coin[i].image = getImage(game.UI.getPlace(Player.coins)[i-1]+'.png');
		for(i = 1; i< 3; i++)game.UI.hart[i].image = getImage(game.UI.getPlace(Player.lifemax)[i-1]+'.png');
		for(i = 4; i< 6; i++)game.UI.hart[i].image = getImage(game.UI.getPlace(Player.life)[i-4]+'.png');
		for(i = 1; i < game.UI.diamond.length; i++)game.UI.diamond[i].image = getImage(game.UI.getPlace(Player.diamond)[i-1]+'.png');
		game.UI.DrawSurFaceState();	//描画
		
		//チュートリアル処理
		this.tookIndex = 0;	//チュートリアル進行度、最初のメッセージ出現で1へ移行
		
		//チュートリアルの各スプライト
		var s;
		//指スプライト-鉱山
		s = this.sprite_yubi_1 = new Sprite(52,84);
		s.image = getImage('tutorial/hand_info.png');
		s.x = game.UI.buttonData[0].x+game.UI.buttonData[0].width/2;
		s.x += -(s.width/2);
		s.y = game.UI.buttonData[0].y-s.height;
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		s.visible = false;
		this.addChild(s);
		//指スプライト-鉱山への扉
		s = this.sprite_yubi_2 = new Sprite(52,84);
		s.image = getImage('tutorial/hand_info.png');
		s.x = pos[0] + CHIPSIZE/2;
		s.x += -(s.width/2);
		s.y = pos[1] -CHIPSIZE -s.height;
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		s.visible = false;
		//this.addChild(s);
		//指スプライト-出発ボタン
		s = this.sprite_yubi_3 = new Sprite(52,84);
		s.image = getImage('tutorial/hand_info.png');
		s.x = game.UI.MM.sprite_go.x;
		s.x += -s.width-32;
		s.y = game.UI.MM.sprite_go.y;
		s.tl.moveBy(-YUBI_MOVE,0,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(YUBI_MOVE,0,YUBI_MOVE_PUSHFRAME).loop();
		s.visible = false;
		s.rotate(-90);
		this.addChild(s);
		//メッセージ
		s = this.sprite_main = new Sprite(512,200);
		s.image = getImage('tutorial/start_1.png');
		s.x = wx-s.width/2;	//PositionはInterfaceのメッセージ位置と同じ
		s.y = wy;
		s.opacity = 0;
		s.visible = true;
		this.addChild(s);
		s.tl.delay(TUTORIAL_STARTDELAY).fadeIn(0).then(function(){ playSe(se.window); tutorial_scene.tookIndex++; });
		s.addEventListener(Event.TOUCH_START, function(e){
			//メッセージがタッチされたら次の指示メッセージを流す
			if(tutorial_scene.tookIndex == 1)
			{
				this.image = getImage('tutorial/start_2.png');
				tutorial_scene.tookIndex++;
				playSe(se.tap_1);
				tutorial_scene.sprite_yubi_1.visible = true;
				tutorial_scene.sprite_yubi_2.visible = true;
			}else if(tutorial_scene.tookIndex == 2)
			{
			
			}
		});
		
		this.addEventListener(Event.TOUCH_START, function(e){
			var clickPos = [e.localX,e.localY];
			
			//InterFace各種ボタンがクリックされた場合の処理、チュートリアル仕様で直にもってきた
			if(tutorial_scene.tookIndex == 2)
			{
				//Menuボタン判定
				for(var i = 0; i < game.UI.buttonData.length; i++)
				{
					var s = game.UI.buttonData[i];
					if(s.visible)
					{
						if(game.UI.HitPos(s,clickPos))
						{
							//自身がクリックされて尚且つwindowがクリック可能シーケンスなら
							switch(i)
							{
								case 0:	//鉱山
								{
									if(!game.UI.MM.ivent)
									{
										game.UI.MM.IventOn();
										playSe(se.window);
										tutorial_scene.tookIndex = 3;
										tutorial_scene.sprite_yubi_1.visible = false;
										tutorial_scene.sprite_yubi_2.visible = false;
										tutorial_scene.sprite_yubi_3.visible = true;
										tutorial_scene.sprite_main.visible = false;
									}
									break;
								}
							}
							
						}
					}
				}//各メニューチェック終了
			}else if(tutorial_scene.tookIndex == 3)
			{
				if(game.UI.HitPos(game.UI.MM.sprite_go,clickPos))
				{
					game.pushScene(new FadeScene(tutorial_2_Assets, makeNewScene(Tutorial_2)));
					playSe(se.move);
					game.UI.MM.IventOff();
				}
			}
			
			//ステータス更新
			game.UI.ViewStateUpdate();
			
			//倉庫を開いている場合は処理しない
			if((e.localX>this.tobira[0]  && e.localX<this.tobira[2] )&&(e.localY>this.tobira[1]  && e.localY< this.tobira[3])){
			  	//UIの鉱山メニューを呼び出す
				if(tutorial_scene.tookIndex == 2)
				{
					game.UI.MM.IventOn();
					playSe(se.window);
					tutorial_scene.tookIndex = 3;
					tutorial_scene.sprite_yubi_1.visible = false;
					tutorial_scene.sprite_yubi_2.visible = false;
					tutorial_scene.sprite_yubi_3.visible = true;
					tutorial_scene.sprite_main.visible = false;
				}
				
			}
			
		});
		
		
		//入り口の座標を取得
		this.oSetY = 4+Player.shopLevel*2;//入り口の座標を取得
		this.oSetY *= 64;
		
		this.addEventListener(Event.ENTER_FRAME, function(e){
			
			this.tensyu.ChangeImage();	//店主アニメ
		});
		
	},
});
