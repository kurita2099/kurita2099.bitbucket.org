/**
 * 鉱石を選択してくださいメッセージとタッチエリア。
 */
TutorialChoiceStoneGroup = Class.create(Object,{
	initialize:function(){
		Object.call(this);

		this.selectKama = null;
		
		this.message = new Sprite2(512, 90);

		this.message.image = getImage('kazi/w_select.png');
		this.message.setPos(SMITHY_WIDTH / 2, 0);
		this.message.wait = 0;
		this.message.addEventListener(Event.ENTER_FRAME, function(){
			
			var instance = smithyScene.choiceStoneGroup;
			
			if(!instance.message.visible){
				return;
			}
			
			if(FPS * 2 <= instance.message.wait){
				instance.visibleNofusion(false);
			}
			instance.message.wait++;
		});
		
		this.make = new Sprite2(192, 90);
		this.make.image = getImage('kazi/w_make.png');
		this.make.setPos(WIDTH / 2, 0);
		this.cancel = new Sprite2(90, 90);
		this.cancel.image = getImage('close.png');
		this.cancel.x = 540;
		this.cancel.y = 134;
		
		this.slotMaterial = new Array(2);
		for(var i = 0; i < this.slotMaterial.length; i++){
			this.slotMaterial[i] = new Sprite2(64,64);
			this.slotMaterial[i].image = getImage('kazi/slot_material.png');
			this.slotMaterial[i].visible = false;
		}

		// 表示中は下のレイヤーをタッチ無効エリアにする。
		this.noTouchArea = new Sprite(WIDTH, HEIGHT);
		this.noTouchArea.y = 0;

		// 決定。
		this.make.addEventListener(Event.TOUCH_START, function(e){

			// 投入する鉱石があるなら生成開始。
			if(tutorial_scene.tookIndex == 3)
			{
				if(smithyScene.dropStoneGroup.isDrop()){
					tutorial_scene.tookIndex = 4;
					tutorial_scene.sprite_main.image = getImage("tutorial/start_9.png");
					
					playSe(se.tap_1);
					smithyScene.choiceStoneGroup.selectKama.makeStart();
					smithyScene.choiceStoneGroup.close(true);
					smithyScene.makeInfoDialog.open(smithyScene.kamaGroup.childNodes[0]);
				}
				else{
					playSe(se.tap_2);
				}
			}
		});

		// キャンセル。
		this.cancel.addEventListener(Event.TOUCH_START, function(e){
		});
	},
	
	select : function(kama){
		
		this.selectKama = kama;
		
		//投入する鉱石を表示するマスを表示
		for(var i = 0; i < this.slotMaterial.length; i++){
			this.slotMaterial[i].setPos(kama.x + kama.width / 2, kama.y);
			this.slotMaterial[i].y = kama.y - 64 * (i + 1);
			this.slotMaterial[i].visible = true;
		}
	},

	open : function(kama){

		playSe(se.window);

		this.selectKama = kama;

		//投入する鉱石を表示するマスを表示
		for(var i = 0; i < this.slotMaterial.length; i++){
			this.slotMaterial[i].setPos(kama.x + kama.width / 2, kama.y);
			this.slotMaterial[i].y = kama.y - 64 * (i + 1);
			this.slotMaterial[i].visible = true;
		}

		// 釜の下にメッセージを表示。
		this.message.image = getImage('kazi/w_select.png');
		this.make.setPos(smithyScene.smithyMain.x + kama.x + kama.width / 2, 0);
		this.make.y = smithyScene.smithyMain.y + kama.y + (kama.height / 2);
		this.message.y = smithyScene.smithyMain.y + kama.y + (kama.height / 2);
		
		smithyScene.dropStoneGroup.select(kama);

		this.visible = true;
		smithyScene.dropStoneGroup.visible = true;
		this.make.visible = true;
		this.cancel.visible = true;
		this.noTouchArea.visible = true;
	},

	/**
	 * ウィンドウを閉じる。
	 * @param	make	true：武器生成開始
	 * 					false：キャンセル
	 */
	close : function(make){

		this.visible = false;
		this.selectKama = null;

		if(make){
			smithyScene.dropStoneGroup.drop();
		}
		else{
			
			// キャンセルした場合は個数をもとに戻す。
			for(var i = 0; i < smithyScene.dropStoneGroup.stones.length; i++){
				var stone = smithyScene.dropStoneGroup.stones[i].type;
				if(stone != SMITHY_STONE_NONE){
					smithyScene.slotStones[stone].count++;
				}
			}
			
			smithyScene.dropStoneGroup.visible = false;
		}
		
		for(var i = 0; i < this.slotMaterial.length; i++){
			this.slotMaterial[i].visible = false;
		}
		this.message.visible = false;
		this.make.visible = false;
		this.cancel.visible = false;
		this.noTouchArea.visible = false;
	},
	
	visibleNofusion : function(flag){
		
		if(flag){
			this.message.image = getImage('kazi/w_nofusion.png');
			this.message.visible = true;
			this.make.visible = false;
			this.message.wait = 0;
			playSe(se.tap_2);
		}
		else{
			this.message.visible = false;
			this.make.visible = true;
		}
	}
});
