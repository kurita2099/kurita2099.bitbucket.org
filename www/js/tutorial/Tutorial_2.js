﻿//店舗シーンのチュートリアル1、余計な機能を削除したタイプ
//基本的にtutorial_scene.tookIndexを参照して制御する

Tutorial_2 = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		tutorial_scene = this;
		playBgm(se.bgm_01);	//bgm
		
		//ずらし
		this.OFFSET_X = 0;
		this.OFFSET_Y = INTERFACE_OFFSET_HEIGHT;

		//レベルに応じて階段座標を変更
		var pos;//計算した座標受け皿
		
		//鉱山フィールドの画像
		var mine = new Sprite(640,704);
		mine.image = getImage("mine.png");
		mine.visible = true;
		mine.x += this.OFFSET_X;
		mine.y += this.OFFSET_Y;
		this.addChild(mine);
		
		//インターフェイス,チュートリアル仕様のため、中身をもってきて余計な処理を省いた
		//ViewStateの余分な物を削除した処理↓
		game.UI.scene = this;
		game.UI.sceneIndex = SCENE_STATE_MINE;	//現在のアイテムの描画種別を記憶
		game.UI.stateSprite.visible = true;
		this.addChild(game.UI.stateSprite);	//描画用のSpriteをシーンに乗せる
		//Surfaceテスト、ピクセルクリア
		game.UI.surface.clear();
	    //一旦全ての描画フラグを初期化
	    game.InventoryManager.clearSlot();
		//ステータスを更新する
		for(i = 1; i < game.UI.coin.length; i++)game.UI.coin[i].image = getImage(game.UI.getPlace(Player.coins)[i-1]+'.png');
		for(i = 1; i< 3; i++)game.UI.hart[i].image = getImage(game.UI.getPlace(Player.lifemax)[i-1]+'.png');
		for(i = 4; i< 6; i++)game.UI.hart[i].image = getImage(game.UI.getPlace(Player.life)[i-4]+'.png');
		for(i = 1; i < game.UI.diamond.length; i++)game.UI.diamond[i].image = getImage(game.UI.getPlace(Player.diamond)[i-1]+'.png');
		//設定したspirteをSurFaceに載せる
		var s;
		for(i = 0; i < game.UI.coin.length; i++){s = game.UI.coin[i];game.UI.surface.draw(s.image,s.x,s.y);}
		for(i = 0; i< game.UI.hart.length; i++){s = game.UI.hart[i];game.UI.surface.draw(s.image,s.x,s.y);}
		for(i = 0; i < game.UI.diamond.length; i++){s = game.UI.diamond[i];game.UI.surface.draw(s.image,s.x,s.y);}
		game.InventoryManager.Draw();
		game.InventoryManager.Atach(this);
		
		//チュートリアル処理
		this.tookIndex = 0;	//チュートリアル進行度、最初のメッセージ出現で1へ移行
		
		//チュートリアルの各スプライト
		
		//ピッケルライフ
		this.p_life = 24;
		s = this.sprite_lifes = [];
		var group = new Group();
		for(var i = 0; i < 24; i++)
		{
			s.push(new Sprite(16,40) );
			s[i].image = getImage("pickel_g.png");
			s[i].visible = true;
			s[i].x = 80 + 20 * i;
			s[i].y = 12;
			s[i].y += this.OFFSET_Y;
			group.addChild(s[i]);
		}
		this.addChild(group);
		
		//ピッケル
		s = this.sprite_pickel = new Sprite(64,64);
		s.image = getImage("pickel_1.png");
		s.x = 6;
		s.y = this.OFFSET_Y;
		s.visible = true;
		this.addChild(s);
		
		//ブロック
		s = this.sprite_blocks = new Array(8);
		group = new Group();
		for(var i = 0; i < 8; i++)
		{
			s[i] = new Array(8);
			for(var j = 0; j < 8; j++)
			{
				var ob = s[i][j] = new Sprite(80,80);
				ob.image = getImage("block.png");
				ob.visible = true;
				ob.x = j*ob.width;
				ob.y = i*ob.height + this.OFFSET_Y + 64;
				ob.frame = 0;
				group.addChild(ob);
			}
		}
		//鉱石
		s = this.sprite_m1 = new Sprite(64,64);
		s.image = getImage("stone_0.png");
		s.x = this.sprite_blocks[2][3].x + (80-64)/2;
		s.y = this.sprite_blocks[2][3].y + (80-64)/2;
		s.visible = true;
		s.addEventListener(Event.TOUCH_START, function(e){
			if(tutorial_scene.tookIndex == 4)
			{
				this.x = game.InventoryManager.s_weaponImage[0].x;
				this.y = game.InventoryManager.s_weaponImage[0].y;//48=初期座標
				tutorial_scene.sprite_yubi1.visible = false;
				playSe(se.get);
			}
		});
		s = this.sprite_m2 = new Sprite(64,64);
		s.image = getImage("stone_1.png");
		s.x = 0
		s.y = 0;
		s.visible = false;
		s.addEventListener(Event.TOUCH_START, function(e){
			if(tutorial_scene.tookIndex == 4)
			{
				this.x = game.InventoryManager.s_weaponImage[1].x;
				this.y = game.InventoryManager.s_weaponImage[1].y;
				tutorial_scene.sprite_yubi2.visible = false;
				playSe(se.get);
			}
		});
		
		this.addChild(this.sprite_m1);
		this.addChild(this.sprite_m2);
		this.addChild(group);//ブロックアタッチ
		
		//チュートリアル用の上に被せるブロック
		s = this.sprite_lblocks = new Array(5);
		group = new Group();
		for(var i = 0; i < this.sprite_lblocks.length; i++)
		{
			s[i] = new Sprite(80,80);
			s[i].image = getImage("tutorial/block_1b.png");
			s[i].visible = false;
			s[i].tl.fadeOut(6).fadeIn(6).loop();
			group.addChild(s[i]);
		}
		this.addChild(group);
		//各ブロックを規定位置へ
		s[0].x = this.sprite_blocks[2][3].x;
		s[0].y = this.sprite_blocks[2][3].y;
		s[0].box = this.sprite_blocks[2][3];
		s[1].x = this.sprite_blocks[3][3].x;
		s[1].y = this.sprite_blocks[3][3].y;
		s[1].box = this.sprite_blocks[3][3];
		s[2].x = this.sprite_blocks[1][3].x;
		s[2].y = this.sprite_blocks[1][3].y;
		s[2].box = this.sprite_blocks[1][3];
		s[3].x = this.sprite_blocks[2][2].x;
		s[3].y = this.sprite_blocks[2][2].y;
		s[3].box = this.sprite_blocks[2][2];
		s[4].x = this.sprite_blocks[2][4].x;
		s[4].y = this.sprite_blocks[2][4].y;
		s[4].box = this.sprite_blocks[2][4];
		//破片ブロック
		s = this.sprite_bblocks = new Array(4);
		group = new Group();
		for(var i = 0; i < s.length; i++)
		{
			s[i] = new Sprite(24,24);
			s[i].image = getImage("mine/block_piece.png");
			s[i].visible = true;
			s[i].opacity = 0;
			s[i].x = 0;
			s[i].y = 0;
			group.addChild(s[i]);
		}
		this.addChild(group);
		//指スプライト
		group = new Group();
		s = this.sprite_yubi1 = new Sprite(52,84);
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		s.image = getImage("tutorial/hand_info.png");
		s.visible = false;
		group.addChild(s);
		s = this.sprite_yubi2 = new Sprite(52,84);
		s.tl.moveBy(0,-YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		s.image = getImage("tutorial/hand_info.png");
		s.visible = false;
		group.addChild(s);
		s = this.sprite_yubi3 = new Sprite(52,84);
		s.rotate(-180);
		s.tl.moveBy(0,YUBI_MOVE,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(0,-YUBI_MOVE,YUBI_MOVE_PUSHFRAME).loop();
		s.image = getImage("tutorial/hand_info.png");
		s.visible = false;
		group.addChild(s);
		this.addChild(group);
		//出口スプライト(アタッチしない、当たり判定で使う
		s = this.sprite_exit = new Sprite(80,80);
		s.x = WIDTH-s.width;
		s.y = this.sprite_blocks[0][7].y - s.height;
		this.sprite_yubi3.x = s.x + this.sprite_yubi3.width/2;
		this.sprite_yubi3.y = s.y + s.height;
		
		//メッセージ
		pos = ChipToPosition(this,2+Player.shopLevel,1);
		var wx = pos[0]+CHIPSIZE/2;
		var wy = pos[1]+64*3;
		s = this.sprite_main = new Sprite(512,200);
		s.image = getImage('tutorial/start_3.png');
		s.x = wx-s.width/2;	//PositionはInterfaceのメッセージ位置と同じ
		s.y = wy;
		s.opacity = 0;
		s.visible = true;
		this.addChild(s);
		s.tl.delay(TUTORIAL_STARTDELAY).fadeIn(0).then(function(){ playSe(se.window); tutorial_scene.tookIndex=1; tutorial_scene.sprite_lblocks[0].visible = true;});
		
		this.addEventListener(Event.TOUCH_START, function(e){
			var clickPos = [e.localX,e.localY];
			if(DEBUG)console.log("TookIndex:"+tutorial_scene.tookIndex);
			if(this.tookIndex == 1)
			{
				if(game.UI.HitPos(this.sprite_lblocks[0],clickPos) &&this.sprite_lblocks[0].box.visible)
				{
					//クリックされたら各処理
					s = this.sprite_blocks[2][3];
					this.BlockBreak(s);
					s.frame++;
					playSe(se.break_);
					tutorial_scene.p_life--;
					this.PickelLifeUpdate();
					if(s.frame == 3)
					{
						//砕ける
						s.visible = false;
						this.sprite_main.image = getImage('tutorial/start_4.png');
						this.sprite_main.tl.fadeOut(0).delay(5).fadeIn(0).then(function(){
							playSe(se.tap_1);
							tutorial_scene.tookIndex=2;
							tutorial_scene.sprite_lblocks[0].visible = false;
							tutorial_scene.sprite_lblocks[1].visible = true;
							tutorial_scene.sprite_lblocks[2].visible = true;
							tutorial_scene.sprite_lblocks[3].visible = true;
							tutorial_scene.sprite_lblocks[4].visible = true;
							});
					}
					this.sprite_lblocks[0].visible = false;
				}
			}else if(this.tookIndex == 2)
			{
				//4ヵ所タップ
				var count = 0;
				var lastBox = null;
				var lastIndex = -1;
				
				for(var i = 1; i < this.sprite_lblocks.length; i++)
				{
					if(game.UI.HitPos(this.sprite_lblocks[i],clickPos) &&this.sprite_lblocks[i].box.visible)
					{
						this.sprite_lblocks[i].box.frame++;
						this.BlockBreak(this.sprite_lblocks[i].box);
						playSe(se.break_);
						tutorial_scene.p_life--;
						this.PickelLifeUpdate();
						if(this.sprite_lblocks[i].box.frame == 3)
						{
							this.sprite_lblocks[i].box.visible = false;
							lastBox = this.sprite_lblocks[i].box;
							lastIndex = i;
						}
						
						this.sprite_lblocks[i].visible = false;
						break;
					}
				}
				for(var i = 1; i < this.sprite_lblocks.length; i++)if(!this.sprite_lblocks[i].box.visible)count++;	//終了判定
				
				if(count == 4)
				{
					var sc = tutorial_scene;
					//pos1：最後に残った箇所が左右	pos2:最後に残った箇所が上下
					var pos1_yubi1;
					var pos1_yubi2;
					var pos2_yubi1;
					var pos2_yubi2;
					
					//回収へ移行
					if(lastBox)
					{
						//鉱石移動
						this.sprite_m2.visible = true;
						this.sprite_m2.x = lastBox.x + (80-64)/2;
						this.sprite_m2.y = lastBox.y + (80-64)/2;
					}
					
					pos1_yubi1 = [ sc.sprite_m1.x + (sc.sprite_m1.width/2) - (sc.sprite_yubi1.width/2),sc.sprite_m1.y - sc.sprite_yubi1.height];	//鉱石の場所へ指移動
					pos1_yubi2 = [ sc.sprite_m2.x + (sc.sprite_m2.width/2) - (sc.sprite_yubi2.width/2), sc.sprite_m2.y - sc.sprite_yubi2.height];
					pos2_yubi1 = [ sc.sprite_m1.x - sc.sprite_yubi1.width, sc.sprite_m1.y];	//鉱石の場所へ指移動
					pos2_yubi2 = [ sc.sprite_m2.x - sc.sprite_yubi2.width, sc.sprite_m2.y];
					
					tutorial_scene.tookIndex=3;
					this.sprite_main.tl.fadeOut(0).delay(5).fadeIn(0).then(function(){
						playSe(se.tap_1);
						tutorial_scene.tookIndex = 4;
						tutorial_scene.sprite_main.image = getImage('tutorial/start_5.png');
						//指スプライト起動
						
						sc.sprite_yubi1.visible = true;
						sc.sprite_yubi2.visible = true;
						if(lastIndex == 1 || lastIndex == 2)
						{
							//上下
							sc.sprite_yubi1.x = pos2_yubi1[0];
							sc.sprite_yubi1.y = pos2_yubi1[1];
							sc.sprite_yubi2.x = pos2_yubi2[0];
							sc.sprite_yubi2.y = pos2_yubi2[1];
							sc.sprite_yubi1.rotate(-90);
							sc.sprite_yubi2.rotate(-90);
							//アニメ変更
							sc.sprite_yubi1.tl.clear();	//タイムラインの掃除
							sc.sprite_yubi2.tl.clear();
							sc.sprite_yubi1.tl.moveBy(-YUBI_MOVE,0,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(YUBI_MOVE,0,YUBI_MOVE_PUSHFRAME).loop();
							sc.sprite_yubi2.tl.moveBy(-YUBI_MOVE,0,YUBI_MOVE_RETURNFRAME).delay(YUBI_DELAYFRAME).moveBy(YUBI_MOVE,0,YUBI_MOVE_PUSHFRAME).loop();
						}else if(lastIndex == 3 || lastIndex == 4)
						{
							//左右
							sc.sprite_yubi1.x = pos1_yubi1[0];
							sc.sprite_yubi1.y = pos1_yubi1[1];
							sc.sprite_yubi2.x = pos1_yubi2[0];
							sc.sprite_yubi2.y = pos1_yubi2[1];
						}
					});
				}
				
			}
			
			if(game.UI.HitPos(this.sprite_exit,clickPos) && this.tookIndex == 5)
			{
				//出口ボタンがタッチされたら次のチュートリアルへ
				//game.pushScene(new FadeScene(shopAssets, makeNewScene(ShopScene)));
				game.pushScene(new FadeScene(tutorial_3_Assets, makeNewScene(Tutorial_3)));
				playSe(se.move);
				stopBgm(se.bgm_01);	//止まってる・・・？
				playBgm(se.bgm_main);
			}
		});
		
		this.addEventListener(Event.ENTER_FRAME, function(e){
			if(this.tookIndex == 4)
			{
				if(!this.sprite_yubi1.visible && !this.sprite_yubi2.visible)
				{
					this.sprite_yubi3.visible = true;
					this.tookIndex = 5;
				}
			}
		});
		
	},
	
	PickelLifeUpdate:function()
	{
		var s = this.sprite_lifes;
		var HP = this.p_life;
		for(var i = 0; i < s.length; i++)s[i].visible = i < HP;
		
	},
	BlockBreak:function(sprite)
	{
		//引数のspriteの4隅から破片を生成
		//まず4箇所の位置を決定
		var s = this.sprite_bblocks;
		s[0].x = sprite.x;
		s[0].y = sprite.y;
		s[1].x = sprite.x+sprite.width - s[1].width;
		s[1].y = sprite.y;
		s[2].x = sprite.x+sprite.width - s[2].width;
		s[2].y = sprite.y+sprite.height - s[2].height;
		s[3].x = sprite.x;
		s[3].y = sprite.y+sprite.height - s[3].height;
		//次に各アニメーション設定
		s[0].tl.fadeIn(0).moveBy(-40,-40,5).fadeOut(0);
		s[1].tl.fadeIn(0).moveBy(40,-40,5).fadeOut(0);
		s[2].tl.fadeIn(0).moveBy(40,40,5).fadeOut(0);
		s[3].tl.fadeIn(0).moveBy(-40,40,5).fadeOut(0);
		
	},
	
});
