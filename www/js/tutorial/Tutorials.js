﻿//各読み込みとグローバル管理

var tutorial_scene = null;	//イベントリスナが自分のシーンを参照する時に使用

//チュートリアル全ての指SpriteのTimeLineアニメーションの各数値設定
const TUTORIAL_STARTDELAY = 10;	//画面移動後の説明表示までのDelayFrame
const YUBI_DELAYFRAME = 8;
const YUBI_MOVE = 30;	//指の移動する大きさ
const YUBI_MOVE_RETURNFRAME = 6;	//指を引く時に何フレーム掛かるか
const YUBI_MOVE_PUSHFRAME = 2;		//指を押す時に何フレーム掛かるか

document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/Tutorial_1.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/Tutorial_2.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/Tutorial_3.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/TutorialMakeInfoDialog.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/TutorialChoiceStoneGroup.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/TutorialKama.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/TutorialCoalBox.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/Tutorial_4.js'></script>");