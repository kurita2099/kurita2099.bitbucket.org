﻿//品出しメニュー

MenuWeaponPop = Class.create(Object,{
	initialize:function(InterFace){
		//品出しのオブジェクトを定義する
		this.UI = InterFace;
		
		this.ivent = false;		//品出し画面が開かれているならtrue
		this.group = new Group();	//描画順を操作するため
		this.index_tab  = 1;		//今どのタブにいるのか
		this.index_page = 1;		//各ページ
		this.index_index = 0;		//現在どこをクリックしているか(1-5で0は非表示)
		this.max_page = [1,3,3,1];	//各ページの最大数
		this.oneLoad = true;		//一回だけ読み込む処理
		this.handSlot = null;			//スロット記憶
		this.handSlotSprite = null;		//現在参照しているPlayerSlotの枠sprite記憶
		
		this.zeroPosition = [ POSITION_MENU[0], POSITION_MENU[1] ];
		
		this.sprite_main = createDomSprite(null ,512,314);	//メインメニュー
		this.sprite_main._element.style.backgroundColor = "#000000";
		this.sprite_main_bg = createDomSprite(null ,512,314); //メインメニューの背景(消える時後ろが見えてしまうので黒い色で補う
		this.sprite_main_bg._element.style.backgroundColor = "#000000";
		this.sprite_close = createDomSprite(null ,90,90);	//×ボタン
		this.sprite_tab = [
				//各種タブスプライト[短剣、斧、剣、伝説]
				createDomSprite(null ,128,90),
				createDomSprite(null ,128,90),
				createDomSprite(null ,128,90),
				createDomSprite(null ,128,90),
		];
		this.sprite_maintab = createDomSprite(null ,128,90);	//一番上に見せるためのタブ画像
		
		this.sprite_page = [
				//ページ送りスプライト[前、次]
				createDomSprite(null ,192,90),
				createDomSprite(null ,192,90),
		];
		this.sprite_num = [
				//在庫表示用の数字
				[createDomSprite(null ,22,22),createDomSprite(null ,22,22)],
				[createDomSprite(null ,22,22),createDomSprite(null ,22,22)],
				[createDomSprite(null ,22,22),createDomSprite(null ,22,22)],
				[createDomSprite(null ,22,22),createDomSprite(null ,22,22)],
				[createDomSprite(null ,22,22),createDomSprite(null ,22,22)],
		];
		
		this.sprite_blackBox = [
				//隠すためのSprite
				createDomSprite(null ,16,16),
				createDomSprite(null ,16,16),
				createDomSprite(null ,16,16),
				createDomSprite(null ,16,16),
				createDomSprite(null ,16,16),
		];
		
		this.sprite_cursor = createDomSprite(null ,8,14);	//カーソルスプライト
		
		//各座標の決定
		var s;
		var zp = this.zeroPosition;
		
		setDomImage(this.sprite_close,"close.png");
		this.sprite_close.x = INTERFACE_OFFSET_WIDTH;
		this.sprite_close.x += -this.sprite_close.width -10;
		this.sprite_close.y = INTERFACE_OFFSET_HEIGHT +20;
		this.sprite_close.visible = false;
		
		this.sprite_maintab.visible = false;
		//開始位置の計算
		var wid = this.sprite_tab[0].width * this.sprite_tab.length /2;	//全てのタブの幅/2分ずらす
		zp[0] += -wid;
		
		for(var i = 0; i < this.sprite_tab.length; i++)
		{
			//タブ位置
			s = this.sprite_tab[i];
			s.x = zp[0] + i * (s.width-4)+6;
			s.y = zp[1]+4.6;
			setDomImage(s ,"InterFace/weaponPop/w_weapon_tab_"+(i+1)+".png");
			s.visible = false;
		}
		
		//見せ用タブ設定
		s = this.sprite_maintab;
		s.x = this.sprite_tab[0].x;
		s.y = this.sprite_tab[0].y;
		setDomImage(s ,"InterFace/weaponPop/w_weapon_tab_1.png");
		
		//メイン位置
		s = this.sprite_main;
		s.x = zp[0];
		s.y = zp[1] + this.sprite_tab[0].height;
		s.visible = false;
		s = this.sprite_main_bg;
		s.x = this.sprite_main.x;
		s.y = this.sprite_main.y;
		s.visible = false;
		for(var i = 0; i < this.sprite_num.length; i++)
		{
			//在庫スプライトの座標合わせ
			s = this.sprite_num[i];
			s[0].x = zp[0] + 441;
			s[0].y = zp[1] + 124 + i*57;
			s[0].visible = false;
			setDomImage(s[0] ,"InterFace/9.png");
			s[1].x = s[0].x+s[0].width;
			s[1].y = s[0].y;
			s[1].visible = false;
			setDomImage(s[1] ,"InterFace/9.png");
		}
		for(var i = 0; i < this.sprite_blackBox.length; i++)
		{
			//BlackBox
			var boxHeight = Math.floor(this.sprite_main.height/5);	//BlackBox一つあたりの高さ
			var s = this.sprite_blackBox[i];
			var osx = 0;
			var osy = 0;
			
			//等間隔ではないので手動設定
			s.x = zp[0]+10;
			s.y = zp[1] + this.sprite_tab[0].height + (i * boxHeight) + 10;
			var kakudai = [this.sprite_main.width-20,50];
			s.width = kakudai[0];
			s.height = kakudai[1];
			setDomImage(s ,"debug/ybox.png");
		}
		//ページ
		s = this.sprite_page[0];
		s.x = this.sprite_main.x;
		s.y = this.sprite_main.y + this.sprite_main.height + WINDOW_SUB_OFFSET;
		s.visible = false;
		setDomImage(s ,"InterFace/weaponPop/w_back.png");
		s = this.sprite_page[1];
		s.x = this.sprite_main.x + this.sprite_main.width - s.width;
		s.y = this.sprite_main.y + this.sprite_main.height + WINDOW_SUB_OFFSET;
		s.visible = false;
		setDomImage(s ,"InterFace/weaponPop/w_next.png");
		
		s = this.sprite_cursor;
		s.visible = false;
		s.x = this.sprite_blackBox[0].x;
		s.y = this.sprite_blackBox[0].y;
		setDomImage(s ,"cursor_s.png");
		
		this.setImage();
	},
	
	setImage:function()
	{
		//自身のデータに基づいてメイン画面を切り替える
		var s = this.sprite_main;
		var FADEINTIME = 6;
		setDomImage(s ,"InterFace/weaponPop/w_weapon_"+this.index_tab+"_"+this.index_page+".png");
		this.sprite_main.opacity = 0;
		this.sprite_main.tl.fadeIn(FADEINTIME);
		
		//ページ
		for(var i = 0; i < this.sprite_page.length; i++)
		{
			s = this.sprite_page[i];
			s.visible = !(this.index_tab == 1 || this.index_tab == 4);
		}
		
		var IDs = [];	//参照するIDを覚える
		var ID = 0;
		if(this.index_tab == 2)ID += W_N;
		if(this.index_tab == 3)ID += W_N+W_O;
		for(var i = 0; i < 5; i++)
		{
			//ページの項目分回す
			IDs.push(i + ID + (this.index_page-1) * 5);
		}
		
		if(this.index_tab == 4)
		{
			//伝説タブなら指定IDを覚える
			IDs = [W_N-1,  W_N+W_O-1,  W_N+W_O+W_S-1,  -1,-1];
		}
		
		for(var i = 0; i < this.sprite_num.length; i++)
		{
			if(IDs[i] != -1)
			{
				//例外IDじゃなければ処理開始
				var bank = game.InventoryManager.getWeaponBank();	//全ての在庫取得
				var num = bank[IDs[i]];						//一つの在庫数取得
				
				s = this.sprite_num[i][0];
				setDomImage(s ,"s"+this.UI.getPlace(num)[1]+".png");
				s.opacity = 0;
				s.tl.fadeIn(FADEINTIME);
				s = this.sprite_num[i][1];
				setDomImage(s ,"s"+this.UI.getPlace(num)[0]+".png");
				s.opacity = 0;
				s.tl.fadeIn(FADEINTIME);
			}
		}
		
		for(var i = 0; i < this.sprite_blackBox.length; i++)
		{
			//目隠し
			s = this.sprite_blackBox[i];
			s.visible = (Player.recipe[ IDs[i] ] == 0 || IDs[i] == -1);
			if(!this.ivent)s.visible = false;
		}
		
		//カーソル
		s = this.sprite_cursor;
		if(this.index_index != 0)
		{
			s.x = this.sprite_blackBox[this.index_index-1].x+20;
			s.y = this.sprite_blackBox[this.index_index-1].y+32 -(this.index_index*6);	//等間隔じゃないので縮めていく
			s.visible = true;
		}else
		{
			s.visible = false;
		}
		
	},
	
	Touch:function(clickPos)
	{
		//スプライトのタッチ判定(タッチスタート)
		//UIの方にあるメソッドの参照を記憶
		var HIT = this.UI.HitPos;
		var s;
		var change = false;
		if(!this.ivent)return;
		
		for(var i = 0; i < this.sprite_tab.length; i++)
		{
			s = this.sprite_tab[i];
			if(HIT(s,clickPos) && s.visible)
			{
				if(this.index_tab != i+1)
				{
					//タブを押して、同じタブを押していないなら
					//タブインデックスを切り替える
					this.index_tab = i+1;
					this.index_page = 1;	//ページ初期化
					this.index_index = 0;	//項目初期化
					//見せ用のmaintabを操作
					this.sprite_maintab._element.style.backgroundImage = this.sprite_tab[i]._element.style.backgroundImage;
					this.sprite_maintab.x = this.sprite_tab[i].x;
					this.sprite_maintab.y = this.sprite_tab[i].y;
					playSe(se.tap_1);
					change = true;
				}
			}
		}
		
		s = this.sprite_page[0];
		if(HIT(s,clickPos) && s.visible)
		{
			//ページを戻す
			this.index_page--;
			playSe(se.tap_1);
			//戻した後0以下になったらその項の最大ページへ
			if(this.index_page <= 0)this.index_page = this.max_page[this.index_tab-1];
			change = true;
		}
		s = this.sprite_page[1];
		if(HIT(s,clickPos) && s.visible)
		{
			//ページを進める
			this.index_page++;
			playSe(se.tap_1);
			//超えたら0ページへ
			if(this.index_page > this.max_page[this.index_tab-1])this.index_page = 1;
			change = true;
		}
		
		var bank = game.InventoryManager.getWeaponBank();	//全ての在庫取得
		var IDs = [];	//参照するIDを覚える
		var ID = 0;
		if(this.index_tab == 2)ID += W_N;
		if(this.index_tab == 3)ID += W_N+W_O;
		for(var i = 0; i < 5; i++)
		{
			//ページの項目分回す
			IDs.push(i + ID + (this.index_page-1) * 5);
		}
		if(this.index_tab == 4)
		{
			//伝説タブなら指定IDを覚える
			IDs = [W_N-1,  W_N+W_O-1,  W_N+W_O+W_S-1,  -1,-1];
		}
		
		for(var i = 0; i < this.sprite_blackBox.length; i++)
		{
			s = this.sprite_blackBox[i];
			if(!s.visible && HIT(s,clickPos) )
			{
				//もし目隠しが取れているなら判定開始
				var id = IDs[i];	//クリックした項目の武器ID
				if(this.handSlot[0] == id)
				{
					//もしidが同じなら倉庫へ戻す
					this.handSlot[0] = -1;
					this.handSlot[1] = -1;
					playSe(se.tap_2);
				}else if(bank[ IDs[i] ] > 0)
				{
					//在庫があるならスロットに入れる
					this.handSlot[0] = id;
					this.handSlot[1] = bank[IDs[i]];						//一つの在庫数取得
					if(this.handSlot[1] >= SLOT_MAX_IN[Player.shopLevel] )this.handSlot[1] = SLOT_MAX_IN[Player.shopLevel];	//もしスロットの最大数を超えていたら、最大数まで戻す
					playSe(se.tap_1);
				}
				this.index_index = i+1;
				change = true;
				
			}
		}
		
		
		//もし変更されるならImage更新
		if(change || this.oneLoad)
		{
			this.setImage();
			this.oneLoad = false;
		}
		
		//×ボタンが押されたら
		s = this.sprite_close;
		if(HIT(s,clickPos) && s.visible)
		{
			this.IventOff();
			playSe(se.tap_2);
			
			this.UI.WindowViewSet(WINDOW_SV_TRUE);	//イベント会話を出現させる
		}
		
		
	},
	
	Atach:function()
	{
		//シーンに乗せる
		this.group = new Group();	//グループ初期化
		
		var s;
		var s_last;	//タブ用
		var scene = this.UI.scene;	//UIが現在参照しているscene
		
		for(var i = 0; i < this.sprite_tab.length; i++)
		{
			//タブ
			s = this.sprite_tab[i];
			scene.addChild(s);
		}
		
		s = this.sprite_main_bg;	//背景
		scene.addChild(s);
		s = this.sprite_main;	//メインメニュー
		scene.addChild(s);
		s = this.sprite_maintab;	//見せ用タブ
		scene.addChild(s);
		s = this.sprite_close;
		scene.addChild(s);
		
		for(var i = 0; i < this.sprite_num.length; i++)
		{
			//在庫スプライトの座標合わせ
			var s = this.sprite_num[i];
			scene.addChild(s[0]);
			scene.addChild(s[1]);
		}
		
		for(var i = 0; i < this.sprite_blackBox.length; i++)
		{
			//BlackBox
			var s = this.sprite_blackBox[i];
			scene.addChild(s);
		}
		
		for(var i = 0; i < this.sprite_page.length; i++)
		{
			//ページ
			s = this.sprite_page[i];
			scene.addChild(s);
		}
		
		s = this.sprite_cursor;
		scene.addChild(s);
		
	},
	
	IventOn:function()
	{
		//イベントの開始で呼ばれるメソッド*呼ばれるメソッドの順序はIventOn→Set_Handslot。都合のため独立
		this.ivent = true;
		this.setVisible(true);	//一度全てを操作
		
		this.UI.WindowViewSet(WINDOW_SV_FALSE);	//イベント会話を隠す
	},
	
	Set_Handslot:function(slotImageSprite,slotIndex)
	{
		//イベントで使用するスロットを設定するメソッド
		//slotImagesprite:枠のsprite参照	slotIndex:Playerのslot番号
		this.handSlot = Player.slot[0][slotIndex];
		this.handSlotSprite = slotImageSprite;
		this.handSlotSprite.image = getImage("slot_2.png");	//枠色変更
		this.oneLoad = true;
		this.setImage();
	},
	
	IventOff:function()
	{
		//イベントの終了で呼ばれるメソッド
		//slotImagesprite:枠のsprite参照
		this.ivent = false;
		this.handSlot = null;
		this.setVisible(false);
		if(this.handSlotSprite != null)
		{
			this.handSlotSprite.image = getImage("slot_1.png");	//何もクリックされなかった場合を除いて、枠色変更
		}
		this.handSlotSprite = null;
		
	},
	
	ChangeSlot:function(slotImageSprite,slotIndex)
	{
		if(!this.ivent)return;
		//参照対象を引数のslotへ変更
		this.handSlot = Player.slot[0][slotIndex];
		this.handSlotSprite.image = getImage("slot_1.png");	//戻す
		this.handSlotSprite = slotImageSprite;
		this.handSlotSprite.image = getImage("slot_2.png");	//変更
	},
	
	setVisible:function(ans)
	{
		//全てのvisibleを操作する(true or false)
		var s;
		s = this.sprite_main;	//メインメニュー
		s.visible = ans;
		s = this.sprite_main_bg;	//メインメニュー
		s.visible = ans;
		
		var legend = (Player.recipe[5] > 0) || (Player.recipe[21] > 0) || (Player.recipe[37] > 0);	//伝説の武器を作成しているならばtrue
		
		for(var i = 0; i < this.sprite_tab.length; i++)
		{
			//タブ、伝説の武器がレシピにあるならタブを描画する
			if(i < this.sprite_tab.length-1)
			{
				s = this.sprite_tab[i];
				s.visible = ans;
			}else if( i == this.sprite_tab.length-1 && legend)
			{
				s = this.sprite_tab[i];
				s.visible = ans;
			}
		}
		
		this.sprite_maintab.visible = ans;
		this.sprite_close.visible = ans;
		
		for(var i = 0; i < this.sprite_page.length; i++)
		{
			s = this.sprite_page[i];
			s.visible = ans;
		}
		
		for(var i = 0; i < this.sprite_num.length; i++)
		{
			//在庫スプライトの座標合わせ
			var s = this.sprite_num[i];
			s[0].visible = ans;
			s[1].visible = ans;
		}
		
		for(var i = 0; i < this.sprite_blackBox.length; i++)
		{
			//BlackBox
			var s = this.sprite_blackBox[i];
			s.visible = ans;
		}
		
		s = this.sprite_cursor;
		s.visible = ans;
		
	},
	
});