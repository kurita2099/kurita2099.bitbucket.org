﻿//増築メニュー及びショップのレベルアップを制御、処理する

//増築シーケンス
const SEQUENCE_MENU_SP_NONE = 0;		//何もしていない状態
const SEQUENCE_MENU_SP_OPEN = 1;		//増築ボタンが押され変化状態
const SEQUENCE_MENU_SP_TIMEVIEW = 2;	//増築ボタンが押され変化状態
const SEQUENCE_MENU_SP_NOVIEW = 3;		//何も描画しない状態
const SHOP_EXPLICE = [ [1000,2],[5000,10],[30000,60] ];	//増築料、0,0:ゴールド  0,1:ダイヤ

MenuShoplevelUp = Class.create(Object,{
	initialize:function(InterFace){
		this.UI = InterFace;
		
		this.lockTime = 0;	//重複処理制御用
		
		this.sprite_extendChips = [];	//増築用のスプライトをここに記憶しておく(利用するのはMain.jsのメソッドのみ)
		for(var i = 0; i < 13*4; i++)
		{
			var s = new Sprite(64,64);
			s.image = getImage("silk_icon_merged.png");
			s.visible = false;
			s.frame = 9;
			this.sprite_extendChips.push(s);
		}
		
		
		
		//増築のメニューオブジェクトを定義する
		this.sceneIndex = this.UI.sceneIndex;		//シーン種類記憶
		this.zeroPosition = [ 10, INTERFACE_OFFSET_HEIGHT+10 ];
		
		this.menuIndex = 0;	//ボタンの現在の状態、0ならゴールド支払い、1ならダイヤ支払い
		this.timer_button = 0;	//ボタンが押されて条件が満たされたら計測される時間
		this.menuSequence = SEQUENCE_MENU_SP_NOVIEW;
		
		var s;
		var zp = this.zeroPosition;
		
		s = this.sprite_main = createDomSprite("w_reform_"+ (Player.shopLevel+1)  +".png" ,250,90);		//増築購入ボタン
		s.x = zp[0];
		s.y = zp[1];
		s.visible = false;
		
		s = this.sprite_time = new Sprite(250,90);
		s.image = getImage("w_reform_time.png");
		s.x = WIDTH/2 - s.width/2;
		s.y = HEIGHT/2 - s.height/2;
		s.visible = false;
		
		this.position_nums = [	[179,34],	[158,34],	[96,34],	[76,34],	];//一桁から順にsprite_timeに対して相対座標
		this.sprite_nums = [];
		for(var i = 0; i < 4; i++)
		{
			s = new Sprite(22,22);
			s.x = this.sprite_time.x + this.position_nums[i][0];
			s.y = this.sprite_time.y + this.position_nums[i][1];
			s.visible = false;
			this.sprite_nums.push(s);
			
		}
	},
	
	EnterFrame:function()
	{
		//二秒間の切り替え判定、増築中の時間表示。UIの方のEnterへ埋め込む
		
		
		switch(this.menuSequence)
		{
			case SEQUENCE_MENU_SP_OPEN:
			{
				var time = (new Date() - this.timer_button) / 1000;
				if(time >= 2)
				{
					//指定時間が過ぎたら、ボタンを元の状態へ戻す
					setDomImage(this.sprite_main,"w_reform_"+ (Player.shopLevel+1)  +".png" ,250,90);
					this.menuIndex = 0;
					this.menuSequence = SEQUENCE_MENU_SP_NONE;
				}
				break;
			}
			
			case SEQUENCE_MENU_SP_TIMEVIEW:
			{
				if(Player.shopLevelNowExtend)
				{
					var time = (new Date() - Player.shopLevelTime) / 1000;
					time = Math.floor(time);

					if(this.lockTime != time)
					{
						this.lockTime = time;
						
						//num画像更新
						var endTime = SHOP_EXTEND_TIME - time;
						if(endTime < 0)endTime = 0;
						
						var min = Math.floor(endTime/60);
						var sec = Math.floor(endTime%60);
						//超えたら恐らくエラー
						var min1 = Math.floor(min%10);
						var min2 = Math.floor(min/10);
						var sec1 = Math.floor(sec%10);
						var sec2 = Math.floor(sec/10);
						console.log("sec1: "+sec1);
						this.sprite_nums[0].image = getImage("s"+sec1+".png");
						this.sprite_nums[1].image = getImage("s"+sec2+".png");
						this.sprite_nums[2].image = getImage("s"+min1+".png");
						this.sprite_nums[3].image = getImage("s"+min2+".png");
						
						if(time > SHOP_EXTEND_TIME)
						{
							//設定された増築時間を超えたら移行
							this.lockTime = 0;
							this.menuSequence = SEQUENCE_MENU_SP_NOVIEW;
							this.ChangeVisible();
							Player.shopLevelNowExtend = false;
							Player.shopLevel++;
							game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
						}
						
					}
				}
				break;
			}
		}
		
	},
	
	Touch:function(clickPos)
	{
		
		//UIの方にあるメソッドの参照を記憶
		var HIT = this.UI.HitPos;
		var s;
		
		s = this.sprite_main;
		if(s.visible && HIT(s,clickPos) && Player.shopLevel < MAX_SHOPLEVEL)
		{
			//メニューがクリックされたら&&増築可能状態ならば
			
			var LV = Player.shopLevel;
			var index = this.menuIndex;
			var playMoney = index == 0 ? Player.coins : Player.diamond;	//現在のボタン種によって判定先変更
			var checkMoney = SHOP_EXPLICE[LV][index];
			if(checkMoney <= playMoney)
			{
				//所持金が満たされているなら
				if(index == 0){ Player.coins += -SHOP_EXPLICE[LV][0]; }else{ Player.diamond += -SHOP_EXPLICE[LV][1]; }	//所持金減算
				Player.shopLevelNowExtend = true;
				Player.shopLevelTime = Date.parse(new Date());
				this.menuSequence = SEQUENCE_MENU_SP_TIMEVIEW;	//タイマー描画モードへ移行する
				
				//visible操作
				this.ChangeVisible();
				console.log("visible:"+this.sprite_nums[0].visible);
				game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
				
			}else
			{
				//金が足りない
				playSe(se.tap_2);
				
				//もしゴールドボタンならダイヤボタンへと切り替える
				if(this.menuIndex == 0)
				{
					this.menuIndex = 1;
					setDomImage(this.sprite_main,"w_reform_"+ (Player.shopLevel+1)  +"b.png" ,250,90);
					this.timer_button = new Date();
					this.menuSequence = SEQUENCE_MENU_SP_OPEN;	//開いた状態でタイマー判定開始
				}
			}
		}
		
	},
	
	Atach:function()
	{
		var s;
		var scene = this.UI.scene;	//現在のシーン
		this.menuIndex = 0;	//一応初期化しておく
		this.ivent = false;
		
		s = this.sprite_main;
		scene.addChild(s);
		
		if(this.UI.sceneIndex == SCENE_STATE_SHOP)
		{
			//ショップシーンでのみaddChild
			s = this.sprite_time;
			scene.addChild(s);
			
			s = this.sprite_nums;
			for(var i = 0; i < s.length; i++)scene.addChild(s[i]);
		}
		
		this.CheckCanShopExtend();
		this.ChangeVisible();
		
		
	},
	
	CheckCanShopExtend:function()
	{
		//増築可能な状態に移行できるか判断、処理
		if(!Player.shopLevelNowExtend && Player.shopLevel < Player.shopLevelMax)
		{
			this.menuSequence = SEQUENCE_MENU_SP_NONE;
			setDomImage(this.sprite_main,"w_reform_"+ (Player.shopLevel+1)  +".png" ,250,90);	//mainの画像更新
		}else if(Player.shopLevelNowExtend)
		{
			//増築中ならタイマーモードへ
			this.menuSequence = SEQUENCE_MENU_SP_TIMEVIEW;
		}else
		{
			this.menuSequence = SEQUENCE_MENU_SP_NOVIEW;
		}
	},
	
	ChangeVisible:function()
	{
		//シークエンスに合わせて該当SpriteVisible操作
		switch(this.menuSequence)
		{
			case SEQUENCE_MENU_SP_NOVIEW:
			{
				this.sprite_main.visible = false;
				this.sprite_time.visible = false;
				s = this.sprite_nums;
				for(var i = 0; i < s.length; i++)s[i].visible = false;
				break;
			}
			
			case SEQUENCE_MENU_SP_NONE:
			{
				this.sprite_main.visible = true;
				this.sprite_time.visible = false;
				s = this.sprite_nums;
				for(var i = 0; i < s.length; i++)s[i].visible = false;
				break;
			}
			case SEQUENCE_MENU_SP_OPEN:
			{
				this.sprite_main.visible = true;
				this.sprite_time.visible = false;
				s = this.sprite_nums;
				for(var i = 0; i < s.length; i++)s[i].visible = false;
				break;
			}
			
			case SEQUENCE_MENU_SP_TIMEVIEW:
			{
				this.sprite_main.visible = false;
				this.sprite_time.visible = true;
				s = this.sprite_nums;
				for(var i = 0; i < s.length; i++)s[i].visible = true;
				break;
			}
		}
		
		
		
	},
	
	
});