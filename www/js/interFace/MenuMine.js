﻿//鉱山メニュー

const MINE_NEEDLIFE = [1,2,3,4,5];	//各鉱山の通行料(ハート)
const MINE_NEEDDIA = [1,2,3,4,5];	//各鉱山の通行料(ダイア)
const MINE_PAGE_GO = 1;		//鉱山出発ページ
const MINE_PAGE_NOHEART = 2;//ダイア消費ページ
const MINE_PAGE_CLOSE = 3;	//全て非表示

MenuMine = Class.create(Object,{
		
	state:null,		//現在の操作状態
	menuIndex:null,	//現在選択している要素(0-4)
		
	initialize:function(InterFace){
		//鉱山のメニューオブジェクトを定義する
		this.UI = InterFace;
		
		this.ivent = false;		//品出し画面が開かれているならtrue
		this.sceneIndex = this.UI.sceneIndex;		//シーン種類記憶
		this.zeroPosition = [ POSITION_MENU[0], POSITION_MENU[1] ];
		this.change = false;	//画面を更新制御するための物
		
		//ツールによる吐き出し、カーソルの位置座標(mainに対して相対座標)
		this.cursorPos_GO = [	[31,39],	[31,94],	[31,151],	[31,205],	[31,263],	];
		
		var s;
		var zp = this.zeroPosition;
		
		//メインメニュー
		s = this.sprite_main = createDomSprite("shop/w_kouzan.png",512,314);
		zp[0] += -(s.width/2);	//メニュー分ずらす
		
		s.x = zp[0];
		s.y = zp[1];
		s.visible = false;
		
		//出発ボタン
		s = this.sprite_go　= createDomSprite("w_go.png",192,90);
		s.x = this.sprite_main.x + this.sprite_main.width/2;
		s.x += -s.width/2;
		s.y = this.sprite_main.y + this.sprite_main.height + WINDOW_SUB_OFFSET;
		s.visible = false;
		
		//ダイア状態の上から被せるDiaSprite
		this.sprite_dias = [];
		for(var i = 0; i < 5; i++)
		{
			s = createDomSprite("shop/w_kouzan_dia.png",28,24);
			s.visible = false;
			s.x = this.cursorPos_GO[i][0] + this.sprite_main.x + 365;
			s.y = this.cursorPos_GO[i][1] + this.sprite_main.y - 4;
			this.sprite_dias.push(s);
		}
		
		//鉱山目隠し
		this.sprite_blackBox_menu = [];
		
		for(var i = 0; i < 5; i++)
		{
			var kakudai;
			this.sprite_blackBox_menu.push(createDomSprite("debug/ybox.png",16,16) );
			s = this.sprite_blackBox_menu[i];
			kakudai = [454,48];	//ピクセル単位で拡大縮小
			s.width = kakudai[0];
			s.height = kakudai[1];
			s.visible = false;
		}
		//等間隔ではないので個別に座標指定
		//メニューの目隠し
		s = this.sprite_blackBox_menu;
		s[0].x = zp[0]+22+22;
		s[0].y = zp[1]+20;
		s[1].x = s[0].x;
		s[1].y = s[0].y+58;
		s[2].x = s[1].x;
		s[2].y = s[1].y+56;
		s[3].x = s[2].x;
		s[3].y = s[2].y+58;
		s[4].x = s[3].x;
		s[4].y = s[3].y+56;
		
		s = this.sprite_cursor = createDomSprite("cursor_s.png",8,14);
		
		this.StateInit();	//初期化
		this.state = MINE_PAGE_CLOSE;
		this.ChangePage();
	},
	
	StateInit:function()
	{
		//各種データを初期化する
		this.menuIndex = 0;
		this.state = MINE_PAGE_CLOSE;
	},
	
	Touch:function(clickPos)
	{
		if(!this.ivent)return;
		if(DEBUG)console.log("MenuMineTouchFunction");
		//UIの方にあるメソッドの参照を記憶
		var s;
		
		this.change = false;	//画面更新制御
		
		this.Touch_Select_Mine(clickPos);//鉱山項目タッチ
		this.Touch_GO(clickPos);//出発項目タッチ

			
		
		if(this.change)this.ChangePage();
	},
	
	IventOn:function()
	{
		if(this.ivent)return;	//既にONなら戻る
		this.ivent = true;
		this.StateInit();
		this.state = MINE_PAGE_GO;
		this.ChangePage();
		
		this.UI.WindowViewSet(WINDOW_SV_FALSE);	//イベント会話を隠す
	},
	
	IventOff:function()
	{
		this.ivent = false;
		this.StateInit();
		this.ChangePage();
	},
	
	ChangePage:function()
	{
		//現在のStateを参照して描画操作
		switch(this.state)
		{
			case MINE_PAGE_GO:
			{
				//各SpriteのVisible操作
				this.sprite_cursor.visible = true;
				this.sprite_main.visible = true;
				this.sprite_go.visible = true;
				
				//カーソル更新
				this.ChangeSpritePosition();
				
				for(var i = 0; i < this.sprite_blackBox_menu.length; i++)
				{
					s = this.sprite_blackBox_menu[i];
					//プレイヤーの鉱山レベルによって目隠しを取る
					s.visible = !(Player.mineLevel >= i);
					this.sprite_dias[i].visible = false
				}
				break;
			}
			case MINE_PAGE_NOHEART:
			{
				this.sprite_cursor.visible = true;
				this.sprite_main.visible = true;
				this.sprite_go.visible = true;
				
				//カーソル更新
				this.ChangeSpritePosition();
				
				for(var i = 0; i < this.sprite_blackBox_menu.length; i++)
				{
					s = this.sprite_blackBox_menu[i];
					//プレイヤーの鉱山レベルによって目隠しを取る
					s.visible = !(Player.mineLevel >= i);
					this.sprite_dias[i].visible = true
				}
				
				break;
			}
			case MINE_PAGE_CLOSE:
			{
				this.sprite_cursor.visible = false;
				this.sprite_main.visible = false;
				this.sprite_go.visible = false;

				for(var i = 0; i < this.sprite_blackBox_menu.length; i++){this.sprite_blackBox_menu[i].visible = false;this.sprite_dias[i].visible = false}
				
				break;
			}
		}
	},
	
	ChangeSpritePosition:function()
	{
		//現在のデータに合わせて座標だけを更新
				
		//カーソル更新
		s = this.sprite_cursor;
		s.x = this.cursorPos_GO[this.menuIndex][0] + this.sprite_main.x;
		s.y = this.cursorPos_GO[this.menuIndex][1] + this.sprite_main.y;

	},
	
	Atach:function()
	{
		var s;
		var scene = this.UI.scene;
		
		s = this.sprite_main;
		scene.addChild(s);
		s = this.sprite_go;
		scene.addChild(s);
		
		for(var i = 0 ; i < this.sprite_blackBox_menu.length; i++)
		{
			s = this.sprite_dias[i];
			scene.addChild(s);
			
			s = this.sprite_blackBox_menu[i];
			s.visible = false;
			scene.addChild(s);
		}
		
		s = this.sprite_cursor;
		scene.addChild(s);
	},
	
	Touch_Select_Mine:function(TouchPosition)
	{
		//鉱山項目タッチ
		for(var i = 0; i < this.sprite_blackBox_menu.length; i++)
		{
			s = this.sprite_blackBox_menu[i];
			if(!s.visible)
			{
				//目隠しが外れていたら押す事ができる
				if(game.UI.HitPos(s,TouchPosition) )
				{
					//押されたら
					if(this.menuIndex != i)
					{
						//押された項目が、前の項目とは違うならばIndexを挿入
						this.menuIndex = i;
						this.ChangeSpritePosition();
						playSe(se.tap_1);
					}
				}
				
			}
		}
	},
	
	Touch_GO:function(TouchPos)
	{
		//出発項目タッチ
		s = this.sprite_go;
		if(this.UI.HitPos(s,TouchPos) && s.visible)
		{
		
			//鉱山入場判定
			if(DEBUG)
			{
				console.log("PlayerLife:"+Player.life);
				console.log("MenuIndex:"+this.menuIndex);
			}
			
			switch(this.state)
			{
				case MINE_PAGE_GO:
				{
					if(Player.life >= MINE_NEEDLIFE[this.menuIndex])
					{
						//鉱山に行くためのライフの数が設定以上ならば、ライフを減らし鉱山への移行処理
						Player.life += -MINE_NEEDLIFE[this.menuIndex];
						
					　　if(plugin && Player.op_hart){
					　　//大窪の追加処理、ライフの減る量が変化したため時間、呼ばれた項目毎にライフ時間の計測
					　　var lifetime = new Date(new Date().getTime() +  (LIFE_COOLTIME*MINE_NEEDLIFE[this.menuIndex]) * 1000);
						window.plugins.localNotification.add(
						{
						date:lifetime,
						message:LIFE_NOTIFICATION_MESS,
						action:"OPEN",
						id :LIFE_NOTIFICATION
						});
					　　}
					　　
						this.change = true;
						game.pushScene(new FadeScene(mineAssets,makeNewScene(MineScene,this.menuIndex)));
					    playSe("sound/run.wav");
					    
					    if(this.sceneIndex == SCENE_STATE_SMITHY)
					    {
					    	//もし鍛冶屋シーンから飛ぶとき、セーブ
					  		smithyScene.save();
							smithyScene = null;
					    }
					    
					    this.IventOff();
					}else
					{
						//購入できない場合、ページ更新
						this.state = MINE_PAGE_NOHEART;
						this.change = true;
						playSe(se.tap_2);
					}
					break;
				}
				case MINE_PAGE_NOHEART:
				{
					if(Player.diamond >= MINE_NEEDDIA[this.menuIndex])
					{
						//鉱山に行くためのライフの数が設定以上ならば、ライフを減らし鉱山への移行処理
						Player.diamond += -MINE_NEEDDIA[this.menuIndex];
						
						this.change = true;
						game.pushScene(new FadeScene(mineAssets,makeNewScene(MineScene,this.menuIndex)));
					    playSe("sound/run.wav");
					    
					    if(this.sceneIndex == SCENE_STATE_SMITHY)
					    {
					    	//もし鍛冶屋シーンから飛ぶとき、セーブ
					  		smithyScene.save();
							smithyScene = null;
					    }
					    
					    this.IventOff();
					}else
					{
						playSe(se.tap_2);
					}
					break;
				}
			}
		}
		
	},
	
	
});