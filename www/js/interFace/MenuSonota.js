﻿//その他オプションメニュー

//その他ボタンの並び
const BUTTON_SONOTA_STRING_OLD = [
								["ハート回復の通知","世界地図を見る"],
								["武器完売の通知","売上ランキング"],
								["武器完成の通知","ツイッターで報告"],
								["基本操作の説明","レビューを書く"],
								["ダイヤの購入","他のアプリ"],
								];
								
const BUTTON_SONOTA_STRING = [
								["通知設定","世界地図","ツイッター"],
								["基本操作","ランキング","他のアプリ"],
								];

//何か開いてる状態
const SONOTA_STATE_NONE = 0;
const SONOTA_STATE_MAP = 1;

MenuSonota = Class.create(Object,{
	initialize:function(InterFace){
		//その他オブジェクトを定義する
		
		this.UI = InterFace;			//インターフェイスにアクセスするために記憶
		this.ivent = false;		//その他画面が開かれているならtrue
		this.zeroPosition = [ POSITION_MENU[0], POSITION_MENU[1] ];
		this.state = SONOTA_STATE_NONE;	//今どのボタンが押されて開かれているか、開かれていたら他の項目を起動させない
		
		
		this.bookPos = new Sprite(64,64);	//その他メニューを開く本の位置を記憶(hit関数に渡すためsprite)
		this.diaPos = new Sprite(64,64);	//ダイアメニューを開くための物、上記同上
		
		var s;
		var zp = this.zeroPosition;
		
		//その他メニュー本体
		s = this.sprite_menu = createDomSprite("w_setup.png" ,512,314);		//その他メニュー
		s.x = zp[0];
		s.y = zp[1];
		s.x += -(s.width/2);
		
		s = this.sprite_popKnow = createDomSprite("w_setup_p.png" ,64,64);	//通知設定が無効状態の画像
		s.x = this.sprite_menu.x + 64;
		s.y = this.sprite_menu.y + 34;

		
		//「×」ボタン
		s = this.sprite_close = new Sprite(90,90);
		s.image = getImage("close.png");
		s.x = CLOSE_OFFSET_X;
		s.y = CLOSE_OFFSET_Y;
		
		//map画像
		s = this.sprite_map = createDomSprite("shop/map.png",640,1136);
		s.x = 0;
		s.y = 0;
		s.opacity = 0;	//見えなくする
		
		
		this.setVisible(false);
		
		
	},
	
	Touch:function(clickPos)
	{
		//if(!this.ivent)return;
		//shop画面以外なら操作しない
		if(this.UI.sceneIndex != SCENE_STATE_SHOP)return;
		
		//UIの方にあるメソッドの参照を記憶
		var HIT = this.UI.HitPos;
		var s;
		var change = false;	//必要な時だけ更新処理を行うための物
		
		//その他ボタンの機能
		s = this.sprite_menu;
		if(s.visible && HIT(s,clickPos) )
		{
			var ix = -1;							//6項目を割り出す指標
			var iy = -1;
			var wid = Math.floor(s.width/3);	//1項目の幅高さ
			var hei = Math.floor(s.height/2);
			var plx = clickPos[0] - s.x;		//相対座標
			var ply = clickPos[1] - s.y;
			
			//割り出し
			ix = Math.floor(plx/wid);
			iy = Math.floor(ply/hei);
			
			if(ix == -1 || iy == -1)return;	//エラー
			
			playSe(se.tap_1);
			if(DEBUG)console.log(BUTTON_SONOTA_STRING[iy][ix]);
			
			//タッチされた項目によって各処理を実行
			switch(BUTTON_SONOTA_STRING[iy][ix])
			{
				case "通知設定":
				{
					//ハート通知,武器完売通知、武器完成通知
					Player.op_hart = Player.op_sell = Player.op_make = !Player.op_hart;	//全部一緒になったが後々変更の可能性があるのであえて残す
					this.sprite_popKnow.visible = !Player.op_hart;	//通知が有効なら被せない
					
					if(plugin)
					{
						if(Player.op_hart == false)window.plugins.localNotification.cancel(LIFE_NOTIFICATION);
						if(Player.op_sell == false)window.plugins.localNotification.cancel(WEAPON_S_NOTIFICATION);
						if(Player.op_make == false)window.plugins.localNotification.cancel(WEAPON_M_NOTIFICATION);
					}
					
					break;
				}
				case "世界地図":
				{
					this.state = SONOTA_STATE_MAP;
					this.sprite_map.tl.fadeIn(0);
					break;
				}
				
				case "基本操作":
				{
					game.pushScene(createHowTo());
					break;
				}
				
				case "ツイッターで報告":
				{
					var hako1 = "伝説の武器屋で";
					var hako2 = "コインを獲得!";
	    			var hako3 = APPURL;
		 			if(plugin){
	     				window.plugins.twitter.composeTweet(
	    				function(s){ ; },
	    				function(e){ ; },
	    				hako1 + Player.coins + hako2+ hako3);
						
					}
					break;
				}
				
				case "売上ランキング":
				{
				if(plugin){
                           try{
                           window.plugins.gamecenter.showLeaderboard(function(){;},function(){;},CATEGORY);
                           }catch(e){
                           alert(e.stack);
                           }
					}
					break;
				}
				
				case "他のアプリ":
				{
					location.href = 'http://qooga.com/app/';
					break;
				}
				

				case "レビューを書く":	//これはエンディングの場所に突っ込む予定
				{
					location.href = APPURL;
					break;
				}

			}
		}
		
		s = this.sprite_close;
		if(HIT(s,clickPos) && s.visible)
		{
			this.IventOff();
			playSe(se.tap_2);
			
			this.UI.WindowViewSet(WINDOW_SV_TRUE);	//イベント会話を出現させる
		}
		
		//何故かここでショップの本の当たり判定、必要があれば修正
		s = this.bookPos;
		if( HIT(s,clickPos))
		{
			if(this.ivent)
			{
			}else
			{
				this.UI.OpenObjectWindow(OB_OPEN_SONOTA);
			}
		}
		//ダイアチップ当たり判定
		s = this.diaPos;
		if( HIT(s,clickPos))
		{
			if(this.ivent)
			{
			}else
			{
				this.UI.WindowViewSet(WINDOW_SV_FALSE);
				game.pushScene(createDiamond());
			}
		}
	},
	
	IventOn:function()
	{
		if(this.ivent)return;	//既にONなら戻る
		this.ivent = true;
		this.sprite_menu.visible = true;
		this.sprite_popKnow.visible = !Player.op_hart;	//通知が有効なら被せない
		this.sprite_close.visible = true;
		
		this.UI.WindowViewSet(WINDOW_SV_FALSE);	//イベント会話を隠す(TRUEは各クローズ時に行う必要がある(OFF命令は複数回呼ばれるため））
	},
	
	IventOff:function()
	{
		this.ivent = false;
		this.setVisible(false);	//全部非表示
		
	},
	
	setVisible:function(ans)
	{
		var s;
		
		s = this.sprite_menu;
		s.visible = ans;
		
		s = this.sprite_popKnow;
		s.visible = ans;
		
		s = this.sprite_close;
		s.visible = ans;
		
		//s = this.sprite_map;	//mapは特殊なので組み込まない
		//s.visible = ans;
	},
	
	Atach:function()
	{
		var s;
		var scene = this.UI.scene;
		var chipPos = ChipToPosition(this.UI.scene,(Player.shopLevel*2)+3,2);	//その他メニューを開く本の位置を取得する
		this.bookPos.x = chipPos[0];
		this.bookPos.y = chipPos[1];
		chipPos = ChipToPosition(this.UI.scene,1,2);
		this.diaPos.x = chipPos[0];
		this.diaPos.y = chipPos[1];
		//ついでに初期化
		this.ivent = false;
		
		s = this.sprite_menu;
		scene.addChild(s);
		
		s = this.sprite_popKnow;
		scene.addChild(s);
		
		s = this.sprite_close;
		scene.addChild(s);
		
		s = this.sprite_map;
		scene.addChild(s);
	},
	
	
});