﻿const WEAPON_IMAGEID =
		[//ナイフ
    1,2,3,4,5,36,
    //斧
    6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,37,
    //剣
    21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,38];
    
const STONE_IMAGEID =
		[//鉱石、
    1,2,3,4,5,6,0,7];

const NEWS_IMAGEID =
		[//速報ID
	3,6,10,14];

const icon_frame = [2,3,4,5,6,7,
		    9,11,13,
		    15,17,19,
		    21,23,25,
		    27,29,31,
		    33,35,36,
		    37,39,41];


//現在のシーン定数
const SCENE_STATE_SHOP = 0;
const SCENE_STATE_MINE = 1;
const SCENE_STATE_HOUSE = 2;
const SCENE_STATE_SMITHY = 3;
const SCENE_STATE_STORE = 4;


//MainWindowのシークエンス
const MESSAGE_STATE_CALL = 0;
const MESSAGE_STATE_WAIT = 1;
const MESSAGE_STATE_POP = 2;
const MESSAGE_STATE_END = 3;
const MESSAGE_STATE_STOP = 4;
const MESSAGE_IV_STATE_START = 5;	//イベント用
const MESSAGE_IV_STATE_END = 6;//イベント用
const MESSAGE_WAIT_START = 0.00;	//何秒でメッセージが表示されるか
const MESSAGE_WAIT_END = 2.00;	//何秒でメッセージが非表示になるか

//各オブジェクトメニュー呼び出し命令
const OB_OPEN_SHINADASHI = 1;
const OB_OPEN_KOUZAN = 2;
const OB_OPEN_SONOTA = 3;
const OB_CLOSE_OBJECTMENU = 5;

//WindowVisibleSetMode
const WINDOW_SV_TRUE = 1;
const WINDOW_SV_FALSE = 2;


//各座標
const POSITION_MENU = [WIDTH/2,INTERFACE_OFFSET_HEIGHT + 193];			//鉱山メニュー、品出しメニューの基本的な位置(x軸は中央なので、そのメニュー毎の幅ごとずらす必要あり）

InterFace = Class.create(Object,{
	initialize:function(){
		Object.call(this);    
	    this.Load();
	    //各メニューオブジェクト
	    this.MWP = new MenuWeaponPop(this);	//品出しオブジェクト
	    this.MM  = new MenuMine(this);		//鉱山オブジェクト
	    this.MSP = new MenuShoplevelUp(this);//増築オブジェクト
	    this.M_SONOTA = new MenuSonota(this);//その他オブジェクト
	},	
	
	Load:function(){
	      //各種ステータスSpriteを生成する(0番目は画像、以降数字)
	      this.OFFSET_X = 0;
		  this.OFFSET_Y = 0;
	      var x = 4+this.OFFSET_X;
	      var y = 6+this.OFFSET_Y;
		        
		        
		  this.sceneIndex;	//UIが現在のsceneを識別するための物, 0:shop 1:鉱山 2:住居 3:鍛冶屋 4:倉庫
		  this.surface = new Surface(WIDTH,48);				//二箇所に分ける（分ける意味：他のシーンでスプライトにaddeventで動作するメソッドがあるため分ける)
		  this.stateSprite = new Sprite(WIDTH,48);	//ステータス描画用sprite
		  this.stateSprite.image = this.surface;			//一枚のSpriteで描画する
	      //メイン画面を挟んで描画
		  this.surfaceMenu = new Surface(WIDTH,96 );
		  this.menuSprite = new Sprite(WIDTH,96);		//ステータス描画用sprite
		  this.menuSprite.y = HEIGHT-96 -100;
		  this.menuSprite.image = this.surfaceMenu;
	      
		  //必要なデータSpriteを生成する
		  //お金
		  this.coin = [0,0,0,0,0,0,0];
		  this.coin[0] = createSprite('coin_s.png',32,32);
		  this.coin[0].x = x;
		  this.coin[0].y = y;
		  
		  //5桁出す
		  for(var i = 7; i > 0; i--){
		  	  x+=32;
			  this.coin[i] = createSprite(this.getPlace(Player.coins)[i-1]+'.png',32,32);
			  this.coin[i].x = x;
			  this.coin[i].y = y;
		  }
		  
		  //ハート
		  x+= 32+26;
		  this.hart = [0,0,0,0,0,0];
		  this.hart[0] = createSprite('hart.png',32,32);
		  this.hart[0].x = x;
		  this.hart[0].y = y;
		  
		  //分子ハート1.2
		  for(var i = 5; i > 3; i--){
			  x+= 32;
			  this.hart[i] = createSprite(this.getPlace(Player.life)[i+1]+'.png',32,32);
			  this.hart[i].x = x;
			  this.hart[i].y = y;
		  }
		  //スラッシュ3
		  x+=32;
		  this.hart[3] = createSprite('slash.png',24,32);
		  this.hart[3].x = x;
		  this.hart[3].y = y;
		  x +=-6;
		  //分母ハート4.5
		  for(var i = 2; i > 0; i--){
			  x+= 32;
			  this.hart[i] = createSprite(this.getPlace((Player.shopLevel+1)*3)[i-1]+'.png',32,32);
			  this.hart[i].x = x;
			  this.hart[i].y = y;
		  }
		  //ダイアモンド
		  x+=32+26;
		  this.diamond = [0,0,0,0];
		  this.diamond[0] = createSprite('diamond.png',32,32);
		  this.diamond[0].x = x;
		  this.diamond[0].y = y;
		  //3桁
		  for(var i = 3; i > 0; i--)
		  {
		  	  x+=32;
		  	  this.diamond[i] = createSprite(this.getPlace(Player.diamond)[i-1]+'.png',32,32);
		  	  this.diamond[i].x = x;
		  	  this.diamond[i].y = y;
		  }
		  
		  //Window
		  this.window_state;	//シークエンス制御
		  this.window_name;		//windowの名前(購入用や勇者用など)
		  this.window_data;		//主に武器のIDなどを記憶する箱、setImage_windowで使用
		  this.window = createDomSprite();	//dom化
		  this.window_sellButton = createDomSprite("kazi/cursor.png",24,12);
		  this.window_sellButton.visible = false;
		  this.window.visible = false;
		  this.windowZeroPosition = [0,0];	//そのシーン毎のメッセージ位置を記憶する,windowはこの変数に記憶した座標から中心で描画される
		  this.windowSequence = -1;		//会話を進めるための指数、通常状態の時は-1
		  this.windowMaxIndex = 3;		//最大会話数
		  this.windowLock = false;		//trueならwindowスプライトを触っても反応しない
		  //在庫用スプライト
		  this.window_num = [ [0,0],[0,0],[0,0] ];	//二桁
		  //座標
		  for(var i = 0; i < 3; i++)
		  {
		  	//this.window_num[i][0] = new Sprite(22,22);
		  	this.window_num[i][0] = createDomSprite("",22,22);
		  	this.window_num[i][0].visible = false;
			//this.window_num[i][1] = new Sprite(22,22);
			this.window_num[i][1] = createDomSprite("",22,22);
		  	this.window_num[i][1].visible = false;
		  }
		  
		  //各種ボタン
		  this.buttonData = [new Sprite(160,96),new Sprite(160,96),new Sprite(160,96),new Sprite(160,96)];
		  this.buttonSceneID = [1,3,0,2];	//imageIDと連動
		  
		  //各タイマー類
		  this.timer_window = 0;
	},
	
	ViewState:function(scene,sceneIndex,windowPos_wid,windowPos_hei)
	{
	
		//現在のSceneにステータス全てを乗せる
		//OFFSETはそのsceneの画像の座標+ステータス分をずらした値
		this.scene = scene;
		this.sceneIndex = sceneIndex;	//現在のアイテムの描画種別を記憶
		this.stateSprite.visible = true;
		this.menuSprite.visible = true;
		scene.addChild(this.stateSprite);	//描画用のSpriteをシーンに乗せる
		scene.addChild(this.menuSprite);
		game.InventoryManager.Atach(scene);	//Slot
  	    //Window,とりあえず位置を購入用にする
		this.windowZeroPosition　= [windowPos_wid,windowPos_hei];
	    this.window.image = "";
	    this.window_state = MESSAGE_STATE_STOP;
	    this.window.visible = false;	//最初は非表示
	    
	   	if(this.sceneIndex != SCENE_STATE_MINE)this.MSP.Atach();	//該当するシーンに乗せる
	    
	    this.scene.addChild(this.window);
	   	this.scene.addChild(this.window_sellButton);
	    
	    //一部の描画フラグを初期化
	    this.window_sellButton.visible = false;
	    for(var i = 0; i < this.window_num.length; i++)
	    {
	    	this.window_num[i][0].visible = false;
	    	this.window_num[i][1].visible = false;
	    	this.scene.addChild(this.window_num[i][0]);
	   		this.scene.addChild(this.window_num[i][1]);
	    }
		
		//スロットスプライトのフレームを元に戻す(家具の状態からframeが治っていない場合の対応)
		for(var i = 0; i < this.buttonData.length; i++)this.buttonData[i].visible = false;
		//シーンによって各種データを設定、ただし鉱山は抜く
		for(var i = 0; i < this.buttonData.length && this.sceneIndex != SCENE_STATE_MINE; i++)
		{
			var s = this.buttonData[i];
			//位置調整、中央寄せ、YはmenuSpriteに乗っているため描画では減算
			s.x = WIDTH/2;
			var wid = s.width-4;
			s.x += -(wid*this.buttonData.length)/2;
			s.x += i*wid;
			s.y = HEIGHT;
			s.y += -100;
			s.y += -s.height;
			s.visible = true;
			
			//各シーン判定
			s.image = this.sceneIndex == this.buttonSceneID[i]?getImage('InterFace/main_tab_'+(i+1)+'b.png'):getImage('InterFace/main_tab_'+(i+1)+'.png');
		}
		
		//各メニューAtach、現在はどれも店のシーンでしかアタッチしない
		if(this.sceneIndex == SCENE_STATE_SHOP)
		{
			this.MWP.Atach();	//品出し
			this.M_SONOTA.Atach();	//その他
		}
		this.MM.Atach();	//鉱山
		
		
		
	},
	
	ViewStateUpdate:function()
	{
		var draw = false;
		//Surfaceテスト、ピクセルクリア
		this.surface.clear();
		this.surfaceMenu.clear();
		
		//ステータスを更新する
		//coin
		for(i = 1; i < this.coin.length; i++){
			this.coin[i].image = getImage(this.getPlace(Player.coins)[i-1]+'.png');
		}
		//hart
		for(i = 1; i< 3; i++){
			this.hart[i].image = getImage(this.getPlace(Player.lifemax)[i-1]+'.png');//(Player.shopLevel+1)*3)[i-1]+'.png');
		}
		for(i = 4; i< 6; i++){
			this.hart[i].image = getImage(this.getPlace(Player.life)[i-4]+'.png');
		}
		//diamond
		for(i = 1; i < this.diamond.length; i++){
			this.diamond[i].image = getImage(this.getPlace(Player.diamond)[i-1]+'.png');
		}

		switch(this.sceneIndex){
			case  SCENE_STATE_SHOP:	//店舗
			 	 game.InventoryManager.setImage_slot(this.sceneIndex,0);
			 	 draw = true;
			 break;
			case  SCENE_STATE_HOUSE:	//住居
			 	 game.InventoryManager.setImage_slot(this.sceneIndex,2);
			 	 draw = true;
			 break;
			case  SCENE_STATE_STORE:	//倉庫
			 	 game.InventoryManager.setImage_slot(this.sceneIndex,0);
			 	 draw = true;
			 break;
		}
		
		this.DrawSurFaceState();	//ステータス描画
		game.InventoryManager.Draw_Slot();//Slot描画
		if(draw)game.InventoryManager.Draw_SlotImage();//Slot内部描画
	
	},
	
	OpenObjectWindow:function(MENU)
	{
		//各メニューオブジェクトを開く処理及び制御
		//再度押された時の挙動もここで処理
		
		switch(MENU)
		{
			//品出しメニュー呼び出し
			case OB_OPEN_SHINADASHI:
			{
				//呼び出しのみ、詳細な制御はUIへ
				
				this.MWP.IventOn();
				playSe(se.window);
				if(this.MM.ivent)this.MM.IventOff();
				if(this.M_SONOTA.ivent)this.M_SONOTA.IventOff();
				break;
			}
			
			//鉱山メニュー呼び出し
			case OB_OPEN_KOUZAN:
			{
				if(!this.MM.ivent)
				{
					this.MM.IventOn();
					playSe(se.window);
				}else if(this.MM.ivent)
				{
					//再度押されたら閉じる
					this.MM.IventOff();
					playSe(se.tap_2);
					
					this.WindowViewSet(WINDOW_SV_TRUE);	//イベントウィンドウが非表示なら表示へ
				}
				
				//開かれていたならば閉じる
				if(this.MWP.ivent)this.MWP.IventOff();
				if(this.M_SONOTA.ivent)this.M_SONOTA.IventOff();
				break;
			}
			
			//その他メニュー呼び出し
			case OB_OPEN_SONOTA:
			{
				//その他ボタンは他windowが被ってると物理的に押す事ができない
				if(!this.M_SONOTA.ivent && !this.MWP.ivent && !this.MM.ivent)
				{
					this.M_SONOTA.IventOn();
					playSe(se.tap_1);
				}
				
				break;
			}
			
			//品出し、鉱山メニュー、その他メニューを閉じる命令
			case OB_CLOSE_OBJECTMENU:
			{
				this.MM.IventOff();
				this.MWP.IventOff();
				this.M_SONOTA.IventOff();
				break;
			}
			
		}
		
		
		
	},
	
	EnterFrame:function()
	{
		//各シーンのevent.enterfame内で呼ぶ処理(大体interfaceが使用するタイマーなど)
		
		//windowのポップシークエンス(MainWindowの制御をここでしている）
		if(this.sceneIndex == SCENE_STATE_SHOP )
		{
			switch(this.window_state)
			{
				case MESSAGE_STATE_CALL:
				{
					this.timer_window = new Date();	//時間取得
					this.window.visible = false;	//既に表示されていたら消す
					this.window_state = MESSAGE_STATE_WAIT;
					break;
				}
				case MESSAGE_STATE_WAIT:
				{
					var time = (new Date() - this.timer_window) / 10;		//経過時間
					time = Math.floor(time);
					time *= 0.01;
					if(time > MESSAGE_WAIT_START )
					{
						//規定時間を超えたらPOPへ移行
						this.window_state = MESSAGE_STATE_POP;
						if(this.window_name == "購入")playSe(se.get);	//購入音を鳴らす
						if(this.window_name == "買えない")playSe(se.miss);
						this.timer_window = new Date();	//時間更新
					}
					break;
				}
				case MESSAGE_STATE_POP:
				{
					//windowImageを変更して即座に次へ
					//補足、少なくとも最短でここに来るために3frame必要なので、callした瞬間に呼び出したい場合ちょっと構造を変更する必要がある
					this.window_state = MESSAGE_STATE_END;
					this.window.visible = true;
					this.setImage_window();	//画像更新
					break;
				}
				case MESSAGE_STATE_END:
				{
					//時間が経過したら次へ
					var time = (new Date() - this.timer_window) / 100;		//経過時間
					time = Math.floor(time);
					time *= 0.1;
					if(time > MESSAGE_WAIT_END )
					{
						this.window_state = MESSAGE_STATE_STOP;
						this.window.visible = false;
					}
					break;
				}
				case MESSAGE_STATE_STOP:
				{
					//何もしない
					break;
				}
				
				case MESSAGE_IV_STATE_START:
				{
					this.timer_window = new Date();	//時間更新
					this.window_state = MESSAGE_IV_STATE_END;
					break;
				}
				case MESSAGE_IV_STATE_END:
				{
					var time = (new Date() - this.timer_window) / 1000;		//経過時間
					time = Math.floor(time);
					if(time > MESSAGE_WAIT_END)
					{
						this.windowSequence = this.windowMaxIndex;	//時間が経過したら会話終了
						this.window_state = MESSAGE_STATE_STOP;
						if(DEBUG)console.log("END");
					}
					break;
				}
			}
		}
		
		//life判定
		if(Player.life < Player.lifemax && !Player.lifeLock)
		{
			//時間を計測
			Player.lifetimer = Date.parse(new Date());	//現在時刻を整数変換
			Player.lifeLock = true;
				
		}else if(Player.life < Player.lifemax)
		{
			//計測開始されHPが減っているならば今の時間を演算して加算する
			var time = (new Date() - Player.lifetimer) / 1000;		//経過時間
			time = Math.floor(time);
			if(time >= LIFE_COOLTIME)
			{
				//もし規定の時間が過ぎたら回復開始
				var hart = time/LIFE_COOLTIME;	//回復するハートの数を計算
				hart = Math.floor(hart);
				Player.life += hart;
				if(Player.lifemax < Player.life)Player.life=Player.lifemax;
				Player.lifeLock = false;	//ロックを外す
				this.ViewStateUpdate();
			}
			
		}else if(Player.life >= Player.lifemax)
		{
			//ロックを外す
			Player.lifeLock = false;
		}
		
		if(this.sceneIndex == SCENE_STATE_SHOP)
		{
			this.MSP.EnterFrame();	//ショップシーンの増築ボタンのゴールドダイア切り替え計測
		}
	},
	
	setImage_window:function(window_name)
	{
		var src;
		var w = 0;
		var h = 0;
		if(window_name)this.window_name = window_name;	//引数付きで渡されたならば引数に応じて更新する
		
		switch(this.window_name)
		{
			case "買えない":
			{
				src = "w_buy_00.png";
				w = 512;
				h = 90;
				
				this.window.width = w;
				this.window.height = h;
			    this.window.x = this.windowZeroPosition[0]-w/2;
	   			this.window.y = this.windowZeroPosition[1];
				//this.window.image = getImage(src);
				setDomImage(this.window ,src);
				
				break;
			}
			case "購入":
			{
				src = "shop/w_buy_";
				w = 512;
				h = 90;
				
				this.window.width = w;
				this.window.height = h;
			    this.window.x = this.windowZeroPosition[0]-w/2;
	   			this.window.y = this.windowZeroPosition[1];
				
				var imageID = WEAPON_IMAGEID[this.window_data];	//イメージに応じた数字を獲得
				if(imageID < 10) setDomImage(this.window ,src+"0"+imageID+".png");
				if(imageID >= 10) setDomImage(this.window ,src+imageID+".png");
				
				break;
			}
			case "速報":
			{
				src = "shop/legend_";
				w = 512;
				h = 200;
				
				this.window.width = w;
				this.window.height = h;
			    this.window.x = this.windowZeroPosition[0]-w/2;
	   			this.window.y = this.windowZeroPosition[1];
				
				var imageID = NEWS_IMAGEID[Player.mineLevel];	//イメージに応じた数字を獲得
				if(imageID < 10)setDomImage(this.window ,src+"0"+imageID+"_4.png");
				if(imageID >= 10)setDomImage(this.window ,src+imageID+"_4.png");
				
				break;
			}
			case "勇者":
			{
				src = "shop/legend_";
				w = 512;
				h = 200;
				
				var iventCount = IventToIndex("勇者");	//現在の勇者進行度
				
				this.window.width = w;
				this.window.height = h;
				this.window.x = this.windowZeroPosition[0]-w/2;
	   			this.window.y = this.windowZeroPosition[1];
	   			this.window_sellButton.x = this.window.x + this.window.width - this.window_sellButton.width - 22;
	   			this.window_sellButton.y = this.window.y + this.window.height - 22;
	   			
				if(iventCount < 10)setDomImage(this.window ,src+"0"+iventCount+"_"+(this.windowSequence+1)+".png");
				if(iventCount >= 10)setDomImage(this.window ,src+iventCount+"_"+(this.windowSequence+1)+".png");
				
				//在庫Spriteの処理
				if(this.windowSequence == 1)
				{
					this.SetWindowNum();
				}else
				{
					//それ以外なら描画しない
					for(var i = 0; i < 3; i++)
					{
						for(var j = 0; j < 2; j++)
						{
							var s = this.window_num[i][j];
							s.visible = false;
						}
					}
				}
				
				break;
			}
			
			case "兵隊長":
			{
				src = "shop/kingdom_";
				w = 512;
				h = 200;
				
				var iventCount = IventToIndex("兵隊長");	//現在の兵隊長進行度
				
				this.window.width = w;
				this.window.height = h;
				this.window.x = this.windowZeroPosition[0]-w/2;
	   			this.window.y = this.windowZeroPosition[1];
	   			this.window_sellButton.x = this.window.x + this.window.width - this.window_sellButton.width - 22;
	   			this.window_sellButton.y = this.window.y + this.window.height - 22;
				
				if(iventCount < 10)setDomImage(this.window ,src+"0"+iventCount+"_"+(this.windowSequence+1)+".png");
				if(iventCount >= 10)setDomImage(this.window ,src+iventCount+"_"+(this.windowSequence+1)+".png");
				
				
				//在庫Spriteの処理
				if(this.windowSequence == 1)
				{
					this.SetWindowNum();
				}else
				{
					//それ以外なら描画しない
					for(var i = 0; i < 3; i++)
					{
						for(var j = 0; j < 2; j++)
						{
							var s = this.window_num[i][j];
							s.visible = false;
						}
					}
				}
				
				break;
			}
			
			case "最後":
			{
				src = "shop/ending_0";
				w = 512;
				h = 200;
				
				this.window.width = w;
				this.window.height = h;
			    this.window.x = this.windowZeroPosition[0]-w/2;
	   			this.window.y = this.windowZeroPosition[1];
				
				setDomImage(this.window ,src+(this.windowSequence+1)+".png");
				
				break;
			}
			
		}
	},
	
	WindowViewSet:function(MODE)
	{
		//特定状況下でのMainWindowのVisibleを操作、制御する
		
		if(this.windowSequence >= 0)
		{
			switch(MODE)
			{
				case WINDOW_SV_TRUE:
				{
					this.window.visible = true;
					this.SellbuttonSet();	//▽判定
					if(this.windowSequence == 1)this.SetWindowNum();	//在庫判定
					break;
				}
				
				case WINDOW_SV_FALSE:
				{
					this.window.visible = false;
					this.window_sellButton.visible = false;
					
					for(var index = 0; index < 3; index++)
					{
						var s = this.window_num[index][0];
						s.visible = false;
						s = this.window_num[index][1];
						s.visible = false;
						
					}
					
					break;
				}
			}
		}
		
	},
	
	CallWindow:function(windowStr,windowData)
	{
		//主にsetImage_windowで必要なデータをセットし、ポップシークエンスを始動するために呼ぶ
		//基本的にはNPC中の処理から呼び出される
		
		this.window_name = windowStr;	//名前を記憶する
		this.window_data = windowData;	//Dataを記憶する
		if(windowStr == "購入" || windowStr == "買えない")
		{
			this.window_state = MESSAGE_STATE_CALL;
		}else
		{
			//それ以外ならイベントなのでwindowを描画するようにして画像をセットする
			this.window.visible = true;
			if(this.windowSequence == -1)this.windowSequence = 0;	//初回なら0からスタート(画像を読み込む際必要)
			this.setImage_window();
			playSe(se.window);
			this.window_state = MESSAGE_STATE_STOP;
			this.OpenObjectWindow(OB_CLOSE_OBJECTMENU);	//全てのメニューを閉じる
			
			this.SellbuttonSet();	//▽描画判定
		}
		
	},
	
	SellbuttonSet:function()
	{
		//全ての武器の在庫が存在するならば▽を描画する
		
		if(this.sceneIndex == SCENE_STATE_SHOP && Player.story < 1)
		{
			//windowが売却シーケンスで尚且つエンディングではないなら尚且つshopシーンなら
		 	var buy = true;
			for(var i = 0; i < IVENT_NEEDWEAPON[0].length; i++)
			{
				//必要な武器及び数が所有されているか検索
			 	if(IVENT_NEEDWEAPON[Player.iventIndex][i][0] != -1)
				{
					//ちゃんと設定されている物のみ判定
					if(Player.weapon[IVENT_NEEDWEAPON[Player.iventIndex][i][0]] < IVENT_NEEDWEAPON[Player.iventIndex][i][1])
					{
						//もし数が満たしていない場合
						buy = false;	//条件が揃っていない
					}
				}
			 }
			 
			this.window_sellButton.visible = buy;	//全ての武器の在庫が存在するならtrue
		}
	
	},
	
	SetWindowNum:function()
	{
		//ウィンドウの在庫スプライトを操作するメソッド
		for(var index = 0; index < 3; index++)
		{
			var ID = IVENT_NEEDWEAPON[Player.iventIndex][index][0];
			var wns = this.window_num;
			if(ID != -1)
			{
				//IDが設定されていたらその行は在庫を表示
				var num = Player.weapon[ID];
				if(num < 10)
				{
					setDomImage(wns[index][1],"s"+num+".png");
					wns[index][1].visible = true;
					wns[index][0].image = false;
				}else if(num >= 10)
				{
					setDomImage(wns[index][0],"s"+Math.floor(num/10)+".png");
					setDomImage(wns[index][1],"s"+num%10+".png");
					wns[index][0].visible = true;
					wns[index][1].visible = true;
				}
				wns[index][0].x = this.window.x+465 - wns[index][0].width;
				wns[index][0].y = this.window.y+32 + index*56;
				wns[index][1].x = this.window.x+465;
				wns[index][1].y = wns[index][0].y;
			}
		}
	},
	
	
	TouchButton:function(ClickPos)
	{
		var windowCanSequence = !(this.windowSequence == 0 || this.windowSequence == 2);
		
		//もし全画面表示であるMAPが開かれているなら特殊な処理
		if(this.sceneIndex == SCENE_STATE_SHOP && this.M_SONOTA.state == SONOTA_STATE_MAP)
		{
			//現在がshopScene && MAP項目が押されてMAPが出ているなら
			
			this.M_SONOTA.sprite_map.tl.fadeOut(0);	//見えなくする
			this.M_SONOTA.state = SONOTA_STATE_NONE;	//状態を元に戻す
			
			return;	//1回帰る
		}
		
		//全てのスロットのタッチ判定
		var slotIndex = game.InventoryManager.getTouchSlotIndex(ClickPos);
		if(slotIndex != null)
		{
			//もしスロットがタッチされているなら
			if(windowCanSequence && this.sceneIndex == SCENE_STATE_SHOP)
			{
				//タッチ許可状態 && shop
				
				if(this.MWP.ivent)
				{
					//品出しが開かれているなら
					if(this.MWP.handSlot == Player.slot[0][slotIndex])
					{
						//同じスロットをクリックされたら
						//武器があるなら戻す
						if(Player.slot[0][slotIndex][1] > 0)
						{
							Player.slot[0][slotIndex][0] = -1;
							Player.slot[0][slotIndex][1] = -1;
							this.MWP.setImage();	//イメージ更新
						}
						playSe(se.tap_2);
					}else if(this.MWP.handSlot != Player.slot[0][i])
					{
						//違うスロットをクリックされたなら
						this.MWP.ChangeSlot(game.InventoryManager.s_back[slotIndex],slotIndex);
						playSe(se.tap_1);
					}
					
				}else if(!this.MWP.ivent)
				{
					//品出しが開かれていなければ
					this.OpenObjectWindow(OB_OPEN_SHINADASHI);	//品出しメニュー呼び出し
					this.MWP.Set_Handslot(game.InventoryManager.s_back[slotIndex],slotIndex);	//品出しメニュースロットセット
					
				}	
				
			}
		}
		

		
		//各種ボタンがクリックされた場合の処理
		//Menuボタン判定
		for(var i = 0; i < this.buttonData.length; i++)
		{
			var s = this.buttonData[i];
			if(s.visible && this.HitPos(s,ClickPos) && windowCanSequence)
			{
				//自身がクリックされて尚且つwindowがクリック可能シーケンスなら
				
				var go = false;	//下部ボタンの機能が発生したらtrue
				switch(i)
				{
					case 0:	//鉱山
					{
						this.OpenObjectWindow(OB_OPEN_KOUZAN);	//鉱山メニューを開く
						break;
					}
					case 1:	//鍛冶屋
					{
						if(this.sceneIndex != SCENE_STATE_SMITHY && !this.MWP.ivent)
						{
						    game.pushScene(new FadeScene(kaziAssets,makeNewScene(SmithyScene)));
						    go = true;
					    }
						break;
					}
					case 2:	//店舗
					{
						if(this.sceneIndex != SCENE_STATE_SHOP && !this.MWP.ivent)
						{
						　　game.pushScene(new FadeScene(shopAssets,makeNewScene(ShopScene)));
						    go = true;
						}
						break;
					}
					case 3:	//住居
					{
						if(this.sceneIndex != SCENE_STATE_HOUSE && !this.MWP.ivent)
						{
						    game.pushScene(new FadeScene(houseAssets,makeNewScene(HouseScene)));
						    go = true;
					    }
						break;
					}
					
				}
				
				if(go)
				{
					//シーンが移動したら
					if(this.sceneIndex == SCENE_STATE_SMITHY)
					{
						//もし現在鍛冶屋の画面なら、sceneグローバル変数の初期化開始
						smithyScene.save();
						smithyScene = null;
					}
					
					playSe("sound/move.wav");
					this.OpenObjectWindow(OB_CLOSE_OBJECTMENU);	//全てのメニューを閉じる
				}
				
			}
			
		}
		
		
		
		//品出しの処理
		this.MWP.Touch(ClickPos);
		//鉱山メニュー
		this.MM.Touch(ClickPos);
		//増築
		this.MSP.Touch(ClickPos);
		//その他
		this.M_SONOTA.Touch(ClickPos);
		
		
	},
	
	HitPos:function(sprite,pos)
	{
		//引数Posがspriteに接触していたらtrueを返す(scaleが設定されていたらそれを判定にする)
		if(!(sprite.scaleX == 1 && sprite.scaleY == 1))
		{
			//alert(""+sprite.x+" "+sprite.width*sprite.scaleX);
		}
		return sprite.scaleX == 1 && sprite.scaleY == 1 ?
		(pos[0] > sprite.x && pos[1] > sprite.y && pos[0] < sprite.x+sprite.width && pos[1] < sprite.y+sprite.height) :
		(pos[0] > sprite.x && pos[1] > sprite.y && pos[0] < sprite.x+sprite.scaleX*sprite.width && pos[1] < sprite.y+sprite.scaleY*sprite.height) ;
	},
	
	TouchWindow:function(ClickPos)
	{
		//Lockが掛かっていたら何もしない
		if(this.windowLock)return;
		if( !(this.window_name == "勇者" || this.window_name == "兵隊長" || this.window_name == "最後") )return;	//条件が満たされた直後のwindowへの連打を防ぐ
		//windowがタッチされたら会話を進めるためのメソッド
		
		if( this.window.visible )
		{
			//もし画面がクリックされてwindowが表示されているならば
			if(DEBUG)console.log(this.windowSequence + " st:"+Player.story);
			if(this.windowSequence == 1 && Player.story < 1)
			{
				//エンディングではない、尚且つwindowがSellシーケンスなら
				if( !this.getInterfaceIvent() )
				{
					//操作可能な状態なら処理開始
					
					//メッセージのsellボタン
					if(this.window_sellButton.visible && this.HitPos(this.window,ClickPos) )//売るボタンが無くなり下矢印になったことによる条件変更
					{
						//sellボタンが見えていて尚且つ押されたら
						this.window_sellButton.visible = false;
						//売却開始
						var iNeedWeapon = IVENT_NEEDWEAPON[Player.iventIndex];
						for(var i = 0; i < iNeedWeapon.length; i++)
						{
							var iId = iNeedWeapon[i][0];
							var iNum = iNeedWeapon[i][1];
							var ans = 0;	//武器数の持越し計算に使用
							var handNum = iNum;
							
							if(iId != -1)
							{
								//まずスロットから減らす
								for(var k = 0; k < Player.slot[0].length; k++)
								{
									//スロットの中から同一IDを検索
									var ID = Player.slot[0][k][0];
									var pNum = Player.slot[0][k][1];
									if(ID == iId)
									{
										//同じ武器なら減らしていく
										ans = pNum - handNum;
										if(ans == 0)
										{
											//丁度の場合
											Player.slot[0][k][0] = -1;
											Player.slot[0][k][1] = -1;
											break;	//for抜ける
										}else if(ans > 0)
										{
											//必要な武器がスロット1枠で足りる場合
											Player.slot[0][k][1] += -handNum;
											break;
										}else
										{
											//0未満になった場合、次へ持越し
											Player.slot[0][k][0] = -1;
											Player.slot[0][k][1] = -1;
											handNum += -pNum;
										}
									}
								}
								//正規データなら処理開始
								Player.weapon[iId] += -iNum;	//武器を減算
								Player.coins += WEAPONPLACE[iId] * iNum;	//お金加算
								if(plugin){
								try{
								window.plugins.gamecenter.reportScore(function(e){;},function(e){;},CATEGORY , Player.coins);
								}catch(e){
								alert(e.stack);
								}
								}
							}
						}
						this.windowSequence++;
						this.setImage_window();
						playSe(se.made);
						//タイマー作動、2秒で会話数を終了状態の数へ持っていく
						//this.window_state = MESSAGE_IV_STATE_START;	//仕様変更で自動で閉じないように
					}
				}
				
			}else
			{
				//会話を進める
				this.windowSequence++;
				playSe(se.tap_1);
				if(Player.story == 1 && this.windowSequence == 1)
				{
					//エンディングで冒頭のメッセージをクリックしたならロックを掛け非描画
					//this.windowLock = true;
					//this.window.visible = false;
				}
			}
			
			if( !(this.windowSequence >= this.windowMaxIndex) )
			{
				//シークエンスを越えなければ画像を更新
				this.setImage_window();
			}
		}
		if(DEBUG)console.log("touchEndSeq:"+this.windowSequence);
	},
	
	getPlace:function(n)
	{
		//受け取った引数を桁変換して配列を返す
		//とりあえず最高8桁
		var index = 8;
		var place = [];
		for(var i = 0; i < index; i++){
			var k = 1;
			for(j = 0; j < i; j++){k *= 10;}	//計算桁をずらしていく
			place.push(Math.floor((n / k) % 10));
		}
		return place;
	},
	
	getInterfaceIvent:function()
	{
		//各イベントが開かれていないかチェックする(特定シーケンスでは押せないようにするため）
		//各イベントが開かれていて、操作不能シーケンスならtrueが帰る
		var window = (this.windowSequence == 0 || this.windowSequence == 2);
		var mine = this.MM.ivent;
		var weapon = this.MWP.ivent;
		var shopLevel = this.MSP.ivent;
		var sonota = this.M_SONOTA.ivent;
		
		return (window || mine || weapon || shopLevel || sonota); 
		
	},
	
	
	DrawSurFaceState:function()
	{
		//設定したspirteをSurFaceに載せる
		var s;
		//coin
		for(i = 0; i < this.coin.length; i++){
			
			s = this.coin[i];
			this.surface.draw(s.image,s.x,s.y);
			
		}
		//hart
		for(i = 0; i< this.hart.length; i++){
			
			s = this.hart[i];
			this.surface.draw(s.image,s.x,s.y);
			
		}
		//diamond
		for(i = 0; i < this.diamond.length; i++){
			
			s = this.diamond[i];
			this.surface.draw(s.image,s.x,s.y);
			
		}
		
		//各種ボタン
		for(var i = 0; i < this.buttonData.length; i++)
		{
			s = this.buttonData[i];
			if(s.visible)this.surfaceMenu.draw(s.image,s.x,0);
		}
	},
	
	
	
	
	
});
