Sprite2 = Class.create(Sprite,{
	initialize:function(w,h){
		Sprite.call(this, w, h);
		this.originPosX = - w / 2;
		this.originPosY = - h / 2;
	},

	// x,y を中心に座標設定
	setPos:function(x,y){
		this.x = x + this.originPosX;
		this.y = y + this.originPosY;
	},

	isHit:function(x,y){
		return		(this.x <= x &&
					x < this.x + this.width &&
					this.y <= y &&
					y < this.y + this.height);
	},
});
