Flicker = Class.create(Object,{
	initialize:function(target){
		Object.call(this);

		target.FL_touchStart = false;
		target.FL_startX = 0;
		target.FL_startY = 0;
		target.FL_oldX = 0;
		target.FL_oldY = 0;
		target.FL_x = 0;
		target.FL_y = 0;
		target.FL_onStart = function(){};
		target.FL_onMove = function(){};
		target.FL_onEnd = function(){};

		target.addEventListener(Event.TOUCH_START, function(e){
			target.FL_touchStart = true;
			target.FL_startX = e.localX;
			target.FL_startY = e.localY;
			target.FL_x = e.localX;
			target.FL_y = e.localY;
			target.FL_onStart();
		});

		target.addEventListener(Event.TOUCH_MOVE, function(e){
			if(target.FL_touchStart){
				target.FL_oldX = target.FL_x;
				target.FL_oldY = target.FL_y;
				target.FL_x = e.localX;
				target.FL_y = e.localY;
				target.FL_onMove();
			}
		});

		target.addEventListener(Event.TOUCH_END, function(e){
			if(target.FL_touchStart){
				target.FL_touchStart = false;
				target.FL_x = e.localX;
				target.FL_y = e.localY;
				target.FL_onEnd();
			}
		});
	},
});
