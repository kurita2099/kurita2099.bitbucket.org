﻿//ハウツーシーン追加表示
function createHowTo(){
  // ハウツーシーン
        var second = new Scene();
		var group = createDomSprite(null ,512,512);	//グループ初期化	//domSpriteはグループが効かない
		var sprites = [];
//        second.backgroundColor = "#FF9999";
       var spritename = ["other/howto_1.png","other/howto_2.png","other/howto_3.png","other/howto_4.png"];

		for(var i=0;i<spritename.length;i++){
			sprites[i] = document.createElement('div');
			sprites[i].style.backgroundImage = 'url(' + DIR_IMAGE + spritename[i] + ')'; 
			sprites[i].style.width = "512px";
			sprites[i].style.height = "512px";
			sprites[i].style.display = "none";
        	group._element.appendChild(sprites[i]);
		}
		var page = 0;
		sprites[page].style.display = "block";
		group.x = (640-512)/2;
		group.y = (960-512)/2;
		second.addChild(group);
		second.addEventListener(Event.TOUCH_START, function(e)
		{
//バツボタン
		if(e.localX > (group.x + 512 -32) && e.localY < group.y ){ 
			game.popScene();
		}
		if(e.localX > group.x && e.localX < group.x + 512  && e.localY > group.y){ 
		 sprites[page].style.display = "none";
		 	if(++page>=spritename.length){
			game.popScene();
		 	}else{
		 	sprites[page].style.display = "block";
		 	}
		}
		});
return second;
}
function createDiamond(){
  // 購入
		var select_no = 1;
        var second = new Scene();
		//var bg = createDomSprite(null,512,300);
		//bg._element.style.backgroundColor = "#000000";
		var bg = new Sprite(512, 400);
		var surf = new Surface(512, 400);
		surf.context.beginPath();
		surf.context.fillStyle = 'rgba(0, 0, 0, 0.6)';
		surf.context.fillRect(0, 0, 512, 400);
		bg.image = surf

		//second.addChild(bg);
		bg.x = (640-512)/2;
		bg.y = (960-400)/2;

		var group = createDomSprite(null ,512,314);	//グループ初期化	//domSpriteはグループが効かない
		var sprites = [];
		var spritename = ["other/w_dia.png"];
		var spritepos = [[0,0]];
		for(var i=0;i<spritename.length;i++){
			sprites[i] = document.createElement('div');
			sprites[i].style.backgroundImage = 'url(' + DIR_IMAGE + spritename[i] + ')'; 
			var w = 64;
			var h = 64;
			if(i==0){
				w = 512;
				h = 314;
			}
			sprites[i].style.position = "absolute";
			sprites[i].style.width = w + "px";
			sprites[i].style.height = h + "px";
			sprites[i].style.display = "block";
			sprites[i].style.left = spritepos[i][0] + 'px';
			sprites[i].style.top = spritepos[i][1] + 'px';
        	group._element.appendChild(sprites[i]);
		}
		group.x = (640-512)/2;
		group.y = (960-314)/2;
		
		second.addChild(group);

		var sprite_cursor = createDomSprite("cursor_s.png",8,14);
		sprite_cursor.x = group.x + 40 -4;
		sprite_cursor.y = group.y + 97 ;
		second.addChild(sprite_cursor);

		var sprite_buy = new Sprite(192,90);
		sprite_buy.image = getImage("w_buy.png");
		sprite_buy.x = (640-192)/2;
        sprite_buy.y = (960-314)/2 + 330;
		second.addChild(sprite_buy);
		sprite_buy.addEventListener(Event.TOUCH_START, function(e)
		{

			if(plugin && !isbuying){

				try{
/*
				window.plugins.inAppPurchaseManager.requestProductData("com.qooga.Bukiya", function(result) {
            		alert("productId: " + result.id + " title: " + result.title + " description: " + result.description + " price: " + result.price);
            		window.plugins.inAppPurchaseManager.makePurchase(result.id, 1);
					isbuying = true;
        		}, function(id) {
            	alert("Invalid product id: " + result);
        		}
    			);
*/
				window.plugins.inAppPurchaseManager.makePurchase(PURCHASE + select_no, 1);
				}catch(e){
            		alert(e.stack);
				}
			}

		});
		second.addEventListener(Event.TOUCH_START, function(e)
		{
//バツボタン
/*
		if(e.localX > (group.x + 512 -32) && e.localY < group.y ){ 
			game.popScene();
		}
*/
		if(e.localX > group.x && e.localX < group.x + group.width 
			&& e.localY > group.y && e.localY < group.y + group.height ){ 
			var offsetY = e.localY - group.y;
			var offsetX = e.localX - group.x;
			//alert("select" + Math.ceil(offset/(200/3)));
			var posx = Math.floor(offsetX/(512/2));
			var posy = Math.floor(offsetY/(314/2));

			select_no = posx + posy*2;
			//alert(select_no);

			sprite_cursor.x = group.x + 40 - 4 + posx * (512/2);
			sprite_cursor.y = group.y + 97 + posy * (314/2 -10);
			//game.popScene();
		}else
		{
			game.popScene();
			//会話戻し
			game.UI.WindowViewSet(WINDOW_SV_TRUE);
		}
		
		});
return second;
}