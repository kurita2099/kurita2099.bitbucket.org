
WeaponObject = Class.create(Object,{
	initialize:function()
	{
		//床に置かれるweaponを定義
		this.sprite=null;				//武器s
		this.sprite_num=[null,null];	//数字s
		this.ID=-1;						//武器ID
		this.num=-1;					//数
	},

});




StoreScene = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);
		
		//ずらし
		this.OFFSET_X = 0;
		this.OFFSET_Y = INTERFACE_OFFSET_HEIGHT;
		//階段(x1y1x2y2)
		this.kup = [0,0,0,0];
		this.blockStart = [162,192];	//画像でのブロック部分がどこから始まるか
		
		pos = ChipToPosition(this,3 + (Player.shopLevel*2),1)
		this.kup[0] = pos[0];
		this.kup[1] = pos[1];
		this.kup[2] = pos[0]+CHIPSIZE;
		this.kup[3] = pos[1]+CHIPSIZE;
		
		//surface
		this.surface = new Surface(WIDTH,HEIGHT);
		this.mainSprite = new Sprite(WIDTH,HEIGHT);	
		this.mainSprite.image = this.surface;
		
		var store = createSprite('warehouse_'+(Player.shopLevel+1)+'.png',640, 704);
		store.visible = true;
		store.x += this.OFFSET_X;
		store.y += this.OFFSET_Y;
		this.addChild(store);
		
		this.weaponObjects = [];
		this.imageUpdate = false;	//weaponObjectに更新を掛けるかどうか
		
		//倉庫に置く武器objectを作る
		var cn = (Player.shopLevel*2)+3;	//床のチップ数
		for(var i = 0; i < cn; i++)
		{
			for(var j = 0; j < cn; j++)
			{
				if( !(i == 0 && j == cn-1 ))
				{
					//階段部分以外に武器オブジェクトを生成
					var pos = ChipToPosition(this,j+1,i+1);
					this.weaponObjects.push(new WeaponObject());
					var wo = this.weaponObjects[this.weaponObjects.length-1];
					wo.sprite = new Sprite(64,64);
					wo.sprite.x = pos[0];
					wo.sprite.y = pos[1];
					
					wo.sprite_num = [new Sprite(16,18),new Sprite(16,18)];
					wo.sprite_num[0].x = wo.sprite.x + wo.sprite.width - wo.sprite_num[0].width;
					wo.sprite_num[0].y = wo.sprite.y + wo.sprite.height - wo.sprite_num[0].height;
					wo.sprite_num[1].x = wo.sprite_num[0].x - wo.sprite_num[0].width;
					wo.sprite_num[1].y = wo.sprite_num[0].y;
				}
			}
		}
		
		//UIから在庫を計算してobjectに反映させる
		var bank = game.InventoryManager.getWeaponBank();
		var ID = 0;
		for(var i = 0; i < this.weaponObjects.length; i++)
		{
			var wo = this.weaponObjects[i];
			if(ID < bank.length)
			{
				while(ID < bank.length-1 && bank[ID] == 0)
				{
					//倉庫の中があるIDまでジャンプ
					ID++;
				}
				
				if(bank[ID] > 0)
				{
					//もし在庫が1以上存在するなら
					wo.ID = ID;
					wo.num = bank[ID];
					ID++;
				}
			}
		}
		
		this.setImage();
		this.Draw();
		
		this.addChild(this.mainSprite);	//surface描画
		
		game.UI.ViewState(this,SCENE_STATE_STORE,0,0);
	    game.UI.ViewStateUpdate();
		
		this.addEventListener(Event.TOUCH_START, function(e)
		{
			if( (e.localX>this.kup[0] && e.localX<this.kup[2]) && (e.localY>this.kup[1] && e.localY < this.kup[3]) ){
				if(!game.UI.getInterfaceIvent())
				{
				//鉱山メニューが開かれていなければ移動できる
                 this.removeChild(game.UI.stateSprite);
				 game.pushScene(new FadeScene(kaziAssets,makeNewScene(SmithyScene)));	//上り階段
				 playSe("sound/move.wav");
				 }
			}
			
			this.imageUpdate = false;
			
			this.TouchWeapon([e.localX,e.localY]);	//武器オブジェクトの処理
			this.TouchStoreSlot([e.localX,e.localY]);//スロットのタッチ処理

			
			if(this.imageUpdate)
			{
				this.setImage();
				this.Draw();
				game.UI.ViewStateUpdate();
			}
			
			game.UI.TouchButton([e.localX,e.localY]);
		
		});
		
	},
	
	setImage:function()
	{
		//データに応じて画像をset
		for(var i = 0; i < this.weaponObjects.length; i++)
		{
			var wo = this.weaponObjects[i];
			if(wo.ID != -1)
			{
				var imageID = WEAPON_IMAGEID[wo.ID];
				if(imageID < 10)wo.sprite.image = getImage('weapon_0'+imageID+'.png');
				if(imageID >= 10)wo.sprite.image = getImage("weapon_"+imageID+".png");
				
				var Place = game.UI.getPlace;	//メソッドを持ってくる
				wo.sprite_num[0].image = getImage("InterFace/"+Place(wo.num)[0]+".png");
				wo.sprite_num[1].image = getImage("InterFace/"+Place(wo.num)[1]+".png");
				wo.sprite.visible = true;
				wo.sprite_num[0].visible = true;
				wo.sprite_num[1].visible = true;
			}else
			{
				wo.sprite.visible = false;
				wo.sprite_num[0].visible = false;
				wo.sprite_num[1].visible = false
			}
		}
		
	},
	
	Draw:function()
	{
		this.surface.clear();	//クリア
		var s;
		for(var i = 0; i < this.weaponObjects.length; i++)
		{
			var wo = this.weaponObjects[i];
			
			if(wo.sprite.visible && wo.sprite.image != null)
			{
				s = wo.sprite;
				this.surface.draw(s.image,s.x,s.y);
			}
			
			if(wo.sprite_num[0].visible)
			{
				s = wo.sprite_num;
				this.surface.draw(s[0].image,s[0].x,s[0].y);
				this.surface.draw(s[1].image,s[1].x,s[1].y);
			}
			
		}
	},
	
	TouchWeapon:function(touchPosition)
	{
		//タッチされた武器オブジェクトの処理
		var getSlot = Player.slot[0][game.InventoryManager.getTouchSlotIndex(touchPosition)];
		if(getSlot != null)
		{
			//もしタッチされた場所がスロットなら
			if(getSlot[0] != -1)
			{
				//既に武器データがあるなら、それを空いている倉庫へ置く。空いていなかったら置けない
				for(var i = 0; i < this.weaponObjects.length && !this.imageUpdate; i++)
				{
					var wo = this.weaponObjects[i];
					if(wo.ID == -1)
					{
						//空きデータなら、移動開始
						wo.ID = getSlot[0];
						wo.num = getSlot[1];
						getSlot[0] = -1;
						getSlot[1] = -1;
						playSe(se.tap_1);
						this.imageUpdate = true;
					}else if(wo.ID == getSlot[0])
					{
						//IDが一致していたらデータを加算
						wo.num += getSlot[1];
						getSlot[0] = -1;
						getSlot[1] = -1;
						playSe(se.tap_1);
						this.imageUpdate = true;
					}
				}
			}else
			{
				//空データ
			}
		}
	},
	
	TouchStoreSlot:function(touchPosition)
	{
		for(var i = 0; i < this.weaponObjects.length && !this.imageUpdate; i++)
		{
			var wo = this.weaponObjects[i];
			if(wo.sprite.visible)
			{
				//有効なスプライトのみ判定
				var HIT = game.UI.HitPos;
				if(HIT(wo.sprite,touchPosition) )
				{
					game.InventoryManager.setWeaponForSlot(IVT_SETWEAPON_AUTO,wo.ID);	//指定した武器ID挿入
					var bank = game.InventoryManager.getWeaponBank();
					wo.num = bank[wo.ID];	//woの在庫数更新
					if(bank[wo.ID] == 0)
					{
						//挿入した後、0個になったら初期化
						wo.ID = -1;
						wo.num = -1;
					}
					playSe(se.tap_1);
					this.imageUpdate = true;
				}
			}
		}
	},
	
});
