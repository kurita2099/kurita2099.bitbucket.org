Group2 = Class.create(Group,{
	initialize:function(){
		Group.call(this);
		this._visible = true;
		this.width = 0;
		this.height = 0;
	},
	visible : {
		get : function(){
			return this._visible;
		},
		set : function(visible){
			this._visible = visible;

			for(var i = 0; i < this.childNodes.length; i++){
				this.childNodes[i].visible = visible;
			}
		}
	},
	isHit:function(x,y){
		return		(this.x <= x &&
					x < this.x + this.width &&
					this.y <= y &&
					y < this.y + this.height);
	},
});
