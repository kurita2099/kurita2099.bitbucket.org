Number = Class.create(Group2,{
	initialize:function(){
		Group2.call(this);
		
		this._figure = 0;
		this._number = 0;
		this.sprites = new Array(3);
		
		for(var i = 0; i < this.sprites.length; i++){
			this.sprites[i] = new Sprite2(16,18);
			this.sprites[i].image = getImage('InterFace/0.png');
			this.sprites[i].x = (-16 * i) + ((this.sprites.length - 1) * 16);
			this.addChild(this.sprites[i]);
		}
	},
	
	figure : {
		get : function(){
			return this._figure;
		},
		set : function(figure){
			this._figure = figure;
			
			for(var i = 0; i < this.sprites.length; i++){
				
				if(i < figure){
					this.sprites[i].visible = true;
				}
				else{
					this.sprites[i].visible = false;
				}
			}
			
			// 桁数が変わったので再配置
			for(var i = 0; i < this._figure; i++){
				this.sprites[i].x = (-16 * i) + ((this._figure - 1) * 16);
			}
		},
	},
	
	number : {
		get : function(){
			return this._number;
		},
		set : function(number){
			this._number = number;
			
			for(var i = 0; i < this.sprites.length && i < this.figure; i++){
				this.sprites[i].image = getImage('InterFace/' + (number % 10) + '.png');
				number = (number / 10) | 0;
			}
		},
	},
	
	visible : {
		get : function(){
			return this._visible;
		},
		set : function(visible){
			this._visible = visible;

			for(var i = 0; i < this.childNodes.length; i++){
				this.childNodes[i].visible = visible;
			}
			
			if(visible){
				for(var i = 0; i < this.sprites.length; i++){
					
					if(i < this.figure){
						this.sprites[i].visible = true;
					}
					else{
						this.sprites[i].visible = false;
					}
				}
			}
		}
	},
});
