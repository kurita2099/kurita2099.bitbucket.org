﻿//ローディングシーン上書き　今は何も表示しない
var Loading  = enchant.Class.create(enchant.Scene, {
  initialize: function() {
    enchant.Scene.call(this);
　　this.backgroundColor = '#000';
    this.addEventListener('progress', function(e) {
      progress = e.loaded / e.total * 1.0;
    });
    this.addEventListener('load', function(e) {
      var core = enchant.Core.instance;
      core.removeScene(core.loadingScene);
      core.dispatchEvent(e);
    });
  }
});
