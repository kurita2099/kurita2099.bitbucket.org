
enchant();

const	WIDTH = 640;
const	HEIGHT = 1136;
const	FPS = 15;
const	BENGOSI_QUIZ_NUM = 20;
const	SWIPE_SPEED = 128;
const	SOUND = true;

// android でタッチ処理が連続で呼ばれる不具合の対応。
// enchant.js ・ android のどちらの問題なのか不明。
// 画面の切り替わり後の操作無効フレーム数を指定する。
const	CHANGE_STATE_WAIT = 10;

const	DIR_IMAGE = 'img/';
const	DIR_SOUND = 'sound/';

var assets = [
'0.png',
'1.png',
'2.png',
'3.png',
'4.png',
'5.png',
'6.png',
'7.png',
'8.png',
'9.png',
'block.png',
'coin_s.png',
'diamond.png',
'hart.png',
'house_1.png',
'house_2.png',
'house_3.png',
'house_4.png',
'mine.png',
'number.png',
'pickel.png',
'pickel_g.png',
'pickel_gauge.png',
'shop_1.png',
'shop_2.png',
'shop_3.png',
'shop_4.png',
'slash.png',
'slot_1.png',
'stone.png',
'warehouse_1.png',
'warehouse_2.png',
'warehouse_3.png',
'warehouse_4.png',
'work_1.png',
'work_2.png',
'work_3.png',
'work_4.png'
];

var sounds = [
"get.wav",
"level_1.mp3",
"shop.mp3"
];

var game;
var scene;

var se = new Object();

var bgmLoop = null;
var dummyLabel;

Sprite2 = Class.create(Sprite,{
	initialize:function(w,h){
		Sprite.call(this, w, h);
		this.originPosX = - w / 2;
		this.originPosY = - h / 2;
	},

	setPos:function(x,y){
		this.x = x + this.originPosX;
		this.y = y + this.originPosY;
	},
});

Stone = Class.create(Sprite2,{
	initialize:function(){
		Sprite2.call(this, 64, 64);
		this.image = getImage('stone.png');

		this.addEventListener(Event.TOUCH_START, function(){
			playSe(se.get);
			this.visible = false;
		});
	},
	stoneType : {
		get : function() {
			return this._stoneType;
		},
		set : function(type) {
			this._stoneType = type;
			this.frame = type;
		}
	},
});

Block = Class.create(Sprite2,{
	initialize:function(){
		Sprite2.call(this, 80, 80);
		this.image = getImage('block.png');
		this.clickCount = 0;
		this._blockType = 0;

		this.addEventListener(Event.TOUCH_START, function(){
			this.clickCount++;
			if(Block.BLOCK_FRAME <= this.clickCount){
				this.visible = false;
			}
			else{
				this.frame = this._blockType * Block.BLOCK_FRAME + this.clickCount;
			}
		});
	},
	blockType : {
		get : function() {
			return this._blockType;
		},
		set : function(type) {
			this._blockType = type;
			this.frame = type * Block.BLOCK_FRAME;
		}
	},
});
Block.BLOCK_FRAME = 3;

MineScene = Class.create(Scene,{
	initialize:function(){
		Scene.call(this);

		mine = createSprite('mine.png',640, 704);
		mine.visible = true;
		this.addChild(mine);

		var stage = 4;
		var diamondNum = [0,1,2,3,4];
		var stoneTable = [
				[2,2,2,2,2,2,2,3],
				[2,2,3,3,3,3,3,4],
				[2,3,3,4,4,4,4,5],
				[2,3,4,4,5,5,5,6],
				[3,4,5,5,6,6,6,7],
				];
		var blockTable = [
				[0,0],
				[4,0],
				[4,2],
				[8,2],
				[8,4],
				];
		var MINE_SPOT_NUM = 64;

		// 鉱石の配置
		var mineSpot = new Array(MINE_SPOT_NUM);
		var blocks = new Array(MINE_SPOT_NUM);
		for(i = 0; i < mineSpot.length; i++){
			mineSpot[i] = -1;
			blocks[i] = 0;
		}
		for(i = 0; i < stoneTable[stage].length; i++){
			var pos = rand(MINE_SPOT_NUM);
			if(mineSpot[pos] == -1){
				stone = new Stone();
				var x = pos % 8;
				var y = (pos / 8) | 0;
				stone.stoneType = stoneTable[stage][i];
				mineSpot[pos] = stone.stoneType;
				stone.setPos(80 * x + 40, 80 * y + 64 + 40);
				this.addChild(stone);
			}
			else{
				// すでに配置している場所だった場合は配置しなおし。
				i--;
			}
		}

		// 石炭の配置
		for(i = 0; i < MINE_SPOT_NUM; i++){
			if(2 <= mineSpot[i]){
				var x = i % 8;
				var y = (i / 8) | 0;
				if(x != 0){
					if(mineSpot[i-1] == -1){
						mineSpot[i-1] = 0;
					}
				}
				if(x != 7){
					if(mineSpot[i+1] == -1){
						mineSpot[i+1] = 0;
					}
				}
				if(y != 0){
					if(mineSpot[i-8] == -1){
						mineSpot[i-8] = 0;
					}
				}
				if(y != 7){
					if(mineSpot[i+8] == -1){
						mineSpot[i+8] = 0;
					}
				}
			}
		}
		// 一部の石炭をダイアモンドに変更する
		for(i = 0; i < diamondNum[stage]; i++){
			var diamond = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			for(j = 0; j < MINE_SPOT_NUM; j++){
				if(mineSpot[(j + diamond) % MINE_SPOT_NUM] == 0){
					mineSpot[(j + diamond) % MINE_SPOT_NUM] = 1;
					break;
				}
			}
		}
		// 石炭、ダイアモンドの生成
		for(i = 0; i < MINE_SPOT_NUM; i++){
			if(0 <= mineSpot[i] && mineSpot[i] <= 1){
				stone = new Stone();
				var x = i % 8;
				var y = (i / 8) | 0;
				if(mineSpot[i] == 0){	stone.stoneType = 0;}
				else{					stone.stoneType = 1;}
				stone.setPos(80 * x + 40, 80 * y + 64 + 40);
				this.addChild(stone);
			}
		}

		// 岩B の配置
		num = (blockTable[stage][0] / 4) | 0;
		for(i = 0; i < num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			for(j = 0; j < MINE_SPOT_NUM; j++){
				if(2 <= mineSpot[(j + mine) % MINE_SPOT_NUM] &&
					blocks[(j + mine) % MINE_SPOT_NUM] == 0){
					blocks[(j + mine) % MINE_SPOT_NUM] = 1;
					break;
				}
			}
		}
		for(i = 0; i < blockTable[stage][0] - num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			if(mineSpot[mine] < 2 &&
				blocks[mine] == 0){
				blocks[mine] = 1;
			}
			else{
				i--;
			}
		}

		// 岩C の配置
		num = (blockTable[stage][1] / 2) | 0;
		for(i = 0; i < num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			for(j = 0; j < MINE_SPOT_NUM; j++){
				if(2 <= mineSpot[(j + mine) % MINE_SPOT_NUM] &&
					blocks[(j + mine) % MINE_SPOT_NUM] == 0){
					blocks[(j + mine) % MINE_SPOT_NUM] = 2;
					break;
				}
			}
		}
		for(i = 0; i < blockTable[stage][1] - num; i++){
			var mine = rand(MINE_SPOT_NUM);	// 初期位置をランダムで決定する
			if(mineSpot[mine] < 2 &&
				blocks[mine] == 0){
				blocks[mine] = 2;
			}
			else{
				i--;
			}
		}

		// ブロックの配置
		for(i = 0; i < MINE_SPOT_NUM; i++){
			block = new Block();
			var x = i % 8;
			var y = (i / 8) | 0;
			block.setPos(80 * x + 40, 80 * y + 64 + 40);
			block.blockType = blocks[i];
			this.addChild(block);
		}
	},

});

window.onload = function(){

	game = new Core(WIDTH, HEIGHT);
	game.fps = FPS;
	game.rootScene.backgroundColor = 'black';

	for(i = 0; i < sounds.length; i++){
		sounds[i] = DIR_SOUND + sounds[i];
	}

	for(i = 0; i < assets.length; i++){
		assets[i] = DIR_IMAGE + assets[i];
	}

	if(PC){
		// 音声データを game.preload で読み込ませるようにする。
		assets = assets.concat(sounds);

		// BGM をループさせるためのダミーのラベルを作成する。
		// フレームの更新イベントで BGM を再生しつづける。
		// 再生中に play を実行しても無視されるので問題はない。
		// ※firefox 24.0 で BGM をループさせるとタイトルに戻る不具合あり。
		//   currentTime を 0 にするループ方法でもタイトルに戻る。
		//   Chrome と android では問題なく動作している。
		dummyLabel = new Label("bummy");
		game.currentScene.addChild(dummyLabel);
		dummyLabel.visible = false;
		dummyLabel.addEventListener(Event.ENTER_FRAME,function(){
			if(bgmLoop != null){
				bgmLoop.play();
			}
		});
	}

	game.preload(assets);
	game.onload = function(){
		bgm = createSound('level_1.mp3', true);
		se.get = createSound('get.wav', false);
		game.pushScene(new MineScene());
	};

	// ゲーム開始
	game.start();
};

/**
 * 画像を取得する。
 * @param assets	画像名
 * @returns	画像
 */
function getImage(assets){
	return game.assets[DIR_IMAGE + assets];
}

/**
 * スプライトの作成。
 */
function createSprite(assets,width,height){
	var sprite = new Sprite(width, height);
	sprite.image = getImage(assets);
	sprite.visible = false;
	return sprite;
}

/**
 * サウンドの作成。
 */
function createSound(assets, isBgm){

	if(PC){
		return DIR_SOUND + assets;
	}
	else{
		if(isBgm){
			return new Media(getPath() + DIR_SOUND + assets, function(){}, function(){}, function(e){ if(e==Media.MEDIA_STOPPED){bgm.play();} });
		}
		else{
			return new Media(getPath() + DIR_SOUND + assets, function(){}, function(){});
		}
	}
}

/**
 * 効果音の再生。
 * PC では同じ音を同時に複数再生できる。
 * スマホでは同じ音は単音のみ。
 */
function playSe(sound){

	if(!SOUND){
		return;
	}

	if(PC){
		game.assets[sound].clone().play();
	}
	else{
		// 再生中に play をしても無視される。
		// 停止してから再生する必要がある。
		sound.stop();
		sound.play();
	}
}

/**
 * BGM の再生。
 * ※スマホでのループ処理はインスタンス生成時のコールバックで行っている。
 */
function playBgm(sound){
	if(!SOUND){
		return;
	}

	if(PC){
		game.assets[sound].play();
		bgmLoop = game.assets[sound];
	}
	else{
		sound.play();
	}
}

/**
 * ローカルパスの取得。
 */
function getPath(){
	var str = location.pathname;
	var i = str.lastIndexOf('/');
	return str.substring(0,i+1);
}

/**
 * 乱数の取得。
 */
function rand(n){
	return Math.floor(Math.random() * n);
}
