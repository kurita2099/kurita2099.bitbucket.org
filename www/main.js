﻿
enchant();
const CATEGORY = "com.qooga.bukiya.score00";
const PURCHASE = "com.qooga.bukiya.coin";//１０ダイヤ　21ダイヤ　55ダイヤ
const APPURL = "http://goo.gl/xEIpyZ";//"https://itunes.apple.com/us/app/chuan-shuono-wu-qi-wuno-chuan/id858927917?l=ja&ls=1&mt=8";
const LIFE_NOTIFICATION = 1;//ライフが増えた
const WEAPON_M_NOTIFICATION = 2;//武器ができた
const WEAPON_S_NOTIFICATION = 3;//武器が売れた
const LIFE_NOTIFICATION_MESS = "ライフが増えた";
const WEAPON_M_NOTIFICATION_MESS = "武器ができた";
const WEAPON_S_NOTIFICATION_MESS = "武器が売れた";

const	DEBUG = true;	//デバグ用、trueだと現在持ち物99個、鉱山レベルmax、coins5000

const	MIYAGAWA_DEBUG = 0;

const	WIDTH = 640;
//const	HEIGHT = 1136;
const	HEIGHT = screen.height> 480 ?1136 : 960;
const	FPS = 15;
const	SOUND = true;

const 	CHIPSIZE = 64;	//1ブロックの大きさ
const   WINDOW_SUB_OFFSET = 12;	//ウィンドウとウィンドウの間の共通空き幅(縦に）
const   INTERFACE_OFFSET_WIDTH = WIDTH;	//ステータスの幅高さ
const   INTERFACE_OFFSET_HEIGHT = 48+80;
const	CLOSE_OFFSET_X = WIDTH -10 - 90;	//Closeボタンの位置(10は指定、90はcloseボタンの幅)
const	CLOSE_OFFSET_Y = INTERFACE_OFFSET_HEIGHT + 10;

const	LIFE_COOLTIME = 6;	//ハートの回復時間 1/sec

const	DIR_IMAGE = 'img/';
const	DIR_SOUND = 'sound/';

const	STONE_DOU = 0;
const	STONE_TETU = 1;
const	STONE_MISURIRU = 2;
const	STONE_AMADAIN = 3;
const	STONE_ORIHARUKON = 4;
const	STONE_METEORON = 5;

const	PICKEL_DOU = 0;
const	PICKEL_TETU = 1;
const	PICKEL_DMISURIRU = 2;
const	PICKEL_AMADAIN = 3;
const	PICKEL_ORIHARUKON = 4;
const	PICKEL_METEORON = 5;

const	W_N = 6;	//ナイフの数
const   W_O = 16;	//斧の数
const   W_S = 16;	//剣の数
const	WEAPONPLACE=[	//各種武器の価格
				//ナイフ
		        30,60,120,240,480,1200,
		        //斧
		        80,120,160,200,240,320,360,400,480,640,680,720,800,960,1280,3200,
		        //剣
		        100,150,200,250,300,400,450,500,600,800,850,900,1000,1200,1600,4000,];

const	IVENT_NORM=  [//ノルマの数を全て配列に入れる
					3,3,3,								 //店規模1
					8,8,8,8,8,							 //店規模2
					16,16,16,16,16,16,16,				 //店規模3
					30,30,30,30,30,30,30,30,30,30,30,	 //店規模4
					];
					
const	IVENT_NPCINDEX= 	     [0,1,0,0 ,1, 0, 1, 0, 0, 1,0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0];		//その時に来るNPCの種類を指定する(0:勇者,1:兵隊長,10:エンディング,-1:何もなし)
const	IVENT_SHOPLEVELUP=       [3,8,15];	//店を拡張するタイミング
//const	IVENT_SHOPLEVELUP=       [1,2,3];
const	IVENT_MINELEVELUP=		 [4,9,16,24];
//const	IVENT_MINELEVELUP=		 [1,2,3,4];

//const	SHOP_EXTEND_TIME = 60;	//増築終了までの時間(1/sec)
const	SHOP_EXTEND_TIME = 8;	//増築終了までの時間(1/sec)
const	MINE_EXTEND_TIME = 3;	//速報windowが出るまでの時間(1/sec)
const	HOUSE_KID_SHOPCALL_TIME = 8;	//子供が客呼び込みする時間の長さ(1/sec)
const	HOUSE_KID_SHOPCALL_COOLTIME = HOUSE_KID_SHOPCALL_TIME+8;	//子供が客呼び込みを終了し、再度呼び込みできるようになるまでの時間の長さ(1/sec)
const	HOUSE_KID_GETMINE_TIME = 10;	//子供が次の鉱石を見つけてくるまでの時間(1/sec)

const 	MAX_SHOPLEVEL = 3;	//プレイヤレベルの段階最大値

const	IVENT_NEEDWEAPON=[
			//イベント毎に必要な武器の種類及び数,3次元配列([イベント進行度][必要な武器の種類の数(最大3種)][武器のIDと数]
			[	[22, 1],	[-1,-1],	[-1,-1]	],	//1		勇者	1
			[	[ 0, 3],	[-1,-1],	[-1,-1]	],	//2		兵隊長	1
			[	[23, 1],	[-1,-1],	[-1,-1]	],	//3		勇者	2
			[	[24, 1],	[-1,-1],	[-1,-1]	],	//4		勇者	3
			[	[23, 6],	[-1,-1],	[-1,-1]	],	//5		兵隊長	2
			[	[26, 1],	[-1,-1],	[-1,-1]	],	//6		勇者	4
			[	[ 1, 5],	[-1,-1],	[-1,-1]	],	//7		兵隊長	3
			[	[ 2, 1],	[-1,-1],	[-1,-1]	],	//8		勇者	5
			[	[27, 1],	[-1,-1],	[-1,-1]	],	//9		勇者	6
			[	[26, 8],	[-1,-1],	[-1,-1]	],	//10	兵隊長	4
			[	[28, 1],	[-1,-1],	[-1,-1]	],	//11	勇者	7
			[	[ 2, 5],	[-1,-1],	[-1,-1]	],	//12	兵隊長	5
			[	[13, 1],	[-1,-1],	[-1,-1]	],	//13	勇者	8
			[	[27, 8],	[-1,-1],	[-1,-1]	],	//14	兵隊長	6
			[	[30, 1],	[14, 1],	[ 3, 1]	],	//15	勇者	9
			[	[31, 1],	[15, 1],	[-1,-1]	],	//16	勇者	10
			[	[ 3, 5],	[-1,-1],	[-1,-1]	],	//17	兵隊長	7
			[	[ 4, 1],	[-1,-1],	[-1,-1]	],	//18	勇者	11
			[	[28, 5],	[29, 5],	[-1,-1]	],	//19	兵隊長	8
			[	[32, 1],	[-1,-1],	[-1,-1]	],	//20	勇者	12
			[	[ 4, 5],	[-1,-1],	[-1,-1]	],	//21	兵隊長	9
			[	[35, 1],	[16, 1],	[-1,-1]	],	//22	勇者	13
			[	[31, 5],	[32, 5],	[-1,-1]	],	//23	兵隊長	10
			[	[36, 1],	[20, 1],	[-1,-1]	],	//24	勇者	14
			[	[33, 5],	[34, 5],	[35, 5]	],	//25	兵隊長	11
			[	[37, 1],	[21, 1],	[ 5, 1]	],	//26	勇者	15
			
			];
			//各種武器のID
			/*
			ブロンズダガー	:0
			アイアンダガー	:1
			ミスリルダガー	:2
			アサシンダガー	:3
			グラディウス    :4
			伝説の短剣		:5

			銅の斧			:6
			トマホーク		:7
			鉄の斧			:8
			バトルアックス	:9
			鋼鉄の斧		:10
			ミスリルの斧	:11
			大地の斧		:12
			灼熱の斧		:13
			吹雪の斧		:14
			真紅の斧		:15
			女神の斧		:16
			巨人の斧		:17
			魔人の斧		:18
			闘魔の斧		:19
			魔王の斧		:20
			伝説の斧		:21

			銅の剣			:22
			ブロードソード	:23
			鉄の剣			:24
			ナイトセーバー	:25
			鋼鉄の剣		:26
			ミスリルの剣	:27
			大地の剣		:28
			炎の剣			:29
			氷の剣			:30
			紅の剣			:31
			アスカロン		:32
			アロンダイト	:33
			デュランダル	:34
			エクスカリバー	:35
			勇者の剣		:36
			伝説の剣		:37
			*/
			
var isbuying = false;//課金処理中
if(plugin == undefined){
var plugin = false;
}			
var isdisplogo = false;//アプリ起動時にタイトルロゴ表示するか？
var Player;
var PlayerOrig = {
		savetime:0,	//onPauseの時間記憶
		opentime:0,	//onResumeの時間記憶
		tutorial:true,//チュートリアルを挟むかどうか
		round:0,	//周回数
		story:0,	//エンディングを終えたか判断する(1は現在エンディングのイベント中、2はエンディングが終了した後)
		weapon:[//ナイフ
		        0,0,0,0,0,0,
		        //斧
		        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		        //剣
		        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
		        
		slot:[	//現在のスロット、[アイテムの種類(0:武器、1:鉱石、2:家具)][どこのスロットか][0:各種ID,1:数]
		[ [-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1] ],
		[ [-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1] ],
		[ [-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1],[-1,-1] ],
			],
		        
		knife:[2,5,],			// 短剣の数
		axe:[2,5,],				// 斧の数
		sword:[2,5,],			// 剣の数
    	coins:0,			//コイン数
		life:3,					// ライフ
		lifemax:3,				// ライフ最大値
    	lifetimer:0,					// ライフ回復用タイマー
    	lifetimerLock:false,
		pickel:0,				// ピッケルの種類
		stone:[0,0,0,0,0,0,],	// 鉱石の数(茶、青、灰、赤、黄、虹)
    	stoneGet:[false,false,false,false,false,false,],// 一度でも獲得した鉱石は true になる
		coal:0,					// 石炭の数
		diamond:0,				// ダイヤモンドの数
		shopLevel:0,			// 店舗拡張レベル
		shopLevelMax:0,			// どのレベルまで店舗を拡張できるか
		shopLevelTime:0,		// 増築が開始された時間を記録する
		shopLevelNowExtend:false,//現在増築中か否か
		mineLevel:0,			// 鉱山のレベル
		mineLevelMax:0,			// どのレベルまで鉱山のレベルが上がるか(速報メッセージを出す時に使用)
		mineLevelTime:0,		// メッゼージを出すためのタイマ
  		kagu:[0,0,0,0,0,0,0,0],	//8種類の家具、及びレベル(順番「タンス、かまど、ソファ、バスタブ、テーブル、子供ベッド、夫婦ベッド、ペット」
  		kaguPos:[ [0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0] ],	//家具の位置、0:0なら読み込み無効
		kama:[{state:1, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},
		      {state:0, makeStone1:0, makeStone2:0, makeStartTime:0, make:0, fireOffTime:0, fireTime:0, fire:0,},],
		recipe:[//ナイフ
		        0,0,0,0,0,0,
		        //斧
		        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		        //剣
		        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
		        
		iventIndex:0,		//イベント進行度、0からスタート
		iventWeaponCount:0,	//武器を何個売ったかカウント、イベントが進行する度初期化
		onIvent:false,		//現在イベント中か否か
		
		//子供関係
		kid_books:0,	//子供に買い与えた本の種類(0:なし 1:魔法使いの宅急便 2:魔王とボク 3:勇者の大冒険)
		kid_call:false,	//現在客呼び込みをしているかどうか
		kid_callTimer:0,	//子供が店呼び込みを終了する時間
		kid_callCoolTimer:0,	//子供が店呼び込みを終了して、復活する時間
		kid_mineTimer:0,	//子供が鉱石を獲得した後、記憶される箱
		
		//店番メッセージキュー:[通知するか否か、数]
		mise_Message:{know:false, num:0},
		
		//ここからオプションの設定
		op_hart:false,	//ハート回復の通知
		op_sell:false,	//武器完売の通知
		op_make:false,	//武器完成の通知
		
		};


var game;
var scene;

var se = new Object();

var bgmLoop = null;
var dummyLabel;

var onPauseTime = 0;

document.write("<script type='text/javascript' src='" + SC_URL + "js/loading/Loading.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/assets.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/FadeScene.js'></script>");//画像の読み込みタイミング変更バージョン
document.write("<script type='text/javascript' src='" + SC_URL + "js/Sprite2.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/Group2.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/Number.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/Flicker.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/title/Title.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/event/SceneEvent.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/shop/Shop.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/smithy/Smithy.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/mine/Mine.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/interFace/InterFaces.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/InventoryManager.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/house/House.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/npc/NPC.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/npc/NpcManager.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/store/StoreScene.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/mapdata.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/tutorial/Tutorials.js'></script>");
document.write("<script type='text/javascript' src='" + SC_URL + "js/other/Other.js'></script>");

function onDeviceReady() {
    document.addEventListener("pause", onPause, false);
    document.addEventListener("resume", onResume, false);
        for(var ii=0;ii<sounds.length;ii++){ 
            //alert(sounds[i]);
            var sname = sounds[ii];
            PGLowLatencyAudio.preloadAudio( sname, sname, 1 ,function(){;},function(e){alert(e+sname);});
    	}
		try{
			window.plugins.gamecenter.authenticate(function(){;},function(){;});
		}catch(e){
            alert(e.stack);
		}
    window.plugins.inAppPurchaseManager.onPurchased = function(transactionIdentifier, productId, transactionReceipt) {
        isbuying = false;
         navigator.notification.alert("購入成功="+productId);
        var addsums = [10,21,55];
        for(var i=0;i<3;i++){
            var id = PURCHASE + (i+1);
            if(id == productId){
                Player.diamond += addsums[id];
                break;
            }
        }
    };
    
    window.plugins.inAppPurchaseManager.onRestored = function(originalTransactionIdentifier, productId, originalTransactionReceipt) {
        isbuying = false;
    };
    
    window.plugins.inAppPurchaseManager.onFailed = function(errorCode, errorText){
        isbuying = false;
        navigator.notification.alert("購入エラー="+errorCode+","+errorText);
    };
    
}

function onPause() {
	Player.savetime = Date.parse(new Date());	//セーブ時間を記憶
	// セーブする
    localStorage.setItem("team_k002",JSON.stringify(Player));
	
	// 金の釜の対応

	if(smithyScene != null){
		smithyScene.onPause();
	}
}

function onResume() {

	Player.opentime = Date.parse(new Date());	//開いた時間記憶
	//スロットに武器が1つでも入っていたら店番設定
	if(game.InventoryManager.getWeaponCount(-1) > 0)
	{
		//過去の時間計算
		var time = new Date() - Player.savetime;
		console.log("Savetime_ミリ秒:"+time);
		game.InventoryManager.TimedSellWeapon(time);
	}
	
	if(smithyScene != null){
		smithyScene.onResume();
	}
}

window.onload = function(){
		var isfirstload = false;//アプリの初回起動か？
    	var jsonString = localStorage.getItem("team_k002");
    	if(jsonString){
    		Player = JSON.parse(jsonString);
    		if(Player.slot == undefined){
    			Player = PlayerOrig;
				isfirstload = true;
    		}
    	}else{
    		Player = PlayerOrig;
			isfirstload = true;
    	}
    	
    	//Player = PlayerOrig;
    	
    	
	if(MIYAGAWA_DEBUG){
		Player.coal = 100;
		for(var i = 0; i < Player.stoneGet.length; i++){
			Player.stone[i] = 100;
			Player.stoneGet[i] = true;
		}
		Player.diamond = 100;
		Player.pickel = PICKEL_TETU;
	}
	
	if(DEBUG)
	{
		//デバグ用に持ち物操作
		for(var i = 0; i < Player.weapon.length; i++)Player.weapon[i] = 20;	//武器数
		for(var i = 0; i < Player.recipe.length; i++)Player.recipe[i] = 1;	//レシピ開放
		//for(var i = 0; i < Player.stone.length; i++)Player.stone[i] = 50;	//鉱石増加
		//for(var i = 0; i < Player.stoneGet.length; i++)Player.stoneGet[i] = true;	//鉱石開放
		
		//Player.coins = 10000;	//お金

		//Player.diamond = 1;	//ダイヤ数
		//Player.coal = 50;		//石炭
		//Player.round = 1;
		//for(var i = 0; i < 8; i++)Player.kagu[i] = 3;
		//Player.shopLevel = 0;
		//Player.kid_books = 3;
		//Player.shopLevelMax = 0;
		//Player.iventIndex = 0;
		//Player.story = 0;
		//Player.mineLevel = 3;	//鉱山レベル
		//Player.mineLevelMax = 3;	//鉱山MAXレベル
		//Player.shopLevelNowExtend = false;
		//Player.iventWeaponCount = 99;	//販売した武器数を99個へ
		//Player.slot[0][0][0] = 2;
		//Player.slot[0][0][1] = 1;
		//Player.onIvent = false;
		//*/
	}

	document.addEventListener("deviceready", onDeviceReady, false);

	game = new Core(WIDTH, HEIGHT);
 	game.loadingScene = new Loading();
	game.fps = FPS;
	game.rootScene.backgroundColor = 'black';

	for(var i = 0; i < sounds.length; i++){
		sounds[i] = DIR_SOUND + sounds[i];
	}

	for(var i = 0; i < assets.length; i++){
		assets[i] = DIR_IMAGE + assets[i];
	}

	if(PC && plugin == false){
		// 音声データを game.preload で読み込ませるようにする。
		assets = assets.concat(sounds);

		// BGM をループさせるためのダミーのラベルを作成する。
		// フレームの更新イベントで BGM を再生しつづける。
		// 再生中に play を実行しても無視されるので問題はない。
		// ※firefox 24.0 で BGM をループさせるとタイトルに戻る不具合あり。
		//   currentTime を 0 にするループ方法でもタイトルに戻る。
		//   Chrome と android では問題なく動作している。
		dummyLabel = new Label("bummy");
		game.currentScene.addChild(dummyLabel);
		dummyLabel.visible = false;
		dummyLabel.addEventListener(Event.ENTER_FRAME,function(){
			if(bgmLoop != null){
				bgmLoop.play();
			}
		});
	}

	game.preload(assets);
	game.onload = function(){
		game.onload = null;		
	    	calcgazou();
		bgm = createSound('level_1.mp3', true);
		se.bgm_01 = createSound('bgm_01.mp3', true);
		se.bgm_02 = createSound('bgm_02.mp3', true);
		se.bgm_03 = createSound('bgm_03.mp3', true);
		se.bgm_04 = createSound('bgm_04.mp3', true);
		se.bgm_05 = createSound('bgm_05.mp3', true);
		se.bgm_ending = createSound('bgm_ending.mp3', true);
		se.bgm_main = createSound('bgm_main.mp3', true);
		se.clear = createSound('clear.mp3', false);
		se.break_ = createSound('break.wav', false);
		se.buy = createSound('buy.wav', false);
		se.fire = createSound('fire.wav', false);
		se.get = createSound('get.wav', false);
		se.made = createSound('made.wav', false);
		se.miss = createSound('miss.wav', false);
		se.move = createSound('move.wav', false);
		se.open = createSound('open.wav', false);
		se.put = createSound('put.wav', false);
		se.run = createSound('run.wav', false);
		se.shop = createSound('shop.mp3', false);
		se.tap_1 = createSound('tap_1.wav', false);
		se.tap_2 = createSound('tap_2.wav', false);
		se.window = createSound('window.wav', false);
		se.unbreakable = createSound('unbreakable.wav', false);
		se.coal = createSound('coal.wav', false);
		se.refresh = createSound('refresh.wav', false);
		
		game.InventoryManager = new InventoryManager();
		game.UI = new InterFace();			//InterFaceオブジェクトを記憶
		game.NpcManager = new NpcManager();	//NpcManagerオブジェクトを記憶
		game.Time = new Date();				//時間
		if(isfirstload){
			game.pushScene(new SceneEvent("opening"));
		}else{
			if(Player.tutorial)
			{
				game.pushScene(new FadeScene(tutorial_1_Assets, makeNewScene(Tutorial_1)));
			}else
			{
				game.pushScene(new FadeScene(shopAssets, makeNewScene(ShopScene)));
			}
		}
	};

	// ゲーム開始
	game.start();
};

/**
 * 画像を取得する。
 * @param assets	画像名
 * @returns	画像
 */
function getImage(assets){
	return game.assets[DIR_IMAGE + assets];
}

/**
 * スプライトの作成。
 */
function createSprite(assets,width,height){
	var sprite = new Sprite(width, height);
	sprite.image = getImage(assets);
	sprite.visible = false;
	return sprite;
}

/**
 * サウンドの作成。
 */
function createSound(assets, isBgm){

	if(PC){
		return DIR_SOUND + assets;
	}
	else{
		if(isBgm){
			return new Media(getPath() + DIR_SOUND + assets, function(){}, function(){}, function(e){ if(e==Media.MEDIA_STOPPED){bgm.play();} });
		}
		else{
			return new Media(getPath() + DIR_SOUND + assets, function(){}, function(){});
		}
	}
}

/**
 * 効果音の再生。
 * PC では同じ音を同時に複数再生できる。
 * スマホでは同じ音は単音のみ。
 */
function playSe(sound){
    try{
  if(plugin){
	PGLowLatencyAudio.play(sound);
      return;
  }
	if(!SOUND){
		return;
	}

	if(PC){
		game.assets[sound].clone().play();
	}
	else{
		// 再生中に play をしても無視される。
		// 停止してから再生する必要がある。
		sound.stop();
		sound.play();
	}
    }catch(e){
	console.log(e);
    }
}

/**
 * BGM の再生。
 * ※スマホでのループ処理はインスタンス生成時のコールバックで行っている。
 */
 var bgm_name;
function playBgm(sound){
   	if(plugin){
		if(bgm_name == sound)return; //今なっている　BGMと同じ場合は何もしない
    	if(bgm_name)PGLowLatencyAudio.stop(bgm_name);
     	PGLowLatencyAudio.loop(sound);
     	bgm_name = sound;
    	return;
   	}
	if(!SOUND){
		return;
	}

	if(PC){
		game.assets[sound].play();
		bgmLoop = game.assets[sound];
	}
	else{
		sound.play();
	}
}
function stopBgm(sound){
   	if(plugin){
		if(sound){
		PGLowLatencyAudio.stop(sound);
		bgm_name = null;
		}else if(bgm_name){
		PGLowLatencyAudio.stop(bgm_name);
		bgm_name = null;
		}
	}
}
/**
 * ローカルパスの取得。
 */
function getPath(){
	var str = location.pathname;
	var i = str.lastIndexOf('/');
	return str.substring(0,i+1);
}

/**
 * 乱数の取得。
 */
function rand(n){
	return Math.floor(Math.random() * n);
}

//分割リソース読み込みようの関数群
var loadImageCounter;	// 画像を何枚読みこんだのか数える
var loadImages = [];			// 画像パスの配列
/**
 * 画像の読み込み
 * @param images	画像パスの配列。
 */
function loadImage(images){
	loadImageCounter = 0;
	loadImages = images;
	
	for(var i = 0; i < loadImages.length; i++){
		
		game.load(DIR_IMAGE + loadImages[i], function(){
			
			// 読み込み完了した画像パスを取得
			var src = this._element.src;
			src = src.substring(src.indexOf('/' + DIR_IMAGE) + 1 + DIR_IMAGE.length);
			
			// 対象の画像が読みこまれたのかチェックする
			for(var j = 0; j < loadImages.length; j++){
				if(loadImages[j] == src){
					loadImageCounter++;
					break;
				}
			}
		})
	}
}


function releaseAllImage(){
	for(var i = 0; i < loadImages.length; i++){
	    	if(game.assets[DIR_IMAGE + loadImages[i]]._element.release){
			game.assets[DIR_IMAGE + loadImages[i]]._element.release();
			delete game.assets[DIR_IMAGE + loadImages[i]];//消してみた
		}
	}
}

function makeNewScene( classname , arg){
    return function(){
	return new classname(arg);
    }
}
//DOMレイヤーを使ったdiv画像の生成
function createDomSprite(filepath,w,h){
		var box = new enchant.Entity();
        if(w)box.width = w;
        if(h)box.height = h;
        box._element = document.createElement('div'); // input要素を割り当て
		if(filepath)box._element.style.backgroundImage = 'url(' + (filepath == "" ? "dummy.png" : DIR_IMAGE) + filepath + ')'; 
		return box;
}
function setDomImage(box,filepath,w,h){
		if(box._element){
		 box._element.style.backgroundImage = 'url(' + (filepath == "" ? "dummy.png" : DIR_IMAGE) + filepath + ')'; 
		 if(w){
            	box.width = w;
		 }
		 if(h){
            	box.height = h;
		 }
		}
}
/*
//マップチップからマップ生成 640x704サイズに自動センターリング
function createMap(mdata){
	var map = new Map(mdata.tileWidth, mdata.tileHeight);
	map.image = getImage(mdata.chipSet);
	map.loadData(mdata.layers[0].tiles);
	var width = mdata.tileWidth*mdata.width;//マップサイズ
	var height =mdata.tileHeight*mdata.height;//マップサイズ
	map.x = (WIDTH - width)/2;//センターリング
	map.y = (704 - height)/2;//センターリング
return map;
}
*/
//マップチップからマップ生成 640x704サイズに自動センターリング(増築に対応したver)
function createMap(mdata,scene){
	
	if(Player.shopLevelNowExtend)
	{
		//増築中なら増築チップを出現させる
		var chips = game.UI.MSP.sprite_extendChips;
		var index = (Player.shopLevel*2)+5+2;
		var group = new Group();
		var startPos = [0,0];
		//外側の壁を逆算して割り出す
		startPos[0] = (WIDTH-(index*64))/2;
		//startPos[1] = (HEIGHT -196 -(index*64))/2;
		startPos[1] = (HEIGHT-100-78)/2 -(index*64)/2;
		
		for(var i = 0; i < chips.length; i++)chips[i].visible = true;//全チップ初期化、使用する物だけaddChildする
		
		var chipCounter = 0;
		for(var i = 0 ; i < index; i++)
		{
			for(var j = 0; j < index; j++)
			{
				//外側だけ設定する
				if(i == 0 || j == 0 || i == index-1 || j == index-1)
				{
					chips[chipCounter].x = startPos[0] + (j*64);
					chips[chipCounter].y = startPos[1] + (i*64);
					group.addChild(chips[chipCounter]);
					chipCounter++;
				}
			}
		}
		
		scene.addChild(group);
	}
	var map = new Map(mdata.tileWidth, mdata.tileHeight);
	map.image = getImage(mdata.chipSet);
	map.loadData(mdata.layers[0].tiles);
	var width = mdata.tileWidth*mdata.width;//マップサイズ
	var height =mdata.tileHeight*mdata.height;//マップサイズ
	map.x = (WIDTH - width)/2;//センターリング
	map.y = (704 - height)/2;//センターリング
return map;
}

//画像の量計算
function calcgazou(){
　　//if(plugin)return;//実機では計測しない
    var sum = 0;
    for (var i in game.assets){
	if(game.assets[i].width)
	sum += game.assets[i].width*game.assets[i].height;
    }
    console.log("total="+sum);
}

function ChipToPosition(scene,cpx,cpy)
{
	//引数のゲーム内チップ座標を絶対座標系に変換して返す、sceneにoffsetが必要
	//例:(scene,0,0)で一番左上のブロック(壁)の絶対座標を返す
	var ox = scene.OFFSET_X;
	var oy = scene.OFFSET_Y;
	var x = CHIPSIZE * cpx;
	var y = CHIPSIZE * cpy;
	var FIELD_OFFSET = MAX_SHOPLEVEL - Player.shopLevel;	//中央にずらしていく
	x += ox;
	y += oy;
	x += FIELD_OFFSET*CHIPSIZE;
	y += FIELD_OFFSET*CHIPSIZE;
	x += -CHIPSIZE/2;
	
	return [x,y];
}

function PositionToChip(map,wpx,wpy)
{
	//引数の絶対座標系を、map内のチップ座標系へ変換する(整数変換)
	//相対座標系へ変換
	wpx = wpx - map.x;
	wpy = wpy - map.y;
	var cpx = Math.floor(wpx / CHIPSIZE);
	var cpy = Math.floor(wpy / CHIPSIZE);
	
	return [cpx,cpy];
}

function IventToIndex(iventName)
{
	//現在のPlayer.iventIndexが、引数の名前のイベントのどの部分かを数値で返す(例、兵隊長:0 = iventIndex:1  勇者:1 = iventIndex:2)
	
	var counter = 0;
	switch(iventName)
	{
		case "勇者":
		{
			//counterを回して現在の進行度を勇者進行度に変換
			for(var i = 0; i < Player.iventIndex+1; i++)if(IVENT_NPCINDEX[i] == 0)counter++;
			return counter;
		}
		
		case "兵隊長":
		{
			//counterを回して現在の進行度を兵隊長進行度に変換
			for(var i = 0; i < Player.iventIndex+1; i++)if(IVENT_NPCINDEX[i] == 1)counter++;
			return counter;
		}
		
		default:
		{
			alert("引数のiventNameが正規ではありません");
			break;
		}
	}
}
